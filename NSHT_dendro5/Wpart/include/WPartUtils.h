//
// Created by maksbh on 5/29/20.
//

#ifndef NAVIER_STOKES_HEAT_TRANSFER_UTILS_H
#define NAVIER_STOKES_HEAT_TRANSFER_UTILS_H

#include <oda.h>
#include <IBM/IBMSolver.h>
#include <NSHTNodeData.h>
#include <sys/stat.h>
PetscErrorCode saveGeometry(const IBMSolver<NSHTNodeData> &ibm, TimeInfo & ti){
  char folder[PATH_MAX];
  snprintf(folder, sizeof(folder), "results_%05d", ti.getTimeStepNumber());
  int ierr = mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (ierr != 0 && errno != EEXIST) {
    PrintError("Could not create folder for storing results (", strerror(errno), ").");
    return 1;
  }

  char fname[PATH_MAX];
  auto &geoms = ibm.getGeometries();
  int i = 0;

  for (const auto &geom : geoms) {
    char Geomname[PATH_MAX];
    snprintf(Geomname, sizeof(Geomname), "%s/%s_%d_%05d", folder, geom.geo->def_.name.c_str(), i,
             ti.getTimeStepNumber());

    stl2vtk(geom.geo, ti, Geomname);
    i++;
  }
}
void getNumberofOutGaussPoints(ot::DA *octDA, std::vector<DENDRITE_UINT > & numGauss, IBMSolver<NSHTNodeData> * ibmsolver, const double * problemSize){
  numGauss.clear();
  numGauss.resize(octDA->getLocalElemSz(),0);
  double coords[8*m_uiDim];
  GRID grid;
  const int nodal_size = 8;
  grid.redimArrays(nodal_size, 1);
  for (DENDRITE_INT i = 0; i < nodal_size; i++) {
    grid.node_array_[i] = new NODE();
  }

  ELEM *elem = new ELEM3dHexahedral();
  grid.elm_array_[0] = elem;

  DENDRITE_INT *node_id_array = new DENDRITE_INT[nodal_size];
  for (int i = 0; i < nodal_size; i++) {
    node_id_array[i] = i;
  }
  elem->redim(nodal_size, node_id_array);

  FEMElm fe(&grid, BASIS_ALL);
  int counter = 0;
  for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    octDA->getElementalCoords(octDA->curr(),coords);
    for(int i = 0;i < 8;i++){
      convertOctToPhys(&coords[3*i],problemSize);
      grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }
    fe.refill(elem, BASIS_LINEAR, 0);
    DENDRITE_UINT numGP = 0;

    while(fe.next_itg_pt()){
      if(not(ibmsolver->point_in_any_geometry(fe.position()))){
        numGP++;
      }
    }
    numGauss[counter] = numGP;
    counter++;
  }
}

void getNumSurfaceGaussPoint(ot::DA * octDA, std::vector<DENDRITE_UINT> & numSurfaceGaussPoints, IBMSolver<NSHTNodeData> * ibmSolver){
  numSurfaceGaussPoints.clear();
  numSurfaceGaussPoints.resize(octDA->getLocalElemSz(),0);
  auto geometries = ibmSolver->getGeometries();
  for (const auto &geom_data : geometries) {
    for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size();gpID++) {
      numSurfaceGaussPoints[geom_data.backgroundElementGP[gpID] - octDA->getMesh()->getElementLocalBegin()] ++;
    }
//    std::cout << "Total Size = " << geom_data.backgroundElementGP.size() << "\n";
//    std::cout << "Total Size accumulated " << std::accumulate(numSurfaceGaussPoints.begin(),numSurfaceGaussPoints.end(),0) << "\n";
  }


}
template <typename Equation, typename NodeData>
void computeMatrixCost(ot::DA * octDA,std::vector<double > & timeClock,TalyEquation<Equation, NodeData> * nsEq){
  nsEq->mat->preMat();
  double coords[8*m_uiDim];
  timeClock.resize(octDA->getLocalElemSz(),0.0);
  int counter = 0;
  std::chrono::high_resolution_clock::time_point t1,t2;
  for(int i = 0; i < 1; i++){
  for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    std::vector<ot::MatRecord> mr;
    octDA->getElementalCoords(octDA->curr(),coords);
    t1= std::chrono::high_resolution_clock::now();
    nsEq->mat->getElementalMatrix(octDA->curr(),mr,coords);
    t2= std::chrono::high_resolution_clock::now();
    timeClock[counter++] = std::chrono::duration<double>(t2 - t1).count();
  }
  }
  nsEq->mat->postMat();
}

template <typename Equation, typename NodeData>
void computeVectorCost(ot::DA * octDA,std::vector<double > & timeClock,TalyEquation<Equation, NodeData> * nsEq){
  nsEq->vec->preComputeVec(nullptr, nullptr,1.0);
  double coords[8*m_uiDim];
  timeClock.resize(octDA->getLocalElemSz());
  int counter = 0;
  std::chrono::high_resolution_clock::time_point t1,t2;
  double outVec[8*4];
  for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    std::vector<ot::MatRecord> mr;
    octDA->getElementalCoords(octDA->curr(),coords);
    t1= std::chrono::high_resolution_clock::now();
    nsEq->vec->elementalComputVec(nullptr,outVec, coords);
    t2= std::chrono::high_resolution_clock::now();
    timeClock[counter++] = std::chrono::duration<double>(t2 - t1).count();
  }
  nsEq->vec->postComputeVec(nullptr,nullptr,1.0);
}

template <typename Equation, typename NodeData>
void computeSurfaceCost(ot::DA * octDA,std::vector<double > & timeClock,TalyEquation<Equation, NodeData> * nsEq, IBMSolver<NodeData>*ibmSolver,
   const DENDRITE_REAL * problemSize) {
  timeClock.resize(octDA->getLocalElemSz());
  TalyDendroSync sync;

  TALYFEMLIB::ZeroMatrix<double> Ae;
  DENDRITE_UINT dof = 4;
  Ae.redim(dof * 8, dof * 8);
  ot::MatRecord mr;
  nsEq->mat->preMat();
  auto geometries = ibmSolver->getGeometries();
  double coords[8 * m_uiDim];
  auto syncVal = nsEq->mat->getSyncValuesVec();
  DENDRITE_REAL **_val_;
  _val_ = new DENDRITE_REAL *[syncVal.size()];
  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[8 * syncVal[i].ndof];
  }
  const int npe = 8;
  TALYFEMLIB::ZEROPTV ptvg, ptvl;
  DendroIntL *node_idx_dendro = new DendroIntL[npe];
  unsigned int eleOrder = octDA->getElementOrder();
  std::chrono::high_resolution_clock::time_point t1,t2;
  for (const auto &geom_data : geometries) {
    /** Looping over all the background element**/
    for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size();) {
      unsigned int currEleId = geom_data.backgroundElementGP[gpID];
      octDA->getElementalCoords(currEleId, coords);
      nsEq->mat->getSyncValues(_val_, currEleId);

      for (int i = 0; i < npe; i++) {
        convertOctToPhys(&coords[3 * i], problemSize);
      }

      sync.syncCoords<1, m_uiDim>(coords, nsEq->grid());

      for (int i = 0; i < syncVal.size(); i++) {
        sync.syncValues<NodeData, 1, m_uiDim>(_val_[i], nsEq->field(), syncVal[i].ndof,
                                              syncVal[i].nodeDataIndex);
      }

      Point position(coords[0], coords[1], coords[2]);
      int level = octDA->getLevel(currEleId);
      double h1 = problemSize[0] / (1u << level);
      double h2 = problemSize[1] / (1u << level);
      double h3 = problemSize[2] / (1u << level);

      Point h(h1, h2, h3);

      FEMElm fe(nsEq->grid(), TALYFEMLIB::BASIS_ALL);

      fe.refill(nsEq->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);

      Ae.fill(0.0);
      std::vector<ot::MatRecord>  records;
      t1 = std::chrono::high_resolution_clock::now();

      while ((geom_data.my_gauss_points[gpID].isLocal == true) and (geom_data.backgroundElementGP[gpID] == currEleId)
          and (gpID < geom_data.backgroundElementGP.size())) {

        ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
        ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
        ptvg.z() = geom_data.my_gauss_points[gpID].values[2];

        // do integration
        GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
        const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label, ptvg);
        fe.calc_at(ptvl);
        fe.set_jacc_x_w(gpinfo.area * (1.0 / static_cast<double>(geom_data.geo->gpno)));
        nsEq->equation()->ibm_Integrands4side_Ae(fe, Ae, geom_data.geo, gpinfo, position, h);
        gpID++;

      }

      octDA->getNodeIndices(node_idx_dendro, currEleId, true);

      /**
      * @author maksbh
      * [Warning:] Do not play around with this ordering of the loop.
      * Dendro5 interpolation assumes that first all enteries of
      * single dof is stored.
      */
      for (int dofi = 0; dofi < dof; dofi++) {
        for (int dofj = 0; dofj < dof; dofj++) {
          for (int i = 0; i < npe; i++) {
            for (int j = 0; j < npe; j++) {
              mr.setRowID(node_idx_dendro[i]);
              mr.setColID(node_idx_dendro[j]);
              mr.setColDim(dofj);
              mr.setRowDim(dofi);
              mr.setMatValue(Ae(dof * i + dofi, j * dof + dofj));
              records.push_back(mr);
            }
          }
        }
      }
      t2 = std::chrono::high_resolution_clock::now();
      timeClock[currEleId - octDA->getMesh()->getElementLocalBegin()] = std::chrono::duration<double>(t2 - t1).count();
    }
  }
  nsEq->mat->postMat();
  for (int i = 0; i < syncVal.size(); i++) {
    delete [] _val_[i];
  }
  delete [] _val_;
  delete[]node_idx_dendro;
}


template <typename Equation, typename NodeData>
void computeVecSurfaceCost(ot::DA * octDA,std::vector<double > & timeClock,TalyEquation<Equation, NodeData> * nsEq, IBMSolver<NodeData>*ibmSolver,
                        const DENDRITE_REAL * problemSize) {
  timeClock.resize(octDA->getLocalElemSz());
  TalyDendroSync sync;

  TALYFEMLIB::ZEROARRAY<double> be;
  DENDRITE_UINT dof = 4;
  be.redim(dof * 8);

  nsEq->vec->preComputeVec(nullptr,nullptr,1.0);
  auto geometries = ibmSolver->getGeometries();
  double coords[8 * m_uiDim];
  auto syncVal = nsEq->mat->getSyncValuesVec();
  DENDRITE_REAL **_val_;
  _val_ = new DENDRITE_REAL *[syncVal.size()];
  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[8 * syncVal[i].ndof];
  }
  const int npe = 8;
  TALYFEMLIB::ZEROPTV ptvg, ptvl;
  DendroIntL *node_idx_dendro = new DendroIntL[npe];
  unsigned int eleOrder = octDA->getElementOrder();
  std::chrono::high_resolution_clock::time_point t1,t2;
  for (const auto &geom_data : geometries) {
    /** Looping over all the background element**/
    for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size();) {
      unsigned int currEleId = geom_data.backgroundElementGP[gpID];
      octDA->getElementalCoords(currEleId, coords);
      nsEq->vec->getSyncValues(_val_, currEleId);

      for (int i = 0; i < npe; i++) {
        convertOctToPhys(&coords[3 * i], problemSize);
      }

      sync.syncCoords<1, m_uiDim>(coords, nsEq->grid());

      for (int i = 0; i < syncVal.size(); i++) {
        sync.syncValues<NodeData, 1, m_uiDim>(_val_[i], nsEq->field(), syncVal[i].ndof,
                                              syncVal[i].nodeDataIndex);
      }

      Point position(coords[0], coords[1], coords[2]);
      int level = octDA->getLevel(currEleId);
      double h1 = problemSize[0] / (1u << level);
      double h2 = problemSize[1] / (1u << level);
      double h3 = problemSize[2] / (1u << level);

      Point h(h1, h2, h3);

      FEMElm fe(nsEq->grid(), TALYFEMLIB::BASIS_ALL);

      fe.refill(nsEq->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);

      be.fill(0.0);

      t1 = std::chrono::high_resolution_clock::now();

      while ((geom_data.my_gauss_points[gpID].isLocal == true) and (geom_data.backgroundElementGP[gpID] == currEleId)
          and (gpID < geom_data.backgroundElementGP.size())) {

        ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
        ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
        ptvg.z() = geom_data.my_gauss_points[gpID].values[2];

        // do integration
        GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
        const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label, ptvg);
        fe.calc_at(ptvl);
        fe.set_jacc_x_w(gpinfo.area * (1.0 / static_cast<double>(geom_data.geo->gpno)));
        nsEq->equation()->ibm_Integrands4side_be(fe, be, geom_data.geo, gpinfo, position, h);
        gpID++;

      }

      octDA->getNodeIndices(node_idx_dendro, currEleId, true);

      t2 = std::chrono::high_resolution_clock::now();
      timeClock[currEleId - octDA->getMesh()->getElementLocalBegin()] = std::chrono::duration<double>(t2 - t1).count();
    }
  }
  nsEq->vec->postComputeVec(nullptr, nullptr,1.0);
  for (int i = 0; i < syncVal.size(); i++) {
    delete [] _val_[i];
  }
  delete [] _val_;
  delete[]node_idx_dendro;
}

double computestd(const std::vector<double> & timer, const std::vector<DENDRITE_UINT > & numGP){
  double var = 0;

  DENDRITE_UINT numGaussPoint = std::accumulate(numGP.begin(),numGP.end(),0);
  double mean = (std::accumulate(timer.begin(),timer.end(),0.0))/(numGaussPoint*1.0);

  for(DENDRITE_UINT i = 0; i < timer.size(); i++){

     var += (timer[i]  - mean*numGP[i]) * (timer[i] - mean*numGP[i]);

  }

  double std = sqrt(var/numGaussPoint);
  return std;
}


double computeRatioVariance(const std::vector<double> & timerVolume, const std::vector<double >& timerSurface,
    const std::vector<DENDRITE_UINT > & numsurfGP, const std::vector<DENDRITE_UINT > & numVolGP){
  double varVolume = 0;
  double varSurface = 0;

  DENDRITE_UINT numVolumeGaussPoint = std::accumulate(numVolGP.begin(),numVolGP.end(),0);
  DENDRITE_UINT numSurfaceGaussPoint = std::accumulate(numsurfGP.begin(),numsurfGP.end(),0);

  double meanVolume = (std::accumulate(timerVolume.begin(),timerVolume.end(),0.0))/(numVolumeGaussPoint*1.0);
  double meanSurface = (std::accumulate(timerSurface.begin(),timerSurface.end(),0.0))/(numSurfaceGaussPoint*1.0);

  for(DENDRITE_UINT i = 0; i < timerVolume.size(); i++){
    varVolume += (timerVolume[i]  - meanVolume*numVolGP[i]) * (timerVolume[i] - meanVolume*numVolGP[i]);
    varSurface += (timerSurface[i]  - meanSurface*numsurfGP[i]) * (timerSurface[i] - meanSurface*numsurfGP[i]);
  }
  double varRatio = varSurface/(meanSurface) + varVolume/(meanVolume);
  return varRatio;
}

#endif //NAVIER_STOKES_HEAT_TRANSFER_UTILS_H
