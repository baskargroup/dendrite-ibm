this_dir=$(pwd)
parentdir="$(dirname "$this_dir")"
exec_dir="$(dirname "$parentdir")"
mpirun_command=$(which mpirun)

# compile everything (assume already cmaked)
echo ---------------------- Compile ---------------------
# cd $exec_dir/$build_type/examples/Basic
for build_folder in $this_dir/cmake-build*
do
  cd $build_folder
  echo ---------------------- Compile $build_folder -----------------
  make -j 8
  echo -------------------------------------------------------------
  cd ..
done

# run every regression test
echo ---------------------- Test ---------------------
cd $this_dir
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    python3 *_regression*.py
  else
    echo "Apply filter " ${1}
    python3 *_regression*.py --filter="${1}"
fi