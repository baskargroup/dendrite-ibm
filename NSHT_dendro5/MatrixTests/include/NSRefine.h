#pragma once

#include <Refinement/Refinement.h>
#include <DendriteUtils.h>
#include "IBM/Geom.h"

/**
 * This is a derived class that takes care of refinement strategy
 * For this example we only refine on the boundaries (position based)
 */
class NSRefine : public Refinement {
  double *coords_ = NULL;
  std::vector<ZEROPTV> nodes;
 protected:
  using Refinement::calcElementalError;
  NSInputData *idata;

 public:

  NSRefine(ot::DA *da, const DENDRITE_REAL *problemSize, NSInputData *idata_in);

  ot::DA_FLAGS::Refine calcRefineFlags() override;

  ~NSRefine() {
    delete coords_;
  }
};

/**
 * Constructor: We instantiate the Refinement class here in the constructor appropriately.
 * Passing just the oct da and the problemSize to the Refinement constructor will make it default to position based
 * refinement.
 * Choosing the type of refinement is based on the way Refinement class is instantiated.
 * @param da: octree mesh
 * @param problemSize: Scaling of the box of the octree mesh
 * @param idata_in: user inputted values
 */
NSRefine::NSRefine(ot::DA *da,
                   const DENDRITE_REAL *problemSize,
                   NSInputData *idata_in)
    : Refinement(da, problemSize), idata(idata_in) {
  coords_ = new double[da->getNumNodesPerElement() * m_uiDim];
  eleOrder_ = baseDA_->getElementOrder();
  nodes.resize(da->getNumNodesPerElement());
  /// We calculate the vector of refine flags for each element
  if (da->isActive()) {
    this->calcRefinementVector();
  }

}

/**
 * Used for position based refinement. Evaluated for every element
 * This method calculates whether to refine the element or not based on position.
 * @return returns a flag on whether to refine the element.
 */

ot::DA_FLAGS::Refine NSRefine::calcRefineFlags() {
  const int nodeNo = baseDA_->getNumNodesPerElement();
  const int currLevel = baseDA_->getLevel(baseDA_->curr());

  baseDA_->getElementalCoords(baseDA_->curr(), coords_);
  coordsToZeroptv(coords_, nodes, problemSize_, eleOrder_, true);


  /// refine walls (maximum to the refine_h level)
  const double eps = 1e-13;
  // refine walls
  const auto &bc = idata->boundary_def;
  if (idata->mesh_def.refine_walls) {
    if (bc[BoundaryDef::X_MINUS].ifRefine) {
      for (const auto &p : nodes) {
        if (p.x() - idata->mesh_def.channel_min.x() < eps and currLevel < idata->mesh_def.refine_h) {
          return ot::DA_FLAGS::Refine::DA_REFINE;
        }
      }
    }
    if (bc[BoundaryDef::Y_MINUS].ifRefine) {
      for (const auto &p : nodes) {
        if (p.y() - idata->mesh_def.channel_min.y() < eps and currLevel < idata->mesh_def.refine_h) {
          return ot::DA_FLAGS::Refine::DA_REFINE;
        }
      }
    }
    if (bc[BoundaryDef::Z_MINUS].ifRefine) {
      for (const auto &p : nodes) {
        if (p.z() - idata->mesh_def.channel_min.z() < eps and currLevel < idata->mesh_def.refine_h) {
          return ot::DA_FLAGS::Refine::DA_REFINE;
        }
      }
    }
    if (bc[BoundaryDef::X_PLUS].ifRefine) {
      for (const auto &p : nodes) {
        if (idata->mesh_def.channel_max.x() - p.x() < eps and currLevel < idata->mesh_def.refine_h) {
          return ot::DA_FLAGS::Refine::DA_REFINE;
        }
      }
    }
    if (bc[BoundaryDef::Y_PLUS].ifRefine) {
      for (const auto &p : nodes) {
        if (idata->mesh_def.channel_max.y() - p.y() < eps and currLevel < idata->mesh_def.refine_h) {
          return ot::DA_FLAGS::Refine::DA_REFINE;
        }
      }
    }
    if (bc[BoundaryDef::Z_PLUS].ifRefine) {
      for (const auto &p : nodes) {
        if (idata->mesh_def.channel_max.z() - p.z() < eps and currLevel < idata->mesh_def.refine_h) {
          return ot::DA_FLAGS::Refine::DA_REFINE;
        }
      }
    }
  }
}


