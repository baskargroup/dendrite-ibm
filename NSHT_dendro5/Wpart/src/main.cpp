//
// Created by maksbh on 5/29/20.
//

#include <iostream>
#include <DendriteUtils.h>
#include <NSHTInputData.h>
#include <NSEquation.h>
#include <IBM/IBMUtils.h>
#include "../include/WPartUtils.h"

int main(int argc, char **argv){
  dendrite_init(argc,argv);
  NSHTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }

  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }
  /// load IBM meshes
  std::vector<Geom<NSHTNodeData> *> geoms;
  for (const auto &geom_def : idata.geom_defs) {
    geoms.push_back(new Geom<NSHTNodeData>(geom_def));
  }
  bool test = static_cast<bool>(std::atoi(argv[1]));

  ot::DA * octDA = createRegularDA(idata.mesh_def.refine_l);

  TimeInfo ti(0, idata.dt, idata.totalT);
  Point d_min(idata.mesh_def.channel_min.x(),
              idata.mesh_def.channel_min.y(),
              idata.mesh_def.channel_min.z());
  Point d_max(idata.mesh_def.channel_max.x(),
              idata.mesh_def.channel_max.y(),
              idata.mesh_def.channel_max.z());

  const double scalingFactor[3]{idata.mesh_def.channel_max.x(),
                                idata.mesh_def.channel_max.y(),
                                idata.mesh_def.channel_max.z()};
  auto ibmBC = [&geoms](unsigned int dof) {
    return 0.0;
  };
  auto ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);

  auto nsEq = new TalyEquation<NSEquation,NSHTNodeData>(octDA,d_min,d_max,NSHTNodeData::NS_DOF,false,&idata,ibm);



  if (octDA->isActive()) {
    ibm->partitionGeometries();
  }
  auto NSSolver =
      setIBMNonLinearSolver<NSEquation, NSHTNodeData>(nsEq, octDA, geoms, ibm, NSHTNodeData::NS_DOF, false);

  std::vector<DENDRITE_UINT > numGaussPoint, numSurfaceGaussPoint;
  getNumberofOutGaussPoints(octDA,numGaussPoint,ibm,scalingFactor);
  getNumSurfaceGaussPoint(octDA,numSurfaceGaussPoint,ibm);



  std::cout << " --------------------Matrix costs -----------------------------------\n";
  {
    for(int numTrial = 0; numTrial < 10; numTrial++){
    std::vector<double> timeClock;
    computeMatrixCost(octDA, timeClock, nsEq);
    double totalVolumeTime = std::accumulate(timeClock.begin(), timeClock.end(), 0.0);
    int totalVolGP = std::accumulate(numGaussPoint.begin(), numGaussPoint.end(), 0);
    double volAverageTime = totalVolumeTime / (totalVolGP * 1.0);
    double stdDevVolume = computestd(timeClock,numGaussPoint);
    std::cout << "Volume Avg time = " << volAverageTime << " " << " " << stdDevVolume <<  " "<< totalVolGP << "\n";

    std::vector<double> timeClockSurface;
    computeSurfaceCost(octDA, timeClockSurface, nsEq, ibm, scalingFactor);

    double totalSurfTime = std::accumulate(timeClockSurface.begin(), timeClockSurface.end(), 0.0);
    int totalSurfGP = std::accumulate(numSurfaceGaussPoint.begin(), numSurfaceGaussPoint.end(), 0);

    double surfAvgTime = (totalSurfTime / (totalSurfGP * 1.0));
    double stdDevSurface = computestd(timeClockSurface,numSurfaceGaussPoint);
    std::cout << "Surface Avg time = " << surfAvgTime << " " << " " << stdDevSurface <<  " "<< totalSurfGP << "\n";
    double ratioMean = volAverageTime / surfAvgTime;
    std::cout << "Ratio = " << volAverageTime / surfAvgTime <<  "\n";
    if(test){
      double ratio = idata.ratio;
      std::cout << ratio << "\n";
      std::vector<double> predictedWeight(octDA->getLocalElemSz());
      std::vector<double> ActualWeight(octDA->getLocalElemSz());
      for(int i = 0; i < predictedWeight.size(); i++){
        predictedWeight[i] = numGaussPoint[i]/8.0 + numSurfaceGaussPoint[i]/(8*ratio);
        ActualWeight[i] = (timeClock[i] + timeClockSurface[i])/(volAverageTime*8);
      }
#ifndef NDEBUG
      std::ofstream file("comparMatTest_debug.dat");
#else
      std::ofstream file("comparMatTest_release_" + std::to_string(numTrial) +".dat");
#endif
      for (int i = 0; i < predictedWeight.size(); i++) {
        if (numSurfaceGaussPoint[i] > 0)
          file << predictedWeight[i] << " " << ActualWeight[i] << " " << timeClock[i] << " " << numGaussPoint[i] << " "
               << timeClockSurface[i] << " " << numSurfaceGaussPoint[i] << "\n";
      }
      file.close();
    }
    else {
      std::vector<double> predictedTime(octDA->getLocalElemSz());
      std::vector<double> actualTime(octDA->getLocalElemSz());
      for (int i = 0; i < predictedTime.size(); i++) {
        predictedTime[i] = numGaussPoint[i] * volAverageTime + numSurfaceGaussPoint[i] * surfAvgTime;
        actualTime[i] = timeClock[i] + timeClockSurface[i];
      }
#ifndef NDEBUG
      std::ofstream file("comparMat_debug.dat");
#else
      std::ofstream file("comparMat_release.dat");
#endif
      for (int i = 0; i < predictedTime.size(); i++) {
        if (numSurfaceGaussPoint[i] > 0)
          file << predictedTime[i] << " " << actualTime[i] << " " << timeClock[i] << " " << numGaussPoint[i] << " "
               << timeClockSurface[i] << " " << numSurfaceGaussPoint[i] << "\n";
      }
      file.close();
    }
    }
   }


  std::cout << " --------------------Vector costs -----------------------------------\n";
  {
    std::vector<double> timeClock;
    computeVectorCost(octDA,timeClock,nsEq);
    double totalVolumeTime = std::accumulate(timeClock.begin(), timeClock.end(), 0.0);
    DENDRITE_UINT totalVolGP = std::accumulate(numGaussPoint.begin(), numGaussPoint.end(), 0);
    double volAverageTime = totalVolumeTime / (totalVolGP * 1.0);
    double stdVol = computestd(timeClock,numGaussPoint);

    std::vector<double> timeClockSurface;
    computeVecSurfaceCost(octDA, timeClockSurface, nsEq, ibm, scalingFactor);

    double totalSurfTime = std::accumulate(timeClockSurface.begin(), timeClockSurface.end(), 0.0);
    int totalSurfGP = std::accumulate(numSurfaceGaussPoint.begin(), numSurfaceGaussPoint.end(), 0);
    double surfAvgTime = (totalSurfTime / (totalSurfGP * 1.0));
    double stdSurf = computestd(timeClockSurface,numSurfaceGaussPoint);
    std::cout << "Volume Avg time = " << volAverageTime << " " << stdVol << "\n";
    std::cout << "Surface Avg time = " << surfAvgTime << " " <<  stdSurf << "\n";
    std::cout << "Ratio = " << volAverageTime / surfAvgTime <<  "\n";

    std::vector<double> predictedTime(octDA->getLocalElemSz());
    std::vector<double> actualTime(octDA->getLocalElemSz());
    for (int i = 0; i < predictedTime.size(); i++) {
      predictedTime[i] = numGaussPoint[i] * volAverageTime + numSurfaceGaussPoint[i] * surfAvgTime;
      actualTime[i] = timeClock[i] + timeClockSurface[i];
    }

#ifndef NDEBUG
    std::ofstream file("comparVec_debug.dat");
#else
    std::ofstream file("comparVec_release.dat");
#endif
    for (int i = 0; i < predictedTime.size(); i++) {
      if (numSurfaceGaussPoint[i] > 0)
        file << predictedTime[i] << " " << actualTime[i] << " " << timeClock[i] << " " << numGaussPoint[i] << " "
             << timeClockSurface[i] << " " << numSurfaceGaussPoint[i] << "\n";
    }
    file.close();

  }


  dendrite_finalize(octDA);


}
