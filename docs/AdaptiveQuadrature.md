Running
=======
* The config file from [NSHT_dendro5/tests/AdaptiveQuadrature](../NSHT_dendro5/tests/AdaptiveQuadrature) should be copied to the build directory.
* The following parameters in the config file needs to be updated:
```bash
gp_handle = 32;
gp_max_splitting = #levels;
```
* Launch `mpirun -np $NPROC ./ns_ht` for adaptive quadrature.