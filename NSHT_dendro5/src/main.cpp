//
// Created by maksbh on 11/24/18.
//
#include <oda.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include "Globals.h"

#include "NSHTNodeData.h"
#include "NSHTInputData.h"
#include "HTEquation.h"
#include "NSEquation.h"
#include "NSHTBCSetup.h"
#include "NSHTRefine.h"
#include "HTPostProcess.h"
#include "NSPostProcess.h"
#include "IBM/Geom.h"
#include "IBM/IBMSolver.h"
#include <IBM/IBMNonLinearSolver.h>
#include <IBM/IBMUtils.h>

#include "PETSc/Solver/LinearSolver.h"
#include "PETSc/PetscUtils.h"
#include <sys/stat.h>
#include <IBM/IBMFeUtils.h>
#include <Checkpoining/checkpointer.hpp>



/**
 * Calculates the L2 error of two vectors, used in block iteration.
 * @param[in] vec_u first of the two vectors for comparison
 * @param[in] vec_block second of the two vectors for comparison
 * @param[in] ndof ndof of the vector
 * @return
 */
double calc_error(Vec vec_u, Vec vec_block, const unsigned int ndof, MPI_Comm comm) {
  PetscInt size;
  VecGetLocalSize(vec_u, &size);
  PetscInt low;
  VecGetOwnershipRange(vec_u, &low, nullptr);

  const PetscScalar *u;
  const PetscScalar *block;
  VecGetArrayRead(vec_u, &u);
  VecGetArrayRead(vec_block, &block);

  std::vector<double> totalU(ndof, 0.0); // holds sum of u^2 at every node
  std::vector<double> totalErr(ndof, 0.0); // holds sum of (u - u_block)^2 at every node
  for (PetscInt i = 0; i < size; i++) {
    const unsigned int dof = (low + i) % ndof;

    totalU[dof] += u[i] * u[i];

    double diff = u[i] - block[i];
    totalErr[dof] += diff * diff;
  }

  VecRestoreArrayRead(vec_u, &u);
  VecRestoreArrayRead(vec_block, &block);

  std::vector<double> allU(ndof);
  std::vector<double> allErrs(ndof);
  MPI_Allreduce(totalU.data(), allU.data(), ndof, MPI_DOUBLE, MPI_SUM, comm);
  MPI_Allreduce(totalErr.data(), allErrs.data(), ndof, MPI_DOUBLE, MPI_SUM, comm);
  totalU.clear();
  totalErr.clear();
  double largest = -1e16;
  for (unsigned int dof = 0; dof < ndof; dof++) {
    double err = allErrs[dof] / (allU[dof] + 1e-12);

    if (std::isnan(err) || std::isinf(err)) {
      throw TALYFEMLIB::TALYException() << "Infinity or NAN in block error";
    }

    if (err > largest) {
      largest = err;
    }
  }
  return sqrt(largest);
}

/**
 * Resets the interior od the geometry to 0.
 * @param octDA
 * @param vec
 * @param ndof
 * @param ibmSolver
 * @param scalingFactor
 */
void resetVecs(ot::DA * octDA,Vec & vec,const DENDRITE_UINT ndof,const IBMSolver<NSHTNodeData> * ibmSolver, const double * scalingFactor){
  double *localArray;
  VecGetArray(vec,&localArray);

  double * ghostedArray;
  octDA->nodalVecToGhostedNodal(localArray,ghostedArray, false,ndof);
  octDA->readFromGhostBegin(ghostedArray,ndof);
  octDA->readFromGhostEnd(ghostedArray,ndof);
  int npe = octDA->getNumNodesPerElement();
  double * coords = new double[m_uiDim*npe];
  int eleOrder = octDA->getElementOrder();
  int totalNumNodes = octDA->getTotalNodalSz();

  DendroIntL * localnodeIdx = new DendroIntL[npe];

  for (octDA->init<ot::DA_FLAGS::WRITABLE>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::WRITABLE>(); octDA->next<ot::DA_FLAGS::WRITABLE>()){
    octDA->getElementalCoords(octDA->curr(),coords);
    octDA->getNodeIndices(localnodeIdx,octDA->curr(),true);

    for(int i = 0; i < npe; i++){
      convertOctToPhys(&coords[m_uiDim*i],scalingFactor);
    }

    int counter = 0;
    for (DENDRITE_UINT iz = 0; iz < (eleOrder + 1); iz++) {
      for (DENDRITE_UINT iy = 0; iy < (eleOrder + 1); iy++) {
        for (DENDRITE_UINT ix = 0; ix < (eleOrder + 1); ix++) {
          ZEROPTV pt {coords[counter*m_uiDim + 0],coords[counter*m_uiDim + 1],coords[counter*m_uiDim + 2]};
          if(ibmSolver->point_in_any_geometry(pt)) {
            if (not(octDA->getMesh()->isNodeHanging(octDA->curr(), ix, iy, iz))) {
              for (int dof = 0; dof < ndof; dof++) {
                ghostedArray[dof * totalNumNodes + localnodeIdx[counter]] = 0;
              }
            }
          }
          counter++;
        }
      }
    }
  }
  octDA->ghostedNodalToNodalVec(ghostedArray,localArray,true,ndof);
  delete[] coords;
  delete[] ghostedArray;
  delete[] localnodeIdx;


  VecRestoreArray(vec,&localArray);
}


/**
 * Save the octree mesh to vtk binary file.
 * @param da
 * @param daScalingFactor
 * @param ti
 * @param ns
 * @param ht
 * @param prefix
 */
PetscErrorCode save_timestep(ot::DA *octDA,
                             Vec vec,
                             unsigned int ndof,
                             const TimeInfo &ti,
                             const IBMSolver<NSHTNodeData> &ibm,
                             const std::string &prefix = "timestep",
                             const double *scalingFactor = nullptr,
                             char **nodalVarNames = nullptr,
                             bool saveGeometry = true) {
  if (octDA->isActive()) {
    // create directory for this timestep (if it doesnt already exist)
    char folder[PATH_MAX];
    snprintf(folder, sizeof(folder), "results_%05d", ti.getTimeStepNumber());
    int ierr = mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (ierr != 0 && errno != EEXIST) {
      PrintError("Could not create folder for storing results (", strerror(errno), ").");
      return 1;
    }

    char fname[PATH_MAX];
    snprintf(fname, sizeof(fname), "%s/%s_%05d", folder, prefix.c_str(), ti.getTimeStepNumber());
    Vec local;
    octDA->petscCreateVector(local,false, false,ndof);
    VecCopy(vec,local);
    resetVecs(octDA,local,ndof,&ibm,scalingFactor);
    octDA->petscVecTopvtu(local, fname, nodalVarNames, false, false, ndof, scalingFactor);
    VecDestroy(&local);
    if(saveGeometry) {
      auto &geoms = ibm.getGeometries();
      int i = 0;
      for (const auto &geom : geoms) {
        char Geomname[PATH_MAX];
        snprintf(Geomname, sizeof(Geomname), "%s/%s_%d_%05d", folder, geom.geo->def_.name.c_str(), i,
                 ti.getTimeStepNumber());
        stl2vtk(geom.geo, ti, Geomname);
        i++;
      }
    }
  }
}

static unsigned int getNodeWeight(const ot::TreeNode *t) {

  const unsigned int mid = (1u << (m_uiMaxDepth - 1));
  if (t->maxX() < (mid) && t->maxY() < mid && t->maxZ() < mid) {
    return 1;
  } else {
    return 1;
  }

}

static unsigned int getNodeWeight1000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 1000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight1500(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 1500;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight2000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 2000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight2500(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 2500;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight3000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 3000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight4000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 4000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight5000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 500;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight6000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 6000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight7000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 7000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight8000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 8000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight9000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 9000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight10000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 10000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight12000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 12000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight14000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 14000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight16000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 16000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight18000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 18000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight20000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 20000;
  } else {
    return 1000;
  }
}

static unsigned int getNodeWeight25000(const ot::TreeNode *t) {
  const unsigned int level = t->getLevel();
  if (level == 8) {
    return 25000;
  } else {
    return 1000;
  }
}

using namespace PETSc;

void getRefinedDAafterCheckPointing(ot::DA *& da, NSHTInputData * inputData, const DENDRITE_REAL * scaling_factor, std::vector<Geom<NSHTNodeData> *> & geoms, std::vector<VecInfo> & vecs){
  while(true) {
    Vec out;
    NSHTRefine refine(da, scaling_factor, inputData, geoms);
    ot::DA * newDA = NULL;
    newDA = refine.getRefineDA(nullptr);
    if(newDA == da){
      return;
    }
    Vec temp;
    newDA->petscCreateVector(temp, false, false,1);
    da->petscVecTopvtu(vecs[0].v,"AfterRefine/refineNS", nullptr, false, false,1,scaling_factor);
    VecDestroy(&temp);
    refine.performIntergridTransfer(newDA,vecs[0].v,out,NSHTNodeData::NS_DOF);
    refine.performVectorExchange(newDA,vecs[0].v,out,NSHTNodeData::NS_DOF);

    refine.performIntergridTransfer(newDA,vecs[1].v,out,NSHTNodeData::HT_DOF);
    refine.performVectorExchange(newDA,vecs[1].v,out,NSHTNodeData::HT_DOF);

    refine.finalizeAMR(newDA);
    std::swap(newDA,da);

  }

}

int main(int argc, char **argv) {
  dendrite_init(argc, argv);
  /// check for regression run
  PetscBool dump_vec = PETSC_FALSE;
  PetscOptionsGetBool(NULL, NULL, "-dump_vec", &dump_vec, NULL);

  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);

  /// nodalVariable name
  char *nsname[]{"vel_x", "vel_y", "vel_z", "pressure"};
  char *htname[]{"temperature"};
  /// read inputData
  NSHTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }

  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  idata.solverOptionsHT.apply_to_petsc_options("-ht_");
  idata.solverOptionsNS.apply_to_petsc_options("-ns_");

  /// load IBM meshes
  std::vector<Geom<NSHTNodeData> *> geoms;
  for (const auto &geom_def : idata.geom_defs) {
    geoms.push_back(new Geom<NSHTNodeData>(geom_def));
  }

  /// checkpoint
  bool resume_from_checkpoint = false;
  {
    PetscBool resume = PETSC_FALSE;
    PetscOptionsGetBool(nullptr, nullptr, "-resume_from_checkpoint", &resume, nullptr);
    resume_from_checkpoint = (resume == PETSC_TRUE);
  }

  m_uiMaxDepth = 20;
  ot::DA *octDA = NULL;
  Vec prev_solution_ht, prev_solution_ns;
  TimeInfo ti(0, idata.dt, idata.totalT);
  Point d_min(idata.mesh_def.channel_min.x(),
              idata.mesh_def.channel_min.y(),
              idata.mesh_def.channel_min.z());
  Point d_max(idata.mesh_def.channel_max.x(),
              idata.mesh_def.channel_max.y(),
              idata.mesh_def.channel_max.z());

  const double scalingFactor[3]{idata.mesh_def.channel_max.x(),
                                idata.mesh_def.channel_max.y(),
                                idata.mesh_def.channel_max.z()};
  checkpointer Checkpoint;
  Checkpoint.set_filename_prefix("Checkpoint");
  Checkpoint.set_checkpoint_frequency(1);
  Checkpoint.set_n_backups(5);
  if (not resume_from_checkpoint) {

    octDA = createRegularDA(idata.mesh_def.refine_l, m_uiMaxDepth,idata.grainSz);
    MPI_Barrier(MPI_COMM_WORLD);

    /// Refinement
    int iterOfRefinement = 0;
    while (iterOfRefinement < 10) {
      NSHTRefine refine(octDA, scalingFactor, &idata, geoms);
      //ot::DA *newDA = refine.getRefineDA(getNodeWeight);
      ot::DA *newDA = NULL;
      if (idata.surface_cost == 1000) {
        newDA = refine.getRefineDA(getNodeWeight1000);
      } else if (idata.surface_cost == 1500) {
        newDA = refine.getRefineDA(getNodeWeight1500);
      } else if (idata.surface_cost == 2000) {
        newDA = refine.getRefineDA(getNodeWeight2000);
      } else if (idata.surface_cost == 2500) {
        newDA = refine.getRefineDA(getNodeWeight2500);
      } else if (idata.surface_cost == 3000) {
        newDA = refine.getRefineDA(getNodeWeight3000);
      } else if (idata.surface_cost == 4000) {
        newDA = refine.getRefineDA(getNodeWeight4000);
      } else if (idata.surface_cost == 5000) {
        newDA = refine.getRefineDA(getNodeWeight5000);
      } else if (idata.surface_cost == 6000) {
        newDA = refine.getRefineDA(getNodeWeight6000);
      } else if (idata.surface_cost == 7000) {
        newDA = refine.getRefineDA(getNodeWeight7000);
      } else if (idata.surface_cost == 8000) {
        newDA = refine.getRefineDA(getNodeWeight8000);
      } else if (idata.surface_cost == 9000) {
        newDA = refine.getRefineDA(getNodeWeight9000);
      } else if (idata.surface_cost == 10000) {
        newDA = refine.getRefineDA(getNodeWeight10000);
      } else if (idata.surface_cost == 12000) {
        newDA = refine.getRefineDA(getNodeWeight12000);
      } else if (idata.surface_cost == 14000) {
        newDA = refine.getRefineDA(getNodeWeight14000);
      } else if (idata.surface_cost == 16000) {
        newDA = refine.getRefineDA(getNodeWeight16000);
      } else if (idata.surface_cost == 18000) {
        newDA = refine.getRefineDA(getNodeWeight18000);
      } else if (idata.surface_cost == 20000) {
        newDA = refine.getRefineDA(getNodeWeight20000);
      } else if (idata.surface_cost == 25000) {
        newDA = refine.getRefineDA(getNodeWeight25000);
      } else {
        PrintStatus("not allowed test ");
        exit(1);
      }
      if (newDA != octDA) {
        delete octDA;
      } else {
        // if the mesh is the same, break out.
        break;
      }
      octDA = newDA;
      PrintStatus(++iterOfRefinement, " iteration of refinement, element: ", octDA->getTotalElemSz());
    }
    PrintStatus("Refinement Done!");
  } else {
    // Checkpoint
    std::vector<VecInfo> vecs;
    Checkpoint.loadFromCheckPointing(octDA, vecs, &ti, geoms);
    if(octDA->isActive()) {
      prev_solution_ns = vecs[0].v;
      prev_solution_ht = vecs[1].v;
    }
  }

  auto ibmBC = [&geoms](unsigned int dof) {
    if (dof == NSHTNodeData::TEMPERATURE) {
      return 1.0;
    }
    return 0.0;
  };

  /// create ibmsolver, using DA, geoms, and input data
  auto ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);

  if(idata.resetVecs and resume_from_checkpoint){

    if(octDA->isActive()) {
      resetVecs(octDA, prev_solution_ns, NSHTNodeData::NS_DOF, ibm, scalingFactor);
      resetVecs(octDA, prev_solution_ht, NSHTNodeData::HT_DOF, ibm, scalingFactor);

    }
    std::vector<VecInfo> vecs(2);
    vecs[0].v = prev_solution_ns;
    vecs[1].v = prev_solution_ht;
    getRefinedDAafterCheckPointing(octDA,&idata,scalingFactor,geoms,vecs);
    if(octDA->isActive()) {
      prev_solution_ns = vecs[0].v;
      prev_solution_ht = vecs[1].v;
    }
    vecs[0].nodeDataIndex = NSHTNodeData::NUM_VARS;
    vecs[1].nodeDataIndex = NSHTNodeData::NUM_VARS + NSHTNodeData::TEMPERATURE;
    vecs[0].ndof = NSHTNodeData::NS_DOF;
    vecs[1].ndof = NSHTNodeData::HT_DOF;
    PrintStatus("Storing new checkpoint");
    Checkpoint.storeCheckpoint(octDA, vecs, &ti, geoms);
    delete ibm;
    ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);

  }
  else if(resume_from_checkpoint){
    std::vector<VecInfo> vecs(2);
    vecs[0].v = prev_solution_ns;
    vecs[1].v = prev_solution_ht;
    getRefinedDAafterCheckPointing(octDA,&idata,scalingFactor,geoms,vecs);
    if(octDA->isActive()) {
      prev_solution_ns = vecs[0].v;
      prev_solution_ht = vecs[1].v;
    }
    vecs[0].nodeDataIndex = NSHTNodeData::NUM_VARS;
    vecs[1].nodeDataIndex = NSHTNodeData::NUM_VARS + NSHTNodeData::TEMPERATURE;
    vecs[0].ndof = NSHTNodeData::NS_DOF;
    vecs[1].ndof = NSHTNodeData::HT_DOF;

    delete ibm;
    ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);
  }



  if (octDA->isActive()) {
    ibm->partitionGeometries();
  }

  /// Handle the gauss point quadrature
  std::vector<unsigned int> gpRelOrder;
  octDA->createVector(gpRelOrder, true, true, 1);
  std::fill(gpRelOrder.begin(), gpRelOrder.end(), 0);
  if (octDA->isActive()) {
    for (octDA->init<ot::DA_FLAGS::WRITABLE>();
         octDA->curr() < octDA->end<ot::DA_FLAGS::WRITABLE>(); octDA->next<ot::DA_FLAGS::WRITABLE>()) {
      if (idata.gp_handle != VariableGP::NoChange) {
        // two possibilities, 1. splitting, 2. increase order
        if (ibm->getInOutElementNODE(octDA->curr()) == INTERCEPTED_NODE) {
          if (idata.gp_handle == VariableGP::RefineBySplitting) {
            gpRelOrder[octDA->curr()] = VariableGP::RefineBySplitting;
          } else if (idata.gp_handle == VariableGP::IncreasedRelativeOrder) {
            gpRelOrder[octDA->curr()] = idata.gp_rel_order;
          }
        } else {
          assert(ibm->getInOutElementGP(octDA->curr()) != INVALID);
        }
      }
    }
  }

  auto htEq =
      new TalyEquation<HTEquation, NSHTNodeData>(octDA, d_min, d_max, NSHTNodeData::HT_DOF, true, &idata, ibm);
  auto htMat = htEq->mat;
  auto htVec = htEq->vec;
  htMat->setTime(&ti);
  htVec->setTime(&ti);
  htEq->setGaussPointVector(gpRelOrder);

  auto utils_ht = new IBMFEUtils<HTEquation, NSHTNodeData>(ibm, idata.gp_max_splitting, octDA);
  htMat->setFEUtils(utils_ht);
  htVec->setFEUtils(utils_ht);

  auto nsEq =
      new TalyEquation<NSEquation, NSHTNodeData>(octDA, d_min, d_max, NSHTNodeData::NS_DOF, false, &idata, ibm);
  auto nsMat = nsEq->mat;
  auto nsVec = nsEq->vec;
  nsMat->setTime(&ti);
  nsVec->setTime(&ti);
  nsEq->setGaussPointVector(gpRelOrder);

  auto utils_ns = new IBMFEUtils<NSEquation, NSHTNodeData>(ibm, idata.gp_max_splitting, octDA);
  nsMat->setFEUtils(utils_ns);
  nsVec->setFEUtils(utils_ns);

  auto HTSolver = setIBMLinearSolver<HTEquation, NSHTNodeData>(htEq, octDA, geoms, ibm, NSHTNodeData::HT_DOF, false);
  auto
      NSSolver =
      setIBMNonLinearSolver<NSEquation, NSHTNodeData>(nsEq, octDA, geoms, ibm, NSHTNodeData::NS_DOF, false);

  if (!resume_from_checkpoint) {
    octDA->petscCreateVector(prev_solution_ht, false, false, NSHTNodeData::HT_DOF);
    octDA->petscCreateVector(prev_solution_ns, false, false, NSHTNodeData::NS_DOF);
  }


//  LinearSolver *HTSolver = setLinearSolver(htEq, octDA, NSHTNodeData::HT_DOF, false);
//  NonlinearSolver *NSSolver = setNonLinearSolver(nsEq, octDA, NSHTNodeData::NS_DOF, false);

  const auto resetDAVecs = [&] {
    if (octDA->isActive()) {
      htMat->setVector({
                           // connect prev_solution_ht to NSHTNodeData's TEMPERATURE_PRE value (read-only)
                           VecInfo(prev_solution_ht,
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::NUM_VARS + NSHTNodeData::TEMPERATURE),
                           VecInfo(HTSolver->getCurrentSolution(),
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::TEMPERATURE),
                           VecInfo(NSSolver->getCurrentSolution(), NSHTNodeData::NS_DOF, NSHTNodeData::VEL_X),
                       });
      htVec->setVector({
                           // connect prev_solution_ht to NSHTNodeData's TEMPERATURE_PRE value (read-only)
                           VecInfo(prev_solution_ht,
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::NUM_VARS + NSHTNodeData::TEMPERATURE),
                           VecInfo(HTSolver->getCurrentSolution(),
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::TEMPERATURE),
                           VecInfo(NSSolver->getCurrentSolution(), NSHTNodeData::NS_DOF, NSHTNodeData::VEL_X),
                       });

      nsMat->setVector({
                           VecInfo(PLACEHOLDER_GUESS, NSHTNodeData::NS_DOF, NSHTNodeData::VEL_X),
                           VecInfo(prev_solution_ns,
                                   NSHTNodeData::NS_DOF,
                                   NSHTNodeData::NUM_VARS + NSHTNodeData::VEL_X,
                                   PLACEHOLDER_NONE),
                           VecInfo(HTSolver->getCurrentSolution(),
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::TEMPERATURE),
                       });
      nsVec->setVector({
                           VecInfo(PLACEHOLDER_GUESS, NSHTNodeData::NS_DOF, NSHTNodeData::VEL_X),
                           VecInfo(prev_solution_ns,
                                   NSHTNodeData::NS_DOF,
                                   NSHTNodeData::NUM_VARS + NSHTNodeData::VEL_X,
                                   PLACEHOLDER_NONE),
                           VecInfo(HTSolver->getCurrentSolution(),
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::TEMPERATURE),
                       });

      {
        KSP m_ksp = HTSolver->ksp();
        KSPSetOptionsPrefix(m_ksp, "ht_");
        KSPSetFromOptions(m_ksp);
        SNES m_snes = NSSolver->snes();
        SNESSetOptionsPrefix(m_snes, "ns_");
        SNESSetFromOptions(m_snes);
      }
    }

  };

  resetDAVecs();

  /// set B.C. for HT & NS
  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Setting BC for HT");
  NSHTBCSetup bc(&idata, &ti);
  HTSolver->setBoundaryCondition([&](double x, double y, double z, unsigned int nodeID) -> Boundary {
    Boundary b;
    ZEROPTV position = {x, y, z};
    bc.setBoundaryConditionsHT(b, position, idata.boundary_def);
    return b;
  });

  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Setting BC for NS");
  NSSolver->setBoundaryCondition([&](double x, double y, double z, unsigned int nodeID) -> Boundary {
    Boundary b;
    ZEROPTV position = {x, y, z};
    bc.setBoundaryConditionsNS(b, position, idata.boundary_def);
    return b;
  });

  if (!resume_from_checkpoint) {
    MPI_Barrier(MPI_COMM_WORLD);
    PrintStatus("Setting IC for NS");
    const auto initial_condition_ns = [&](double x, double y, double z, int dof) {
      //  return vel, default is 0
      if (dof != NSHTNodeData::PRESSURE) {
//        return dof * 1.5 + x + y + z;
        return idata.ic_def.vel_ic[dof];
      }
      // return pressure, default is 0
//      return x*y*z;
      return idata.ic_def.p_ic;
    };
    MPI_Barrier(MPI_COMM_WORLD);
    PrintStatus("Setting IC for HT");
    const auto initial_condition_ht = [&](double x, double y, double z, int dof) {
//      return sin(M_PI * x/4.) *  sin(M_PI * y/4.) * sin(M_PI * z/4.);
//      return x*y*z + 100;
      // Only for regression test
      if (dump_vec && idata.boundary_def[1].temperature_type == BoundaryDef::Temperature_Type::ROBIN_T) {
        return 100 * (1.0 - x / 3);
      }
      return idata.ic_def.t_ic;
    };
    petscSetInitialCondition(octDA, prev_solution_ns, NSHTNodeData::NS_DOF, initial_condition_ns, d_min, d_max);
    petscSetInitialCondition(octDA, prev_solution_ht, NSHTNodeData::HT_DOF, initial_condition_ht, d_min, d_max);
    MPI_Barrier(MPI_COMM_WORLD);
    PrintStatus("before save IC");
    if(idata.SolveNS) {
      save_timestep(octDA, prev_solution_ns, NSHTNodeData::NS_DOF, ti, *ibm, "ns", scalingFactor, nsname, false);
    }
    if(idata.SolveHT) {
      save_timestep(octDA, prev_solution_ht, NSHTNodeData::HT_DOF, ti, *ibm, "ht", scalingFactor, htname, false);
    }
  } else {
    PrintStatus("Reload IC from checkpoint!");
  }
  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("after save IC");
  ///initial condition copied in the current solution vector.
  if (octDA->isActive()) {
    VecCopy(prev_solution_ns, NSSolver->getCurrentSolution());
    VecCopy(prev_solution_ht, HTSolver->getCurrentSolution());
  }
  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Starting solving \n");

  const int iterMaxBlock = idata.iterMaxBlock;
  const double blockTolerance = idata.blockTolerance;
  Vec block_ht, block_ns;
  DENDRITE_UINT localElemSz = octDA->getLocalElemSz();
  DENDRITE_UINT totalElem;
  MPI_Reduce(&localElemSz,&totalElem,1,MPI_UINT32_T,MPI_SUM,0,octDA->getCommActive());
  PrintStatus("Total number of elements = ",totalElem);
  PetscInt globalDof;
  VecGetSize(prev_solution_ns,&globalDof);
  PrintStatus("Total number of dof = ",globalDof);

  VecGetLocalSize(prev_solution_ns,&globalDof);
  PrintStatus("Total number of dof = ",globalDof);

  while (ti.getCurrentTime() < (ti.getEndTime() - 1e-16)) {




    int iterCurrentBlock = 0;
    double errorCurrentBlock = 1e16;
    PrintInfo("\nTimestep ", ti.getTimeStepNumber(), " - ", ti.getCurrentTime(), " -> Current Diffusion = " ,idata.getDiffusionNS(ti.getCurrentTime()));
    octDA->petscCreateVector(block_ht, false, false, NSHTNodeData::HT_DOF);
    octDA->petscCreateVector(block_ns, false, false, NSHTNodeData::NS_DOF);
    if(octDA->isActive()) {
      while (iterCurrentBlock < iterMaxBlock && errorCurrentBlock > blockTolerance) {

        VecCopy(NSSolver->getCurrentSolution(), block_ns);
        VecCopy(HTSolver->getCurrentSolution(), block_ht);

        if (idata.SolveHT) {
          PrintStatus("Solve HT");
          HTSolver->solve();
        }
        if (idata.SolveNS) {
          PrintStatus("Solve NS");
          NSSolver->solve();
        }
        if (idata.SolveNS && idata.SolveHT) {
          double errHT =
              calc_error(HTSolver->getCurrentSolution(), block_ht, NSHTNodeData::HT_DOF, octDA->getCommActive());
          double errNS =
              calc_error(NSSolver->getCurrentSolution(), block_ns, NSHTNodeData::NS_DOF, octDA->getCommActive());
          errorCurrentBlock = std::max(errHT, errNS);
          PrintInfo("Block ", iterCurrentBlock + 1, " error = ", errorCurrentBlock);
        } else {
          errorCurrentBlock = -1;
        }

        /// Copy current solution to block solution
        iterCurrentBlock++;

      }
    }

    if(octDA->isActive()) {
      VecCopy(HTSolver->getCurrentSolution(), prev_solution_ht);
      VecCopy(NSSolver->getCurrentSolution(), prev_solution_ns);
      VecDestroy(&block_ht);
      VecDestroy(&block_ns);
    }


    if(octDA->isActive()) {
      PetscScalar maxDof[4];
      PetscScalar minDof[4];
      PetscScalar globalMax[4];
      PetscScalar globalMin[4];
      const double *array;
      int size;
      VecGetLocalSize(prev_solution_ns,&size);
      int eachDofSize = static_cast<int>(std::round(size/4));
      VecGetArrayRead(prev_solution_ns, &array);
      maxDof[0] = *std::max_element(&array[eachDofSize*NSHTNodeData::VEL_X],&array[eachDofSize*(NSHTNodeData::VEL_X + 1)]);
      maxDof[1] = *std::max_element(&array[eachDofSize*NSHTNodeData::VEL_Y],&array[eachDofSize*(NSHTNodeData::VEL_Y + 1)]);
      maxDof[2] = *std::max_element(&array[eachDofSize*NSHTNodeData::VEL_Z],&array[eachDofSize*(NSHTNodeData::VEL_Z + 1)]);
      maxDof[3] = *std::max_element(&array[eachDofSize*NSHTNodeData::PRESSURE],&array[eachDofSize*(NSHTNodeData::PRESSURE + 1)]);

      minDof[0] = *std::min_element(&array[eachDofSize*NSHTNodeData::VEL_X],&array[eachDofSize*(NSHTNodeData::VEL_X + 1)]);
      minDof[1] = *std::min_element(&array[eachDofSize*NSHTNodeData::VEL_Y],&array[eachDofSize*(NSHTNodeData::VEL_Y + 1)]);
      minDof[2] = *std::min_element(&array[eachDofSize*NSHTNodeData::VEL_Z],&array[eachDofSize*(NSHTNodeData::VEL_Z + 1)]);
      minDof[3] = *std::min_element(&array[eachDofSize*NSHTNodeData::PRESSURE],&array[eachDofSize*(NSHTNodeData::PRESSURE + 1)]);
      MPI_Reduce(maxDof,globalMax,4,MPI_DOUBLE,MPI_MAX,0,octDA->getCommActive());
      MPI_Reduce(minDof,globalMin,4,MPI_DOUBLE,MPI_MIN,0,octDA->getCommActive());

      PrintStatus("Max Values = ",globalMax[0]," " , globalMax[1], " " ,globalMax[2], " ", globalMax[3]);
      PrintStatus("Min Values = ",globalMin[0]," " , globalMin[1], " " ,globalMin[2], " ", globalMin[3]);




      VecRestoreArrayRead(prev_solution_ns,&array);
    }

    // interpolate onto surface
    std::vector<VecInfo> vecs;
    vecs.push_back(VecInfo(prev_solution_ns,
                           NSHTNodeData::NS_DOF,
                           NSHTNodeData::NUM_VARS + NSHTNodeData::VEL_X,
                           PLACEHOLDER_NONE));
    vecs.push_back(VecInfo(prev_solution_ht,
                           NSHTNodeData::HT_DOF,
                           NSHTNodeData::NUM_VARS + NSHTNodeData::TEMPERATURE,
                           PLACEHOLDER_NONE));

    // interpolate field pressure onto geometry
    ibm->computeSurfaceValue(vecs[0], NSHTNodeData::PRESSURE);
    // interpolate field temperature onto geometry
    ibm->computeSurfaceValue(vecs[1], 0, NSHTNodeData::TEMPERATURE);

    // HT postprocessing
    if (idata.SolveHT) {
      /// Calc the heat flux of an object
      HeatPostProcess HTP(&idata, ibm, htEq, ti);
      HTP.calcNu(*octDA, scalingFactor);
      HTP.calcHeatFlux(*octDA, scalingFactor);
    }

    // NS postprocessing & storing force for moving
    std::vector<ZEROPTV> force(ibm->getGeometries().size());
    std::vector<ZEROPTV> torque(ibm->getGeometries().size());
    if (!ibm->getGeometries().empty()) {
      /// Calc the force and torque of an object
      NSPostProcess NSP(&idata, ibm, nsEq, ti);
      auto surface_force = NSP.calcForceObject(*octDA, scalingFactor);
      for (int i = 0; i < ibm->getGeometries().size(); i++) {
        force[i] = surface_force[i].Force_all() * (-1);
        if (geoms[i]->def_.if_buoyancy) {
          force[i] += idata.bodyforce_def.calcBodyForce(geoms[i]->pI.rho, geoms[i]->pI.volume);
        }
        torque[i] = surface_force[i].Torque * (-1);
      }
      NSP.PrintKinematicStatus();
    }

    ti.increment();
    if ((ti.getCurrentTime() >= idata.OutputStartTime) && (ti.getTimeStepNumber() % idata.OutputInterval == 0)) {
      if(idata.SolveNS) {
        save_timestep(octDA, prev_solution_ns, NSHTNodeData::NS_DOF, ti, *ibm, "ns", scalingFactor, nsname,false);
      }
      if(idata.SolveHT) {
        save_timestep(octDA, prev_solution_ht, NSHTNodeData::HT_DOF, ti, *ibm, "ht", scalingFactor, htname,false);
      }
      if (dump_vec == PETSC_TRUE) {
        petscDumpFilesforRegressionTest(octDA, prev_solution_ns, "ns_vec");
        petscDumpFilesforRegressionTest(octDA, prev_solution_ht, "ht_vec");
      }
      // checkpointing
      Checkpoint.storeCheckpoint(octDA, vecs, &ti, geoms);
    }
    bool geom_moved = false;
    for (int i = 0; i < ibm->getGeometries().size(); i++) {
      ibm->getGeometries()[i].geo->updateGeomKinematicInfo(force[i], torque[i], ti.getCurrentStep());
      // update geometry nodal velocity based on their kinematic info
//        ibm->getGeometries()[i].geo->updateNodeVel();
      geom_moved = geom_moved or (not(ibm->getGeometries()[i].geo->def_.is_static));
    };

    if (geom_moved) {
      /// freshly out nodes and modify
      std::vector<localFreshlyOut> localCleared;
      ibm->get_freshly_cleared(localCleared);

//      check_get_freshly_cleared(octDA, localCleared, scalingFactor);

      ibm->modify_freshly_cleared(localCleared, vecs);
      ibm->compute_freshly_cleared(localCleared, vecs);
      for (int i = 0; i < ibm->getGeometries().size(); i++) {
        ibm->getGeometries()[i].geo->updateNodeVel();
      }

      Vec out_ns, out_ht;
      PrintStatus("Remeshing - refine");
      while (true) {
        NSHTRefine refineMoving(octDA, scalingFactor, &idata, geoms);
        //ot::DA *newDA = refine.getRefineDA(getNodeWeight);
        ot::DA *newDA = NULL;
        if (idata.surface_cost == 1000) {
          newDA = refineMoving.getRefineDA(getNodeWeight1000);
        } else if (idata.surface_cost == 1500) {
          newDA = refineMoving.getRefineDA(getNodeWeight1500);
        } else if (idata.surface_cost == 2000) {
          newDA = refineMoving.getRefineDA(getNodeWeight2000);
        } else if (idata.surface_cost == 2500) {
          newDA = refineMoving.getRefineDA(getNodeWeight2500);
        } else if (idata.surface_cost == 3000) {
          newDA = refineMoving.getRefineDA(getNodeWeight3000);
        } else if (idata.surface_cost == 4000) {
          newDA = refineMoving.getRefineDA(getNodeWeight4000);
        } else if (idata.surface_cost == 5000) {
          newDA = refineMoving.getRefineDA(getNodeWeight5000);
        } else if (idata.surface_cost == 6000) {
          newDA = refineMoving.getRefineDA(getNodeWeight6000);
        } else if (idata.surface_cost == 7000) {
          newDA = refineMoving.getRefineDA(getNodeWeight7000);
        } else if (idata.surface_cost == 8000) {
          newDA = refineMoving.getRefineDA(getNodeWeight8000);
        } else if (idata.surface_cost == 9000) {
          newDA = refineMoving.getRefineDA(getNodeWeight9000);
        } else if (idata.surface_cost == 10000) {
          newDA = refineMoving.getRefineDA(getNodeWeight10000);
        } else if (idata.surface_cost == 12000) {
          newDA = refineMoving.getRefineDA(getNodeWeight12000);
        } else if (idata.surface_cost == 14000) {
          newDA = refineMoving.getRefineDA(getNodeWeight14000);
        } else if (idata.surface_cost == 16000) {
          newDA = refineMoving.getRefineDA(getNodeWeight16000);
        } else if (idata.surface_cost == 18000) {
          newDA = refineMoving.getRefineDA(getNodeWeight18000);
        } else if (idata.surface_cost == 20000) {
          newDA = refineMoving.getRefineDA(getNodeWeight20000);
        } else if (idata.surface_cost == 25000) {
          newDA = refineMoving.getRefineDA(getNodeWeight25000);
        } else {
          PrintStatus("not allowed test ");
          exit(1);
        }
        if (newDA == octDA) {
          break;
        }

        /// Perform the intergrid Transfer
        refineMoving.performIntergridTransfer(newDA, prev_solution_ns, out_ns, NSHTNodeData::NS_DOF);
        refineMoving.performVectorExchange(newDA, prev_solution_ns, out_ns, NSHTNodeData::NS_DOF);
        refineMoving.performIntergridTransfer(newDA, prev_solution_ht, out_ht, NSHTNodeData::HT_DOF);
        refineMoving.performVectorExchange(newDA, prev_solution_ht, out_ht, NSHTNodeData::HT_DOF);
        refineMoving.finalizeAMR(newDA);
        std::swap(octDA, newDA);
      }
      PrintStatus("Remeshing - refine done - coarsening");
      // coarsen one time only per timestep
      {
        NSHTRefine refineMoving(octDA, scalingFactor, &idata, geoms, true);
        //ot::DA *newDA = refineMoving.getRefineDA(getNodeWeight);
        ot::DA *newDA = NULL;
        if (idata.surface_cost == 1000) {
          newDA = refineMoving.getRefineDA(getNodeWeight1000);
        } else if (idata.surface_cost == 1500) {
          newDA = refineMoving.getRefineDA(getNodeWeight1500);
        } else if (idata.surface_cost == 2000) {
          newDA = refineMoving.getRefineDA(getNodeWeight2000);
        } else if (idata.surface_cost == 2500) {
          newDA = refineMoving.getRefineDA(getNodeWeight2500);
        } else if (idata.surface_cost == 3000) {
          newDA = refineMoving.getRefineDA(getNodeWeight3000);
        } else if (idata.surface_cost == 4000) {
          newDA = refineMoving.getRefineDA(getNodeWeight4000);
        } else if (idata.surface_cost == 5000) {
          newDA = refineMoving.getRefineDA(getNodeWeight5000);
        } else if (idata.surface_cost == 6000) {
          newDA = refineMoving.getRefineDA(getNodeWeight6000);
        } else if (idata.surface_cost == 7000) {
          newDA = refineMoving.getRefineDA(getNodeWeight7000);
        } else if (idata.surface_cost == 8000) {
          newDA = refineMoving.getRefineDA(getNodeWeight8000);
        } else if (idata.surface_cost == 9000) {
          newDA = refineMoving.getRefineDA(getNodeWeight9000);
        } else if (idata.surface_cost == 10000) {
          newDA = refineMoving.getRefineDA(getNodeWeight10000);
        } else if (idata.surface_cost == 12000) {
          newDA = refineMoving.getRefineDA(getNodeWeight12000);
        } else if (idata.surface_cost == 14000) {
          newDA = refineMoving.getRefineDA(getNodeWeight14000);
        } else if (idata.surface_cost == 16000) {
          newDA = refineMoving.getRefineDA(getNodeWeight16000);
        } else if (idata.surface_cost == 18000) {
          newDA = refineMoving.getRefineDA(getNodeWeight18000);
        } else if (idata.surface_cost == 20000) {
          newDA = refineMoving.getRefineDA(getNodeWeight20000);
        } else if (idata.surface_cost == 25000) {
          newDA = refineMoving.getRefineDA(getNodeWeight25000);
        } else {
          PrintStatus("not allowed test ");
          exit(1);
        }


        /// Perform the intergrid Transfer
//        octDA->petscVecTopvtu(prev_solution_ns,"originalHT", nullptr, false, false,1,scalingFactor);
        refineMoving.performIntergridTransfer(newDA, prev_solution_ns, out_ns, NSHTNodeData::NS_DOF);
        refineMoving.performVectorExchange(newDA, prev_solution_ns, out_ns, NSHTNodeData::NS_DOF);
//        newDA->petscVecTopvtu(prev_solution_ns,"afterHT", nullptr, false, false,1,scalingFactor);

        refineMoving.performIntergridTransfer(newDA, prev_solution_ht, out_ht, NSHTNodeData::HT_DOF);
        refineMoving.performVectorExchange(newDA, prev_solution_ht, out_ht, NSHTNodeData::HT_DOF);

        refineMoving.finalizeAMR(newDA);
        std::swap(octDA, newDA);
      }

      PrintStatus("Remeshing - coarsen done");

      auto boundary_NS = NSSolver->getBoundaryCondition();
      auto boundary_HT = HTSolver->getBoundaryCondition();

      delete htEq;
      delete nsEq;
      delete utils_ht;
      delete utils_ns;
      delete HTSolver;
      delete NSSolver;
      delete ibm;

      ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);
      if (octDA->isActive()) {
        ibm->partitionGeometries();
      }
      /// Handle the gauss point quadrature
      gpRelOrder.clear();
      octDA->createVector(gpRelOrder, true, true, 1);
      std::fill(gpRelOrder.begin(), gpRelOrder.end(), 0);
      if (octDA->isActive()) {
        for (octDA->init<ot::DA_FLAGS::WRITABLE>();
             octDA->curr() < octDA->end<ot::DA_FLAGS::WRITABLE>(); octDA->next<ot::DA_FLAGS::WRITABLE>()) {
          if (idata.gp_handle != VariableGP::NoChange) {
            // two possibilities, 1. splitting, 2. increase order
            if (ibm->getInOutElementNODE(octDA->curr()) == INTERCEPTED_NODE) {
              if (idata.gp_handle == VariableGP::RefineBySplitting) {
                gpRelOrder[octDA->curr()] = VariableGP::RefineBySplitting;
              } else if (idata.gp_handle == VariableGP::IncreasedRelativeOrder) {
                gpRelOrder[octDA->curr()] = idata.gp_rel_order;
              }
            } else {
              assert(ibm->getInOutElementGP(octDA->curr()) != INVALID);
            }
          }
        }
      }

      htEq = new TalyEquation<HTEquation, NSHTNodeData>(octDA, d_min, d_max, NSHTNodeData::HT_DOF, true, &idata, ibm);
      htMat = htEq->mat;
      htVec = htEq->vec;
      htMat->setTime(&ti);
      htVec->setTime(&ti);
      htEq->setGaussPointVector(gpRelOrder);

      utils_ht = new IBMFEUtils<HTEquation, NSHTNodeData>(ibm, idata.gp_max_splitting, octDA);
      htMat->setFEUtils(utils_ht);
      htVec->setFEUtils(utils_ht);

      nsEq =
          new TalyEquation<NSEquation, NSHTNodeData>(octDA, d_min, d_max, NSHTNodeData::NS_DOF, false, &idata, ibm);
      nsMat = nsEq->mat;
      nsVec = nsEq->vec;
      nsMat->setTime(&ti);
      nsVec->setTime(&ti);
      nsEq->setGaussPointVector(gpRelOrder);

      utils_ns = new IBMFEUtils<NSEquation, NSHTNodeData>(ibm, idata.gp_max_splitting, octDA);
      nsMat->setFEUtils(utils_ns);
      nsVec->setFEUtils(utils_ns);

      HTSolver = setIBMLinearSolver<HTEquation, NSHTNodeData>(htEq, octDA, geoms, ibm, NSHTNodeData::HT_DOF, false);
      NSSolver =
          setIBMNonLinearSolver<NSEquation, NSHTNodeData>(nsEq, octDA, geoms, ibm, NSHTNodeData::NS_DOF, false);

      NSSolver->setBoundaryCondition(boundary_NS);
      HTSolver->setBoundaryCondition(boundary_HT);
      if (octDA->isActive()) {
        VecCopy(prev_solution_ht, HTSolver->getCurrentSolution());
        VecCopy(prev_solution_ns, NSSolver->getCurrentSolution());
      }
      resetDAVecs();
    }

  }

  PrintStatus("Done solving \n");
  delete HTSolver;
  delete NSSolver;

  dendrite_finalize(octDA);

}
