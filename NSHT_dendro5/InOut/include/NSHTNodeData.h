/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#pragma once

#include <exception>



class NSHTNodeData {
 public:

  /// NS degrees of freedom
  static const int NS_DOF = 4;
  /// HT degrees of freedom
  static const unsigned int HT_DOF = 1;
  /// number of  variables in the NSHTNodeData
  static const unsigned int NUM_VARS = NS_DOF + HT_DOF;

  /// Store the values of the degrees of freedom at the current timestep
  double u[NUM_VARS];
  /// Store the values at the degrees of freedom at the previous timestep
  double u_n[NUM_VARS];
  /// Store the values at the degrees of freedom at the block iteration
  double u_block[NUM_VARS];

  enum Vars {
    VEL_X = 0,
    VEL_Y = 1,
    VEL_Z = 2,
    PRESSURE = 3,
    TEMPERATURE = 4,
  };

  NSHTNodeData() {
    for (int i = 0; i < NUM_VARS; i++) {
      u[i] = 0.0;
      u_n[i] = 0.0;
      u_block[i] = 0.0;
    }
  }

  /**
   * Returns reference to the given value in the object
   *
   * @param index the index of the desired item
   * @return reference to the desired data item
   */
  inline double &value(int index) {
    assert (index >= 0 && index < NUM_VARS * 3);
    if (index >= 0 && index < NUM_VARS) {
      return u[index];
    }
    /// If the index is greater than NUM_VARS then assign the numbers to u_n
    if (index >= NUM_VARS && index < NUM_VARS * 2) {
      return u_n[index - NUM_VARS];
    }
    /// If the index is greater than NUM_VARS * 2 then assign the numbers to u_block
    if (index >= 2 * NUM_VARS && index < NUM_VARS * 3) {
      return u_block[index - NUM_VARS * 2];
    }
    TALYFEMLIB::TALYException() << "Invalid variable index!";
  }

  inline double value(int index) const {
    return const_cast<NSHTNodeData *>(this)->value(index);
  }

  /**
   * Returns the name of the given data value in the object
   * @param index the index of the desired item nsame
   * @return name of the specified data item
   */
  static const char *name(int index) {
    switch (index) {
      case VEL_X: return "vel_x";
      case VEL_Y: return "vel_y";
      case VEL_Z: return "vel_z";
      case PRESSURE: return "pressure";
      case TEMPERATURE: return "temperature";
      default: throw std::runtime_error("Invalid NSHTNodeData index");
    }
    return nullptr;
  }

  /**
   * Returns the number of the data items in the object
   * @return number of the data items in the object
   */
  static int valueno() {
    return NUM_VARS;
  }
};
