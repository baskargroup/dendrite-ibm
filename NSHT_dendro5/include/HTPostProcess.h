#pragma once

#include "HTEquation.h"
#include "NSHTNodeData.h"
#include "NSHTInputData.h"
#include "IBM/IBMSolver.h"

/**
 * Class responsible for doing post-processing work for HT equation
 */
class HeatPostProcess {
 public:
  HeatPostProcess(NSHTInputData *inputData,
                  IBMSolver<NSHTNodeData> *ibmSolver,
                  TalyEquation<HTEquation, NSHTNodeData> *talyEq,
                  const TimeInfo& ti);

  /// functions to calc heat flux or Nu
  void calcHeatFlux(ot::DA &m_octDA,
                    const double *daScalingFactor);

  void calcNu(ot::DA &m_octDA,
              const double *daScalingFactor);

  void PrintHeatFluxInfo();
  void PrintNuInfo();
 private:
  // heat flux
  std::vector<ZEROPTV> calcFluxElementIBM(const FEMElm &fe,
                                          Geom<NSHTNodeData> *geo,
                                          const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                          const Point &node,
                                          const Point &h);
  std::vector<ZEROPTV> calcFluxElementSurf(const FEMElm &fe);
  std::vector<ZEROPTV> calcHeatFluxPerMpiIBM(ot::DA &da,
                                             const double *daScalingFactor,
                                             const IBMSolver<NSHTNodeData>::GeomData &geom_data,
                                             bool visitGhostElements = false);
  std::vector<ZEROPTV> calcHeatFluxPerMpiSurf(ot::DA &da,
                                              const double *daScalingFactor,
                                              bool visitGhostElements = false);
  // nusselt
  std::vector<double> calcNuElementIBM(const FEMElm &fe,
                                       Geom<NSHTNodeData> *geo,
                                       const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                       const Point &node,
                                       const Point &h);
  std::vector<double> calcNuElementSurf(const FEMElm &fe);
  std::vector<double> calcNuPerMpiIBM(ot::DA &da,
                                      const double *daScalingFactor,
                                      const IBMSolver<NSHTNodeData>::GeomData &geom_data,
                                      bool visitGhostElements = false);
  std::vector<double> calcNuPerMpiSurf(ot::DA &da,
                                       const double *daScalingFactor,
                                       bool visitGhostElements = false);
  /// member variables
  const NSHTInputData *idata_;
  unsigned int surface2cal_ = -1;
  TalyEquation<HTEquation, NSHTNodeData> *talyEq_;
  TimeInfo ti_;
  IBMSolver<NSHTNodeData> *ibmSolver_;
  std::vector<std::vector<double>> Nu_ibm;
  std::vector<std::vector<double>> Nu_surface;
  std::vector<std::vector<ZEROPTV>> Flux_ibm;
  std::vector<std::vector<ZEROPTV>> Flux_surface;

};

HeatPostProcess::HeatPostProcess(NSHTInputData *inputData,
                                 IBMSolver<NSHTNodeData> *ibmSolver,
                                 TalyEquation<HTEquation, NSHTNodeData> *talyEq,
                                 const TimeInfo& ti)
    : idata_(inputData), ibmSolver_(ibmSolver), talyEq_(talyEq), ti_(ti) {
}

std::vector<ZEROPTV> HeatPostProcess::calcFluxElementIBM(const FEMElm &fe,
                                                         Geom<NSHTNodeData> *geo,
                                                         const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                                         const Point &node,
                                                         const Point &h) {
  const int nsd = idata_->nsd;
  const double Cb_e = idata_->Cb_e;
  const double detJxW = fe.detJxW();
  double Coe_theta = idata_->getDiffusionHT(ti_.getCurrentTime());

//  GeomDef::BCType bcType = geo->def_.bc_type;
//  double T = geo->def_.dirichlet;
//  if (!geo->def_.bc_type_V.empty()) {
    GeomDef::BCType  bcType = geo->def_.bc_type_V[NSHTNodeData::TEMPERATURE];
    assert(bcType != GeomDef::BCType::INVALID_BC);
    std::vector<double> bc_value = geo->def_.getBC(NSHTNodeData::TEMPERATURE);
    double T = bc_value[0];
//  }

  ZeroMatrix<double> ksiX;
  ksiX.redim(nsd, nsd);

  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      ksiX(i, j) = fe.cof(j, i) / fe.jacc(); // This is correct, we need the volume jacabion
    }
  }

  ZeroMatrix<double> Ge;
  Ge.redim(nsd, nsd);

  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      Ge(i, j) = 0.0;
      for (int k = 0; k < nsd; k++)
        Ge(i, j) += ksiX(k, i) * ksiX(k, j);
    }
  }

  double n_Gn = 0.0;
  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      n_Gn += gpinfo.gp_normal(j) * Ge(j, i) * gpinfo.gp_normal(i);
    }
  }
  // wall normal distance
  double hb = 2.0 / sqrt(n_Gn);

  /// Thermal conductivity is assumed to be the same with thermal diffusivity
  ZEROPTV dtheta;
  for (int i = 0; i < nsd; i++) {
    dtheta(i) = talyEq_->field()->valueDerivativeFEM(fe, NSHTNodeData::TEMPERATURE, i);
  }

  double theta = talyEq_->field()->valueFEM(fe, NSHTNodeData::TEMPERATURE);

  /// Heat Flux Vector
  ZEROPTV phi_normal;
  ZEROPTV phi_pen;
  for (int dim = 0; dim < nsd; dim++) {
    phi_normal(dim) = -Coe_theta * gpinfo.gp_normal(dim) * dtheta(dim) * detJxW;
    phi_pen(dim) = (Coe_theta * Cb_e / hb) * (theta - T) * detJxW;
  }
  std::vector<ZEROPTV> phi = {phi_normal, phi_pen};
  return phi;

}

std::vector<ZEROPTV> HeatPostProcess::calcFluxElementSurf(const FEMElm &fe) {
  const int nsd = idata_->nsd;
  const double Cb_e = idata_->Cb_e;
  const double detJxW = fe.detJxW();
  double Coe_theta = idata_->getDiffusionHT(ti_.getCurrentTime());

  ZeroMatrix<double> ksiX;
  ksiX.redim(nsd, nsd);

  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      ksiX(i, j) = fe.cof(j, i) / fe.jacc(); // This is correct, we need the volume jacabion
    }
  }

  ZeroMatrix<double> Ge;
  Ge.redim(nsd, nsd);

  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      Ge(i, j) = 0.0;
      for (int k = 0; k < nsd; k++) {
        Ge(i, j) += ksiX(k, i) * ksiX(k, j);
      }
    }
  }

  double n_Gn = 0.0;
  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      n_Gn += fe.surface()->normal()(j) * Ge(j, i) * fe.surface()->normal()(i);
    }
  }
  // wall normal distance
  double hb = 2.0 / sqrt(n_Gn);

  /// Thermal conductivity is assumed to the the same with thermal diffusivity
  ZEROPTV dtheta;
  for (int i = 0; i < nsd; i++) {
    dtheta(i) = talyEq_->field()->valueDerivativeFEM(fe, NSHTNodeData::TEMPERATURE, i);
  }

  double theta = talyEq_->field()->valueFEM(fe, NSHTNodeData::TEMPERATURE);

  /// Heat Flux Vector
  ZEROPTV phi_normal, phi_pen;
  for (int dim = 0; dim < nsd; dim++) {
    phi_normal(dim) = -Coe_theta * fe.surface()->normal()(dim) * dtheta(dim) * detJxW;
  }

  for (int dim = 0; dim < nsd; dim++) {
    if (idata_->boundary_def[surface2cal_ - 1].temperature_type == BoundaryDef::WEAK) {
      double T = idata_->boundary_def[surface2cal_ - 1].getRamping(ti_.getCurrentTime(),
                                                                   idata_->boundary_def[surface2cal_ - 1].temperature);
      /// penalty term
      phi_pen(dim) = (Coe_theta * Cb_e / hb) * (theta - T) * detJxW;
    }
  }

  std::vector<ZEROPTV> phi = {phi_normal, phi_pen};
  return phi;

}

std::vector<double> HeatPostProcess::calcNuElementIBM(const FEMElm &fe,
                                                      Geom<NSHTNodeData> *geo,
                                                      const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                                      const Point &node,
                                                      const Point &h) {
  const int nsd = idata_->nsd;
  const double Cb_e = idata_->Cb_e;
  const double detSideJxW = fe.detJxW();
  const ZEROPTV &normal = gpinfo.gp_normal;
//  GeomDef::BCType bcType = geo->def_.bc_type;
//  double T = geo->def_.dirichlet;
//  if (!geo->def_.bc_type_V.empty()) {
    GeomDef::BCType bcType = geo->def_.bc_type_V[NSHTNodeData::TEMPERATURE];
    assert(bcType != GeomDef::BCType::INVALID_BC);
    std::vector<double> bc_value = geo->def_.getBC(NSHTNodeData::TEMPERATURE);
    double T = bc_value[0];
//  }

  // wall normal distance
  double hb = MathOp::normalDistance(fe.position(), normal, node, h);

  /// Thermal conductivity is assumed to be the same with thermal diffusivity
  ZEROPTV dtheta;
  for (int i = 0; i < nsd; i++) {
    dtheta(i) = talyEq_->field()->valueDerivativeFEM(fe, NSHTNodeData::TEMPERATURE, i);
  }
  double theta = talyEq_->field()->valueFEM(fe, NSHTNodeData::TEMPERATURE);
  double flux = 0.0;
  for (int i = 0; i < nsd; i++) {
    flux += dtheta(i) * normal(i);
  }
  double Nu_element = 0.0;
  double Nu_element_pen = 0.0;
  Nu_element = flux * detSideJxW;

  // penalty
  Nu_element_pen = -(Cb_e / hb) * (theta - T) * detSideJxW;

  std::vector<double> all = {Nu_element, Nu_element_pen};
  return all;

}

std::vector<double> HeatPostProcess::calcNuElementSurf(const FEMElm &fe) {
  const int nsd = idata_->nsd;
  const double Cb_e = idata_->Cb_e;
  const double detJxW = fe.detJxW();

  ZeroMatrix<double> ksiX;
  ksiX.redim(nsd, nsd);

  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      ksiX(i, j) = fe.cof(j, i) / fe.jacc(); // This is correct, we need the volume jacabion
    }
  }

  ZeroMatrix<double> Ge;
  Ge.redim(nsd, nsd);

  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      Ge(i, j) = 0.0;
      for (int k = 0; k < nsd; k++) {
        Ge(i, j) += ksiX(k, i) * ksiX(k, j);
      }
    }
  }

  double n_Gn = 0.0;
  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      n_Gn += fe.surface()->normal()(j) * Ge(j, i) * fe.surface()->normal()(i);
    }
  }
  // wall normal distance
  double hb = 2.0 / sqrt(n_Gn);

  /// Thermal conductivity is assumed to the the same with thermal diffusivity
  ZEROPTV dtheta;
  for (int i = 0; i < nsd; i++) {
    dtheta(i) = talyEq_->field()->valueDerivativeFEM(fe, NSHTNodeData::TEMPERATURE, i);
  }
  double theta = talyEq_->field()->valueFEM(fe, NSHTNodeData::TEMPERATURE);

  double flux = 0.0;
  for (int i = 0; i < nsd; i++) {
    flux += dtheta(i) * fe.surface()->normal()(i);
  }

  double Nu_element = flux * detJxW;
  double Nu_penalty = 0.0;
  /// weakBC penalty
  if (idata_->boundary_def[surface2cal_ - 1].temperature_type == BoundaryDef::WEAK) {
    double T = idata_->boundary_def[surface2cal_ - 1].getRamping(ti_.getCurrentTime(),
                                                                 idata_->boundary_def[surface2cal_ - 1].temperature);
    Nu_penalty += -(Cb_e / hb) * (theta - T) * detJxW;
  }

  return std::vector<double>{Nu_element, Nu_penalty};

}

std::vector<ZEROPTV> HeatPostProcess::calcHeatFluxPerMpiIBM(ot::DA &da,
                                                            const double *daScalingFactor,
                                                            const IBMSolver<NSHTNodeData>::GeomData &geom_data,
                                                            bool visitGhostElements) {
  const unsigned int ndof = NSHTNodeData::HT_DOF;
  auto syncVal = talyEq_->mat->getSyncValuesVec();
  int npe = da.getNumNodesPerElement();
  int eleOrder = da.getElementOrder();

  DENDRITE_REAL **_val_;
  DENDRITE_REAL *coords;
  std::vector<ZEROPTV> nodal_pos(npe);
  coords = new DENDRITE_REAL[m_uiDim * npe];
  _val_ = new DENDRITE_REAL *[syncVal.size()];
  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
  }
  talyEq_->mat->preMat();
  TalyDendroSync sync;

  /// sum of all the heat fluxs in all the elements,
  int counter = 0;
  std::vector<ZEROPTV> Flux_sum(2);

  /** Looping over all the background element**/
  ZEROPTV ptvl; /// Local point in isoparameteric space
  ZEROPTV ptvg; /// Gauss point
  for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size(); gpID++) {

    if (geom_data.my_gauss_points[gpID].isLocal == true) {
      counter++;
      /// get element coordinates and data
      unsigned int eleID = geom_data.backgroundElementGP[gpID];

      da.getElementalCoords(eleID, coords);
      coordsToZeroptv(coords, nodal_pos, daScalingFactor, eleOrder, true);
      talyEq_->mat->getSyncValues(_val_, eleID);

      if (eleOrder == 1) {
        sync.syncCoords<1, m_uiDim>(coords, talyEq_->grid());
      } else if (eleOrder == 2) {
        sync.syncCoords<2, m_uiDim>(coords, talyEq_->grid());
      }
      for (int i = 0; i < syncVal.size(); i++) {
        if (eleOrder == 1) {
          sync.syncValues<NSHTNodeData, 1, m_uiDim>(_val_[i], talyEq_->field(), syncVal[i].ndof,
                                                    syncVal[i].nodeDataIndex);
        } else if (eleOrder == 2) {
          sync.syncValues<NSHTNodeData, 2, m_uiDim>(_val_[i], talyEq_->field(), syncVal[i].ndof,
                                                    syncVal[i].nodeDataIndex);
        }
      }

      /// element corner and size
      Point position(coords[0], coords[1], coords[2]);
      int level = da.getLevel(eleID);
      double h1 = daScalingFactor[0] / (1u << level);
      double h2 = daScalingFactor[1] / (1u << level);
      double h3 = daScalingFactor[2] / (1u << level);
      Point h(h1, h2, h3);

      /// Prepare background element data
      FEMElm fe(talyEq_->grid(), TALYFEMLIB::BASIS_ALL);

      if (eleOrder == 1) {
        fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
      } else if (eleOrder == 2) {
        fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
      }

      ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
      ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
      ptvg.z() = geom_data.my_gauss_points[gpID].values[2];
      GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
      const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label);
      fe.calc_at(ptvl);
      fe.set_jacc_x_w(gpinfo.area * (1.0 / 3.0));

      /// Calculate Nu
      std::vector<ZEROPTV> flux_elem(2);

      flux_elem = calcFluxElementIBM(fe, geom_data.geo, gpinfo, position, h);
      Flux_sum[0] += flux_elem[0];
      Flux_sum[1] += flux_elem[1];
    }
  }

  /** Post Mat **/
  for (int i = 0; i < syncVal.size(); i++) {
    delete[] _val_[i];
  }
  delete[]      coords;
  talyEq_->mat->postMat();

  return Flux_sum;
}

std::vector<ZEROPTV> HeatPostProcess::calcHeatFluxPerMpiSurf(ot::DA &da,
                                                             const double *daScalingFactor,
                                                             bool visitGhostElements) {
  auto syncVal = talyEq_->mat->getSyncValuesVec();
  int npe = da.getNumNodesPerElement();
  int eleOrder = da.getElementOrder();
  DENDRITE_REAL *coords_dendro;
  coords_dendro = new DENDRITE_REAL[m_uiDim * npe];

  auto _val_ = new DENDRITE_REAL *[syncVal.size()];
  auto vec_buff_ = new DENDRITE_REAL *[syncVal.size()];

  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
  }

  for (int i = 0; i < syncVal.size(); i++) {
    da.nodalVecToGhostedNodal(syncVal[i].val, vec_buff_[i], false, syncVal[i].ndof);
    da.readFromGhostBegin(vec_buff_[i], syncVal[i].ndof);
    da.readFromGhostEnd(vec_buff_[i], syncVal[i].ndof);
  }

  TalyDendroSync sync_;
  TalyMesh<TALYFEMLIB::NODEData> taly_mesh(eleOrder);
  /// sum of all the Flux in all the elements
  std::vector<ZEROPTV> Flux_mpi(2);

  for (da.init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       da.curr() < da.end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da.next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    TALYFEMLIB::ELEM *taly_elem_ = talyEq_->grid()->elm_array_[0];
    taly_elem_->set_elm_id(da.curr());
    da.getElementalCoords(da.curr(), coords_dendro);
    for (int i = 0; i < npe; i++) {
      convertOctToPhys(&coords_dendro[3 * i], daScalingFactor);
    }
    if (eleOrder == 1) {
      sync_.syncCoords<1, 3>(coords_dendro, talyEq_->grid());
    } else if (eleOrder == 2) {
      sync_.syncCoords<2, 3>(coords_dendro, talyEq_->grid());
    }
    for (int i = 0; i < syncVal.size(); i++) {
      da.getElementNodalValues(vec_buff_[i], _val_[i], da.curr(), syncVal[i].ndof);
      if (eleOrder == 1) {
        sync_.syncValues<NSHTNodeData, 1, m_uiDim>(_val_[i],
                                                   talyEq_->field(),
                                                   syncVal[i].ndof,
                                                   syncVal[i].nodeDataIndex);
      } else if (eleOrder == 2) {
        sync_.syncValues<NSHTNodeData, 2, m_uiDim>(_val_[i],
                                                   talyEq_->field(),
                                                   syncVal[i].ndof,
                                                   syncVal[i].nodeDataIndex);
      }
    }

    TALYFEMLIB::FEMElm fe(talyEq_->grid(), TALYFEMLIB::BASIS_ALL);
    if (eleOrder == 1) {
      fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
    } else if (eleOrder == 2) {
      fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
    }

    /// surface integration
    // determines which side(s) of the domain p lies on (may be multiple at corners)
    const auto get_surf_flags = [&](const TALYFEMLIB::ZEROPTV &p) -> unsigned int {
      static const double tol = 1e-8;
      unsigned int flags = 0;
      // use the actual domain size
      if (fabs(p.x() - idata_->mesh_def.channel_min[0]) < tol)
        flags |= (1u << 1u);
      if (fabs(p.x() - idata_->mesh_def.channel_max[0]) < tol)
        flags |= (1u << 2u);
      if (fabs(p.y() - idata_->mesh_def.channel_min[1]) < tol)
        flags |= (1u << 3u);
      if (fabs(p.y() - idata_->mesh_def.channel_max[1]) < tol)
        flags |= (1u << 4u);
      if (fabs(p.z() - idata_->mesh_def.channel_min[2]) < tol)
        flags |= (1u << 5u);
      if (fabs(p.z() - idata_->mesh_def.channel_max[2]) < tol)
        flags |= (1u << 6u);
      return flags;
    };

    //********************************************************
    const int *surf_arr = taly_elem_->GetSurfaceCheckArray();
    const int surf_row_len = taly_elem_->GetSurfaceCheckArrayRowLength();
    const int nodes_per_surf = taly_elem_->GetNodesPerSurface();
    for (int i = 0; i < taly_elem_->GetSurfaceCount(); i++) {
      int surf_id = surf_arr[i * surf_row_len];  // id determines which nodes on the surface

      // note: loop assumes elements have 8 nodes and use an identity map, so we skip using elem->ElemToLocalNodeID
      unsigned int flags = ~0u;  // initialize to all true
      TALYFEMLIB::ZEROPTV position;

      for (unsigned int n = 0; n < nodes_per_surf && flags != 0; n++) {
        int elem_node_id = surf_arr[i * surf_row_len + 1 + n];
        // remove any surface flags that all nodes do not have in common
        // skipping elem->ElemToLocalNodeID because identity map!
        position = talyEq_->grid()->GetNode(elem_node_id)->location();
        flags = flags & sync_.get_surf_flags(position, daScalingFactor);

      }

      if (flags & (1u << surface2cal_)) {
        // TODO set up basis cache for surfaces (should also cache normals...)
        TALYFEMLIB::SurfaceIndicator surf(surf_id);
        surf.set_normal(taly_elem_->CalculateNormal(talyEq_->grid(), surf_id));

        if (eleOrder == 1) {
          fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
        } else if (eleOrder == 2) {
          fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_QUADRATIC, 0);
        }

        // loop over surface gauss points
        while (fe.next_itg_pt()) {
          std::vector<ZEROPTV> Flux_elm = calcFluxElementSurf(fe);
          Flux_mpi[0] += Flux_elm[0];
          Flux_mpi[1] += Flux_elm[1];
        }
      }
    }
    //********************************************************
  }

  /** Post ComputeVec **/
  for (int i = 0; i < syncVal.size(); i++) {
    delete[] _val_[i];
  }
  delete[] coords_dendro;
  return Flux_mpi;
}

std::vector<double> HeatPostProcess::calcNuPerMpiIBM(ot::DA &da,
                                                     const double *daScalingFactor,
                                                     const IBMSolver<NSHTNodeData>::GeomData &geom_data,
                                                     bool visitGhostElements) {
  const unsigned int ndof = NSHTNodeData::HT_DOF;
  auto syncVal = talyEq_->mat->getSyncValuesVec();
  int npe = da.getNumNodesPerElement();
  int eleOrder = da.getElementOrder();

  DENDRITE_REAL **_val_;
  DENDRITE_REAL *coords;
  std::vector<ZEROPTV> nodal_pos(npe);
  coords = new DENDRITE_REAL[m_uiDim * npe];
  _val_ = new DENDRITE_REAL *[syncVal.size()];
  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
  }
  talyEq_->mat->preMat();
  TalyDendroSync sync;

  /** Looping over the geometries**/
  /// sum of all the heat fluxs in all the elements
  int counter = 0;
  std::vector<double> Nu_sum = {0.0, 0.0};

  /** Looping over all the background element**/
  ZEROPTV ptvl; /// Local point in isoparameteric space
  ZEROPTV ptvg; /// Gauss point
  for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size(); gpID++) {

    if (geom_data.my_gauss_points[gpID].isLocal == true) {
      counter++;
      /// get element coordinates and data
      unsigned int eleID = geom_data.backgroundElementGP[gpID];

      da.getElementalCoords(eleID, coords);
      coordsToZeroptv(coords, nodal_pos, daScalingFactor, eleOrder, true);
      talyEq_->mat->getSyncValues(_val_, eleID);

      if (eleOrder == 1) {
        sync.syncCoords<1, m_uiDim>(coords, talyEq_->grid());
      } else if (eleOrder == 2) {
        sync.syncCoords<2, m_uiDim>(coords, talyEq_->grid());
      }
      for (int i = 0; i < syncVal.size(); i++) {
        if (eleOrder == 1) {
          sync.syncValues<NSHTNodeData, 1, m_uiDim>(_val_[i], talyEq_->field(), syncVal[i].ndof,
                                                    syncVal[i].nodeDataIndex);
        } else if (eleOrder == 2) {
          sync.syncValues<NSHTNodeData, 2, m_uiDim>(_val_[i], talyEq_->field(), syncVal[i].ndof,
                                                    syncVal[i].nodeDataIndex);
        }
      }

      /// element corner and size
      Point position(coords[0], coords[1], coords[2]);
      int level = da.getLevel(eleID);
      double h1 = daScalingFactor[0] / (1u << level);
      double h2 = daScalingFactor[1] / (1u << level);
      double h3 = daScalingFactor[2] / (1u << level);
      Point h(h1, h2, h3);

      /// Prepare background element data
      FEMElm fe(talyEq_->grid(), TALYFEMLIB::BASIS_ALL);

      if (eleOrder == 1) {
        fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
      } else if (eleOrder == 2) {
        fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
      }

      ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
      ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
      ptvg.z() = geom_data.my_gauss_points[gpID].values[2];
      GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
      const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label);
      fe.calc_at(ptvl);
      fe.set_jacc_x_w(gpinfo.area * (1.0 / 3.0));

      /// Calculate Nu
      std::vector<double> Nu_elem = {0.0, 0.0};

      Nu_elem = calcNuElementIBM(fe, geom_data.geo, gpinfo, position, h);
      Nu_sum[0] += Nu_elem[0];
      Nu_sum[1] += Nu_elem[1];
    }
  }

  /** Post Mat **/
  for (int i = 0; i < syncVal.size(); i++) {
    delete[] _val_[i];
  }
  delete[]      coords;
  talyEq_->mat->postMat();

  return Nu_sum;
}

std::vector<double> HeatPostProcess::calcNuPerMpiSurf(ot::DA &da,
                                                      const double *daScalingFactor,
                                                      bool visitGhostElements) {
  auto syncVal = talyEq_->mat->getSyncValuesVec();
  int npe = da.getNumNodesPerElement();
  int eleOrder = da.getElementOrder();
  DENDRITE_REAL *coords_dendro;
  coords_dendro = new DENDRITE_REAL[m_uiDim * npe];

  auto _val_ = new DENDRITE_REAL *[syncVal.size()];
  auto vec_buff_ = new DENDRITE_REAL *[syncVal.size()];

  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
  }

  for (int i = 0; i < syncVal.size(); i++) {
    da.nodalVecToGhostedNodal(syncVal[i].val, vec_buff_[i], false, syncVal[i].ndof);
    da.readFromGhostBegin(vec_buff_[i], syncVal[i].ndof);
    da.readFromGhostEnd(vec_buff_[i], syncVal[i].ndof);
  }

  TalyDendroSync sync_;
  TalyMesh<TALYFEMLIB::NODEData> taly_mesh(eleOrder);
  /// sum of all the Nu in all the elements
  std::vector<double> Nu_mpi(2);

  for (da.init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       da.curr() < da.end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da.next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    TALYFEMLIB::ELEM *taly_elem_ = talyEq_->grid()->elm_array_[0];
    taly_elem_->set_elm_id(da.curr());
    da.getElementalCoords(da.curr(), coords_dendro);
    for (int i = 0; i < npe; i++) {
      convertOctToPhys(&coords_dendro[3 * i], daScalingFactor);
    }
    if (eleOrder == 1) {
      sync_.syncCoords<1, 3>(coords_dendro, talyEq_->grid());
    } else if (eleOrder == 2) {
      sync_.syncCoords<2, 3>(coords_dendro, talyEq_->grid());
    }
    for (int i = 0; i < syncVal.size(); i++) {
      da.getElementNodalValues(vec_buff_[i], _val_[i], da.curr(), syncVal[i].ndof);
      if (eleOrder == 1) {
        sync_.syncValues<NSHTNodeData, 1, m_uiDim>(_val_[i],
                                                   talyEq_->field(),
                                                   syncVal[i].ndof,
                                                   syncVal[i].nodeDataIndex);
      } else if (eleOrder == 2) {
        sync_.syncValues<NSHTNodeData, 2, m_uiDim>(_val_[i],
                                                   talyEq_->field(),
                                                   syncVal[i].ndof,
                                                   syncVal[i].nodeDataIndex);
      }
    }

    TALYFEMLIB::FEMElm fe(talyEq_->grid(), TALYFEMLIB::BASIS_ALL);
    if (eleOrder == 1) {
      fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
    } else if (eleOrder == 2) {
      fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
    }

    /// surface integration
    // determines which side(s) of the domain p lies on (may be multiple at corners)
    const auto get_surf_flags = [&](const TALYFEMLIB::ZEROPTV &p) -> unsigned int {
      static const double tol = 1e-8;
      unsigned int flags = 0;
      // use the actual domain size
      if (fabs(p.x() - idata_->mesh_def.channel_min[0]) < tol)
        flags |= (1u << 1u);
      if (fabs(p.x() - idata_->mesh_def.channel_max[0]) < tol)
        flags |= (1u << 2u);
      if (fabs(p.y() - idata_->mesh_def.channel_min[1]) < tol)
        flags |= (1u << 3u);
      if (fabs(p.y() - idata_->mesh_def.channel_max[1]) < tol)
        flags |= (1u << 4u);
      if (fabs(p.z() - idata_->mesh_def.channel_min[2]) < tol)
        flags |= (1u << 5u);
      if (fabs(p.z() - idata_->mesh_def.channel_max[2]) < tol)
        flags |= (1u << 6u);
      return flags;
    };

    //********************************************************
    const int *surf_arr = taly_elem_->GetSurfaceCheckArray();
    const int surf_row_len = taly_elem_->GetSurfaceCheckArrayRowLength();
    const int nodes_per_surf = taly_elem_->GetNodesPerSurface();
    for (int i = 0; i < taly_elem_->GetSurfaceCount(); i++) {
      int surf_id = surf_arr[i * surf_row_len];  // id determines which nodes on the surface

      // note: loop assumes elements have 8 nodes and use an identity map, so we skip using elem->ElemToLocalNodeID
      unsigned int flags = ~0u;  // initialize to all true
      TALYFEMLIB::ZEROPTV position;

      for (unsigned int n = 0; n < nodes_per_surf && flags != 0; n++) {
        int elem_node_id = surf_arr[i * surf_row_len + 1 + n];
        // remove any surface flags that all nodes do not have in common
        // skipping elem->ElemToLocalNodeID because identity map!
        position = talyEq_->grid()->GetNode(elem_node_id)->location();
        flags = flags & sync_.get_surf_flags(position, daScalingFactor);

      }

      if (flags & (1u << surface2cal_)) {
        // TODO set up basis cache for surfaces (should also cache normals...)
        TALYFEMLIB::SurfaceIndicator surf(surf_id);
        surf.set_normal(taly_elem_->CalculateNormal(talyEq_->grid(), surf_id));

        if (eleOrder == 1) {
          fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
        } else if (eleOrder == 2) {
          fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_QUADRATIC, 0);
        }

        // loop over surface gauss points
        while (fe.next_itg_pt()) {
          std::vector<double> Nu_elm = calcNuElementSurf(fe);
          Nu_mpi[0] += Nu_elm[0];
          Nu_mpi[1] += Nu_elm[1];
        }
      }
    }
    //********************************************************
  }

  /** Post ComputeVec **/
  for (int i = 0; i < syncVal.size(); i++) {
    delete[] _val_[i];
  }
  delete[] coords_dendro;
  return Nu_mpi;
}

// this is the function to be called when calculating the heat flux on a object
void HeatPostProcess::calcHeatFlux(ot::DA &m_octDA,
                                   const double *daScalingFactor) {

  PrintStatus("Solve for heat flux of IBM");
  Flux_ibm.clear();
  Flux_surface.clear();
  for (const auto &geom_data : ibmSolver_->getGeometries()) {
    std::vector<ZEROPTV> heatFlux_local(2);
    std::vector<ZEROPTV> heatFlux_all(2);
    heatFlux_local = calcHeatFluxPerMpiIBM(m_octDA, daScalingFactor, geom_data);
    MPI_Allreduce(heatFlux_local.data(), heatFlux_all.data(), 3, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    Flux_ibm.push_back(heatFlux_all);
  }

  PrintStatus("Solve for heat flux of surface");
  for (const auto &surface_id: idata_->SurfaceMonitor) {
    surface2cal_ = surface_id;
    std::vector<ZEROPTV> heatFlux_local(2);
    std::vector<ZEROPTV> heatFlux_all(2);
    heatFlux_local = calcHeatFluxPerMpiSurf(m_octDA, daScalingFactor);
    MPI_Allreduce(heatFlux_local.data(), heatFlux_all.data(), 3, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    Flux_surface.push_back(heatFlux_all);
  }
  PrintHeatFluxInfo();
}

/// this is the function to be called when calculating the heat flux on a object
void HeatPostProcess::calcNu(ot::DA &m_octDA,
                             const double *daScalingFactor) {
  PrintStatus("Solve for Nu of IBM");
  Nu_ibm.clear();
  for (const auto &geom_data : ibmSolver_->getGeometries()) {
    std::vector<double> Nu_local, Nu_all;
    Nu_local.resize(2);
    Nu_all.resize(2);
    Nu_local = calcNuPerMpiIBM(m_octDA, daScalingFactor, geom_data);
    MPI_Allreduce(Nu_local.data(), Nu_all.data(), 2, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    Nu_ibm.push_back(Nu_all);
  }
  PrintStatus("Solve for Nu of surface");
  for (const auto &surface_id: idata_->SurfaceMonitor) {
    surface2cal_ = surface_id;
    std::vector<double> Nu_all, Nu_local;
    Nu_all.resize(2);
    Nu_local.resize(2);
    Nu_local = calcNuPerMpiSurf(m_octDA, daScalingFactor);
    MPI_Allreduce(Nu_local.data(), Nu_all.data(), 2, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    Nu_surface.push_back(Nu_all);
  }
  PrintNuInfo();

}

void HeatPostProcess::PrintHeatFluxInfo() {
  for (int i = 0; i < idata_->SurfaceMonitor.size(); i++) {
    /// Print results
    PrintStatus("surface:\t",
                idata_->SurfaceMonitor[i],
                "\theat flux:\t",
                Flux_surface[i][0],
                "+",
                Flux_surface[i][1],
                "=",
                Flux_surface[i][0] + Flux_surface[i][1]);

    if (!TALYFEMLIB::GetMPIRank()) {
      std::string fname = "Flux_" + to_string(idata_->SurfaceMonitor[i]) + ".dat";
      FILE *fp = fopen(fname.c_str(), "a");
      assert (fp != nullptr);
      fprintf(fp,
              "Timestep: %1d, Time: %.5f\n"
              "Flux_x = %.10e, Flux_y = %.10e, Flux_z = %.10e\n"
              "Flux_x_pen = %.10e, Flux_y_pen = %.10e, Flux_z_pen = %.10e\n"
              "Flux_x_total = %.10e, Flux_y_total = %.10e, Flux_z_total = %.10e\n",
              ti_.getTimeStepNumber(),
              ti_.getCurrentTime(),
              Flux_surface[i][0].x(),
              Flux_surface[i][0].y(),
              Flux_surface[i][0].z(),
              Flux_surface[i][1].x(),
              Flux_surface[i][1].y(),
              Flux_surface[i][1].z(),
              Flux_surface[i][0].x() + Flux_surface[i][1].x(),
              Flux_surface[i][0].y() + Flux_surface[i][1].y(),
              Flux_surface[i][0].z() + Flux_surface[i][1].z());
      fclose(fp);
    }
  }

  for (int i = 0; i < ibmSolver_->getGeometries().size(); i++) {
    const auto geom_data = ibmSolver_->getGeometries()[i];

    /// Print results
    PrintStatus(geom_data.geo->def_.name,
                " heat flux: ",
                Flux_ibm[i][0],
                "+",
                Flux_ibm[i][1],
                "=",
                Flux_ibm[i][0] + Flux_ibm[i][1]);

    if (geom_data.geo->def_.ifPostprocess) {
      /// Save to file
      if (!TALYFEMLIB::GetMPIRank()) {
        std::string fname = "Flux_" + geom_data.geo->def_.name + ".dat";
        FILE *fp = fopen(fname.c_str(), "a");
        assert (fp != nullptr);
        fprintf(fp,
                "Timestep: %1d, Time: %.5f\n"
                "Flux_x = %.10e, Flux_y = %.10e, Flux_z = %.10e\n"
                "Flux_x_pen = %.10e, Flux_y_pen = %.10e, Flux_z_pen = %.10e\n"
                "Flux_x_total = %.10e, Flux_y_total = %.10e, Flux_z_total = %.10e\n",
                ti_.getTimeStepNumber(),
                ti_.getCurrentTime(),
                Flux_ibm[i][0].x(),
                Flux_ibm[i][0].y(),
                Flux_ibm[i][0].z(),
                Flux_ibm[i][1].x(),
                Flux_ibm[i][1].y(),
                Flux_ibm[i][1].z(),
                Flux_ibm[i][0].x() + Flux_ibm[i][1].x(),
                Flux_ibm[i][0].y() + Flux_ibm[i][1].y(),
                Flux_ibm[i][0].z() + Flux_ibm[i][1].z());
        fclose(fp);
      }
    }
  }
}

void HeatPostProcess::PrintNuInfo() {
  for (int i = 0; i < idata_->SurfaceMonitor.size(); i++) {
    PrintStatus("surface:\t",
                idata_->SurfaceMonitor[i],
                "\tNu:\t",
                Nu_surface[i][0],
                " + ",
                Nu_surface[i][1],
                " = ",
                Nu_surface[i][0] + Nu_surface[i][1]);
    if (!TALYFEMLIB::GetMPIRank()) {
      std::string fname = "Nu_" + to_string(idata_->SurfaceMonitor[i]) + ".dat";
      FILE *fp = fopen(fname.c_str(), "a");
      assert (fp != nullptr);
      fprintf(fp,
              "Timestep: %1d, Time: %.5f\n"
              "Nu_normal = %.10e, Nu_penalty = %.10e\n",
              ti_.getTimeStepNumber(),
              ti_.getCurrentTime(),
              Nu_surface[i][0],
              Nu_surface[i][1]);
      fclose(fp);
    }
  }

  for (int i = 0; i < ibmSolver_->getGeometries().size(); i++) {
    const auto geom_data = ibmSolver_->getGeometries()[i];

    if (geom_data.geo->def_.ifPostprocess) {
      /// Print results
      PrintStatus(geom_data.geo->def_.name,
                  " Nu:\t",
                  Nu_ibm[i][0],
                  " + ",
                  Nu_ibm[i][1],
                  " = ",
                  Nu_ibm[i][0] + Nu_ibm[i][1]);
      /// Save to file
      if (!TALYFEMLIB::GetMPIRank()) {
        std::string fname = "Nu_" + geom_data.geo->def_.name + ".dat";
        FILE *fp = fopen(fname.c_str(), "a");
        assert (fp != nullptr);
        fprintf(fp,
                "Timestep: %1d, Time: %.5f\n"
                "Nu_normal = %.10e, Nu_penalty = %.10e\n",
                ti_.getTimeStepNumber(),
                ti_.getCurrentTime(),
                Nu_ibm[i][0],
                Nu_ibm[i][1]);
        fclose(fp);
      }
    }

  }

}
