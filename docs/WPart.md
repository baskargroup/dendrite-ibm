Running
=======
* The config file from [NSHT_dendro5/tests/WeightedPartitioning](../NSHT_dendro5/tests/WeightedPartititoning) should be copied to the build directory.
* The following parameters in the config file needs to be updated:
```bash
RatioWeight = $ratio;
surface_cost = 1000;
```
* `surface_cost = 1000` ensures equal weight partititoning.
* To enable weighted partitioning change `surface_cost` to any other
value than 1000 and change `RatioWeight` to be the ration of Tv/Ts.
* Launch `mpirun -np $NPROC ./wrun` for weighted partitioing.