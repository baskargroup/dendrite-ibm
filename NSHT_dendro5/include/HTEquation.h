#pragma once

#include <talyfem/fem/cequation.h>
#include <talyfem/stabilizer/tezduyar_upwind.h>
#include "NSHTNodeData.h"
#include "IBM/IBMSolver.h"
#include "IBM/MathOperation.h"

class HTEquation : public TALYFEMLIB::CEquation<NSHTNodeData> {
 public:
  explicit HTEquation(NSHTInputData *idata, IBMSolver<NSHTNodeData> *ibmSolver)
      : TALYFEMLIB::CEquation<NSHTNodeData>(false, TALYFEMLIB::kAssembleGaussPoints) {
    ibmSolver_ = ibmSolver;
    idata_ = idata;
  }

  void Solve(double dt, double t) override {
    assert(false);
  }

  void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
                  TALYFEMLIB::ZEROARRAY<double> &be) override {
    assert(false);
  }

  void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
    using namespace TALYFEMLIB;
    const int elmID = fe.elem()->elm_id();
    if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::IN_GP) {
      return;
    } else if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::INTERCEPTED_GP) {
      auto triangles = ibmSolver_->getTriInOctant(elmID);
      if (point_in_any_geometry(fe.position(), triangles)) {
        return;
      }
    }

    const int nsd = fe.nsd();               // # of dimensions: 1D, 2D, or 3D
    const int nbf = fe.nbf();               // # of basis functions
    const double detJxW = fe.detJxW();      // (determinant of J) cross W
    double Coe_diff = idata_->getDiffusionHT(t_);
    const double Ci_f = idata_->Ci_f;

    ZEROPTV u;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, i);
    }

    if (idata_->solverTypeHT == NSHTInputData::RBVMS_HT) {
      /// Get the cofactor matrix from FEELm class
      ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      /// G_{ij} in equation 65 in Bazilevs et al. (2007)
      ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++) {
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
          }
        }
      }
      // todo inverse estimate
      double u_Gu = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          u_Gu += u(i) * Ge(i, j) * u(j);
        }
      }

      double G_G_u = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
        }
      }
      double tauE = idata_->tauM_scale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);

/*      /// calculate the residual of HT coarse scale
      ZEROPTV dtheta;
      for (int i = 0; i < nsd; i++) {
        dtheta(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::TEMPERATURE, i);
      }
      double d2theta = 0.0;
      for (int dir = 0; dir < nsd; dir++) {
        d2theta += this->p_data_->value2DerivativeFEM(fe, NSHTNodeData::TEMPERATURE, dir, dir);
      }
      double convec = 0.0;
      for (int i = 0; i < nsd; i++) {
        convec += u(i) * dtheta(i);
      }
      double diffusion = Coe_diff * d2theta;
      double theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);
      double theta_pre = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE + NSHTNodeData::NUM_VARS);

      double HT_Res = (theta - theta_pre) / dt_ + convec - diffusion;*/
      // todo maybe fine scale velocity
      for (int a = 0; a < nbf; a++) {
        double crossTermVelocityPart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermVelocityPart += fe.dN(a, i) * u(i);
        }
        for (int b = 0; b < nbf; b++) {
          double M = (fe.N(a) + crossTermVelocityPart * tauE) * fe.N(b) * detJxW;
          double N = 0;
          for (int k = 0; k < nsd; k++) {
            N += Coe_diff * fe.dN(a, k) * fe.dN(b, k) * detJxW
                + (fe.N(a) + crossTermVelocityPart * tauE) * u(k) * fe.dN(b, k) * detJxW;
          }
          Ae(a, b) += M / dt_ + N;
        }
      }

    } else if (idata_->solverTypeHT == NSHTInputData::STABILIZED_HT) {
      for (int a = 0; a < nbf; a++) {
        TALYFEMLIB::TezduyarUpwindFE SUPGterm;
        SUPGterm.calcSUPGWeightingFunction(fe, u, Coe_diff);
        for (int b = 0; b < nbf; b++) {
          double M = (fe.N(a) + SUPGterm.SUPG(a)) * fe.N(b) * detJxW;
          double N = 0;
          for (int k = 0; k < nsd; k++) {
            N += Coe_diff * fe.dN(a, k) * fe.dN(b, k) * detJxW
                + (fe.N(a) + SUPGterm.SUPG(a)) * u(k) * fe.dN(b, k) * detJxW;
          }
          Ae(a, b) += M / dt_ + N;
        }
      }
    }


/*    #ifndef NDEBUG
      FILE *fp = fopen("Ae_ht_new.dat", "a");
      for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
          fprintf(fp, "%.10f\t", Ae(i, j));
        }
      }
      fprintf(fp, "\n");
      fclose(fp);
      exit(0);
    #endif*/
  }

  void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {
    using namespace TALYFEMLIB;
    const int elmID = fe.elem()->elm_id();
    if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::IN_GP) {
      return;
    } else if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::INTERCEPTED_GP) {
      auto triangles = ibmSolver_->getTriInOctant(elmID);
      if (point_in_any_geometry(fe.position(), triangles)) {
        return;
      }
    }
    const double detJxW = fe.detJxW();
    const int nsd = idata_->nsd;
    double Coe_diff = idata_->getDiffusionHT(t_);

    ZEROPTV u;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, i);
    }

    const double t_pre = p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + NSHTNodeData::TEMPERATURE);
    if (idata_->solverTypeHT == NSHTInputData::RBVMS_HT) {
      const double Ci_f = idata_->Ci_f;
      /// Get the cofactor matrix from FEELm class
      ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      /// G_{ij} in equation 65 in Bazilevs et al. (2007)
      ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++) {
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
          }
        }
      }
      // todo inverse estimate
      double u_Gu = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          u_Gu += u(i) * Ge(i, j) * u(j);
        }
      }

      double G_G_u = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
        }
      }
      double tauE = idata_->tauM_scale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
      for (int a = 0; a < fe.nbf(); a++) {
        double crossTermVelocityPart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermVelocityPart += fe.dN(a, i) * u(i);
        }
        be(a) += (fe.N(a) + tauE * crossTermVelocityPart) / dt_ * t_pre * detJxW;
      }
    } else if (idata_->solverTypeHT == NSHTInputData::STABILIZED_HT) {
      for (int a = 0; a < fe.nbf(); a++) {
        TALYFEMLIB::TezduyarUpwindFE SUPGterm;
        SUPGterm.calcSUPGWeightingFunction(fe, u, Coe_diff);
        be(a) += (fe.N(a) + SUPGterm.SUPG(a)) / dt_ * t_pre * detJxW;
      }
    }
/*    #ifndef NDEBUG
      FILE *fp = fopen("be_ht_new.dat", "a");
      for (int j = 0; j < 8; j++) {
        fprintf(fp, "%.10f\t", be(j));
      }
      fprintf(fp, "\n");
      fclose(fp);
    #endif*/
  }

  void Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZeroMatrix<double> &Ae) override {
    /// from integral by parts, there is no Ae for the boundary term,
    /// because -[w\dot \alpha \grad T] are all known variable, and they goes into the be
    /// for the weak BC
    const int nsd = idata_->nsd;
    const double Cb_e = idata_->Cb_e;
    const double detSideJxW = fe.detJxW();
    double Coe_diff = idata_->getDiffusionHT(t_);

    const auto &bc = idata_->boundary_def[side_idx - 1];
    if (bc.temperature_type == BoundaryDef::Temperature_Type::WEAK) {
      TALYFEMLIB::ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          /// This is again correct, we need the volume jacabion on the surface of the box, not the same as in ibm
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      TALYFEMLIB::ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++)
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
        }
      }

      double n_Gn = 0.0;

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          n_Gn += fe.surface()->normal()(j) * Ge(j, i) * fe.surface()->normal()(i);
        }
      }

      /// wall normal distance, check this to see if ksiX is right or not
      double hb = 2.0 / sqrt(n_Gn);

      const ZEROPTV &normal = fe.surface()->normal();
      for (int a = 0; a < fe.nbf(); a++) {
        double supg = 0.0;
        for (int m = 0; m < nsd; m++) {
          supg += fe.dN(a, m) * normal(m);
        }

        for (int b = 0; b < fe.nbf(); b++) {
          double supgb = 0.0;
          for (int m = 0; m < nsd; m++) {
            supgb += fe.dN(b, m) * normal(m);
          }

          /// diffusion
          Ae(a, b) += -Coe_diff * fe.N(a) * (supgb) * detSideJxW
              /// adjoint
              - Coe_diff * supg * fe.N(b) * detSideJxW
                  /// penalty, constant factor vs. calculated
              + (Coe_diff * Cb_e / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
        }
      }
    }

  }

  void Integrands4side_be(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZEROARRAY<double> &be) override {
    using namespace TALYFEMLIB;
    const int nsd = idata_->nsd;
    const double detSideJxW = fe.detJxW();
    const double Cb_e = idata_->Cb_e;
    double Coe_diff = idata_->getDiffusionHT(t_);

    const auto &bc = idata_->boundary_def[side_idx - 1];
    if (bc.temperature_type == BoundaryDef::Temperature_Type::NEUMANN_T) {
      const ZEROPTV &p = fe.position();
      const ZEROPTV &normal = fe.surface()->normal();
      const double flux = bc.getRamping(t(), bc.flux);
      /// this comes from the integral by parts. -[w\dot \alpha \grad T], we are calling -(\alpha \grad T) = flux
      /// notice that the positive flux means the system is losing energy (normal is pointing outward)
      for (int a = 0; a < fe.nbf(); a++) {
        for (int m = 0; m < nsd; m++) {
          be(a) -= fe.N(a) * flux * normal(m) * fe.detJxW();
        }
      }
    } else if (bc.temperature_type == BoundaryDef::Temperature_Type::ROBIN_T) {
      const ZEROPTV &p = fe.position();
      const ZEROPTV &normal = fe.surface()->normal();
      const double G_constant = bc.getRamping(t(), bc.G_vec);
      const double a_constant = bc.getRamping(t(), bc.a_vec);
      const double theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);
      const double pseudo_flux = G_constant - a_constant * theta;
      for (int a = 0; a < fe.nbf(); a++) {
        for (int m = 0; m < nsd; m++) {
          be(a) -= fe.N(a) * pseudo_flux * normal(m) * fe.detJxW();
        }
      }
    } else if (bc.temperature_type == BoundaryDef::Temperature_Type::WEAK) {
      ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          /// This is again correct, we need the volume jacabion on the surface of the box, not the same as in ibm
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++)
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
        }
      }

      double n_Gn = 0.0;

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          n_Gn += fe.surface()->normal()(j) * Ge(j, i) * fe.surface()->normal()(i);
        }
      }

      /// wall normal distance, check this to see if ksiX is right or not
      double hb = 2.0 / sqrt(n_Gn);

      for (int a = 0; a < fe.nbf(); a++) {
        double supg = 0.0;
        for (int m = 0; m < nsd; m++) {
          supg += fe.dN(a, m) * fe.surface()->normal()(m);
        }
        /// adjoint
        be(a) += -Coe_diff * supg * bc.getRamping(t_, bc.temperature) * detSideJxW
            /// penalty
            + (Coe_diff * Cb_e / pow(hb, idata_->orderOfCb)) * fe.N(a) * bc.getRamping(t_, bc.temperature) * detSideJxW;
      }
    }
  }

  void ibm_Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe,
                              ZeroMatrix<double> &Ae,
                              Geom<NSHTNodeData> *geo,
                              const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                              const Point &node,
                              const Point &h) {
    const ZEROPTV &normal = gpinfo.gp_normal;
    const int nbf = fe.nbf();
    const int nsd = idata_->nsd;
    const double Cb_e = idata_->Cb_e;
    const double detSideJxW = fe.detJxW();
    const double Coe_weak = Cb_e;

    double Coe_diff = idata_->getDiffusionHT(t_);

//    GeomDef::BCType bcType = geo->def_.bc_type;
//    if (!geo->def_.bc_type_V.empty()) {
    GeomDef::BCType bcType = geo->def_.bc_type_V[NSHTNodeData::TEMPERATURE];
//    }
    assert(bcType != GeomDef::BCType::INVALID_BC);

    double theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);
    /// wall normal distance
    double hb = MathOp::normalDistance(fe.position(), normal, node, h);
    if (hb < 0) {
      PrintStatus("Elm node:\t", ZEROPTV{node.x(), node.y(), node.z()});
      PrintStatus("gp_pos:\t", fe.position());
      PrintStatus("normal:\t", normal);
      for (int i = 0; i < 3; ++i) {
        PrintStatus("triangle:\t", gpinfo.tri_nodes[i]);
      }
    }

    /// Based on what kind of solver chosen, choose the corresponding solver (SUPG or VMS)
    if (idata_->solverTypeHT == NSHTInputData::RBVMS_HT) {
      if (bcType == GeomDef::DIRICHLET) {
        if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            double supg = 0.0;
            /// dot(grad{l}, normal), l is the notation of test function for HT
            for (int m = 0; m < nsd; m++) {
              supg += fe.dN(a, m) * normal(m);
            }

            for (int b = 0; b < nbf; b++) {
              double supgb = 0.0;
              /// dot(grad{T}, normal)
              for (int m = 0; m < nsd; m++) {
                supgb += fe.dN(b, m) * normal(m);
              }

              Ae(a, b) +=
                  /// diffusion
                  -Coe_diff * fe.N(a) * (supgb) * detSideJxW
                      /// adjoint
                      - Coe_diff * supg * fe.N(b) * detSideJxW
                          /// penalty
                      + (Coe_diff * Cb_e / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
//            + Coe_weak * fe.N(a) * fe.N(b) * detSideJxW;
            }
          }
        } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            for (int b = 0; b < nbf; b++) {
              Ae(a, b) += 2 * (Coe_diff * Cb_e / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
//            + Coe_weak * fe.N(a) * fe.N(b) * detSideJxW;
            }
          }
        }

      } else if (bcType == GeomDef::NEUMANN) {
        /// do not do anything
      }
    } else if (idata_->solverTypeHT == NSHTInputData::STABILIZED_HT) {
      throw TALYFEMLIB::TALYException() << "SUPG HT not supported yet";
    }
/*  FILE *fp = fopen("Ae_ibm.dat", "a");
  for (int i = 0; i < nbf; i++) {
    for (int j = 0; j < nbf; j++) {
      fprintf(fp, "%.10f\t", Ae(i, j));
    }
  }
  fprintf(fp, "\n");
  fclose(fp);*/
  }

  void ibm_Integrands4side_be(const TALYFEMLIB::FEMElm &fe,
                              ZEROARRAY<double> &be,
                              Geom<NSHTNodeData> *geo,
                              const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                              const Point &node,
                              const Point &h) {

    /// If the geometry is 2D plane, we should treat this differently.
    const ZEROPTV &normal = gpinfo.gp_normal;

//    GeomDef::BCType bcType = geo->def_.bc_type;
//    double T = geo->def_.dirichlet;
//    double heat_flux = geo->def_.flux;
//    double G_constant = geo->def_.G_constant;
//    double a_constant = geo->def_.a_constant;
//    if (!geo->def_.bc_type_V.empty()) {
    GeomDef::BCType bcType = geo->def_.bc_type_V[NSHTNodeData::TEMPERATURE];
    std::vector<double> bc_value = geo->def_.getBC(NSHTNodeData::TEMPERATURE);
    double T = bc_value[0];
    double heat_flux = bc_value[0];
    double G_constant = bc_value[0];
    double a_constant = bc_value[1];
//    }
    assert(bcType != GeomDef::BCType::INVALID_BC);

    const int nbf = fe.nbf();
    const int nsd = idata_->nsd;
    const double Cb_e = idata_->Cb_e;
    const double detSideJxW = fe.detJxW();
    const double Coe_weak = Cb_e;
    double Coe_diff = idata_->getDiffusionHT(t_);

    double theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);

    /// wall normal distance (the maximum of nbf nodes)
    double hb = MathOp::normalDistance(fe.position(), normal, node, h);
    if (hb < 0) {
      PrintStatus(hb);
      PrintStatus("Elm_node:\t", ZEROPTV{node.x(), node.y(), node.z()});
      PrintStatus("gp_pos:\t", fe.position());
      PrintStatus("normal:\t", normal);
      for (int i = 0; i < 3; ++i) {
        PrintStatus("triangle:\t", gpinfo.tri_nodes[i]);
      }
    }



    /// Based on what kind of solver chosen, choose the corresponding solver (SUPG or VMS)
    if (idata_->solverTypeHT == NSHTInputData::RBVMS_HT) {
      if (bcType == GeomDef::DIRICHLET) {
        /// switch between pure surface and 3D element
        if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            double supg = 0.0;
            for (int m = 0; m < nsd; m++) {
              supg += fe.dN(a, m) * normal(m);
            }
            /// adjoint
            be(a) += -Coe_diff * supg * (T) * detSideJxW
                /// penalty
                + (Coe_diff * Cb_e / pow(hb, idata_->orderOfCb)) * fe.N(a) * (T) * detSideJxW;
//              + Coe_weak * fe.N(a) * T * detSideJxW;
          }
        } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            be(a) += 2 * (Coe_diff * Cb_e / pow(hb, idata_->orderOfCb)) * fe.N(a) * (T) * detSideJxW;
          }
        }
      } else if (bcType == GeomDef::NEUMANN) {
        /// neumann bc
        /// switch between pure surface and 3D element
        if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            for (int m = 0; m < nsd; m++) {
              be(a) += fe.N(a) * heat_flux * normal(m) * normal(m) * detSideJxW;
            }
          }
        } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            for (int m = 0; m < nsd; m++) {
              be(a) += 2 * fe.N(a) * heat_flux * normal(m) * normal(m) * detSideJxW;
            }
          }
        }
      } else if (bcType == GeomDef::ROBIN) {
        /// Robin bc
        const double pseudo_flux = G_constant - a_constant * theta;
        /// switch between pure surface and 3D element
        if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            for (int m = 0; m < nsd; m++) {
              be(a) -= fe.N(a) * pseudo_flux * normal(m) * normal(m) * detSideJxW;
            }
          }
        } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
          for (int a = 0; a < nbf; a++) {
            for (int m = 0; m < nsd; m++) {
              be(a) -= 2 * fe.N(a) * pseudo_flux * normal(m) * normal(m) * detSideJxW;
            }
          }
        }
      }
    } else if (idata_->solverTypeHT == NSHTInputData::STABILIZED_HT) {
      throw TALYFEMLIB::TALYException() << "SUPG HT not supported yet";
    }

/*  int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
  FILE *fp = fopen("be_ibm.dat", "a");
  fprintf(fp, "%d\t", gpinfo.label);
  for (int j = 0; j < nbf; j++) {
    fprintf(fp, "%.10f\t", be(interchangedmat[j]));
  }
  fprintf(fp, "\n");
  fclose(fp);*/

  }

 private:
  NSHTInputData *idata_;
  IBMSolver<NSHTNodeData> *ibmSolver_;

  bool point_in_any_geometry(const ZEROPTV &pt, std::vector<triInfo> &triangles) const {
    std::vector<double> dots;
    for (const auto &tri : triangles) {
      dots.push_back(tri.normal.innerProduct(pt - tri.centroid));
    }

    if (triangles.empty()) {
      return ibmSolver_->point_in_any_geometry(pt);
    } else {
      if (std::all_of(dots.begin(), dots.end(), [](double d) { return d < 0; })) {
        return false;
      } else if (std::none_of(dots.begin(), dots.end(), [](double d) { return d < 0; })) {
        return true;
      } else {
        return ibmSolver_->point_in_any_geometry(pt);
      }
    }
    throw TALYFEMLIB::TALYException() << "can not happen!";
    return false;
  }
};
