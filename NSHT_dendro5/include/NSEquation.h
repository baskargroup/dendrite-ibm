#pragma once

#include <talyfem/fem/cequation.h>
#include <talyfem/stabilizer/tezduyar_upwind.h>
#include "NSHTNodeData.h"
#include "IBM/IBMSolver.h"
#include "IBM/MathOperation.h"

class NSEquation : public TALYFEMLIB::CEquation<NSHTNodeData> {
 public:
  explicit NSEquation(NSHTInputData *idata, IBMSolver<NSHTNodeData> *ibmSolver)
      : TALYFEMLIB::CEquation<NSHTNodeData>(false, TALYFEMLIB::kAssembleGaussPoints) {
    ibmSolver_ = ibmSolver;
    idata_ = idata;
  }

  void Solve(double dt, double t) override {
    assert(false);
  }

  void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
                  TALYFEMLIB::ZEROARRAY<double> &be) override {
    assert(false);
  }

  void calcAe_vms_theta(const FEMElm &fe, ZeroMatrix<double> &Ae, const double tauMScale) {
    const double thetaTimeStepping = idata_->thetaTimeStepping;
    const double Coe_diff = idata_->getDiffusionNS(t_);
    const double Ci_f = idata_->Ci_f;
    const int nsd = fe.nsd();
    const int n_basis_functions = fe.nbf();
    const double detJxW = fe.detJxW();
    double dt = this->dt_;
    const bool timeStab = idata_->iftimeStab;


    /// field variables
    /** Get u and u_pre from the NodeData
     * NOTE: ValueFEM is defined on NodeData object, therefore it follows
     * indices defined in NodeData subclass.
     * NOTE: Every iteration the solution is copied into NodeData, we use these
     * solved fields from the NodeData object to calculate the be (NS residual)
     * of current
     * iteration
     */
    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + i);
    }

    /// Define velocity gradient tensor
    ZeroMatrix<double> du, du_pre;
    du.redim(nsd, nsd); du_pre.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
        du_pre(i, j) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::NUM_VARS + i, j);
      }
    }

    /// Get the cofactor matrix from FEELm class
    ZeroMatrix<double> ksiX;
    ksiX.redim(nsd, nsd);

    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        ksiX(i, j) = fe.cof(j, i) / fe.jacc();
      }
    }

    /// G_{ij} in equation 65 in Bazilevs et al. (2007)
    ZeroMatrix<double> Ge;
    Ge.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        Ge(i, j) = 0.0;
        for (int k = 0; k < nsd; k++) {
          Ge(i, j) += ksiX(k, i) * ksiX(k, j);
        }
      }
    }

    /// Equation 67 in Bazilevs et al. (2007)
    double u_Gu = 0.0;
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        u_Gu += u(i) * Ge(i, j) * u(j);
      }
    }

    /// Last term in Equation 63 in Bazilevs et al. (2007)
    double G_G_u = 0.0;
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
      }
    }

    /// Calculate the tau_M based on the projections calculated, Equation 63 in
    /// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
    /// in the paper it is set to 1.
    double tauM = tauMScale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
    /// Equation 68 in Bazilevs et al. (2007)
    ZEROARRAY<double> ge;
    ge.redim(nsd);
    for (int i = 0; i < nsd; i++) {
      ge(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        ge(i) += ksiX(j, i);
      }
    }

    /// Equation 69 Bazilevs et al. (2007)
    double g_g = 0.0;
    for (int i = 0; i < nsd; i++) {
      g_g += ge(i) * ge(i);
    }

    /// Calculate continuity residual based on
    double tauC = idata_->tauC_scale / (tauM * g_g);
    tauM = tauMScale / sqrt(static_cast<int>(timeStab)*4.0 / (dt_ * dt_) + u_Gu + G_G_u);
    /** Calculate the laplacian of velocity
     * This is required for course scale residual of Navier-Stokes for
     * diffusion term
     */
    ZEROPTV d2u, d2u_pre;
    /// loop for three directions of velocity (MAX NSD is no of velocity
    /// directions in the problem)
    /// PS: This assumes the first three degrees are always velocity, it is
    /// wise to keep it that way
    for (int dof = 0; dof < nsd; dof++) {
      /// Summing over three directions of velocity as it is laplacian
      for (int dir = 0; dir < nsd; dir++) {
        d2u(dof) += this->p_data_->value2DerivativeFEM(fe, dof, dir, dir);
        d2u_pre(dof) += this->p_data_->value2DerivativeFEM(fe, NSHTNodeData::NUM_VARS + dof, dir, dir);
      }
    }

    /** Remember your defined on the ValueFEM is on NodeData object, so use
     * indices the way you defined in NodeData, we acquire pressure from the
     * NodeData.
  */
    double p = this->p_data_->valueFEM(fe, NSHTNodeData::PRESSURE);
    // double theta = p_data_->valueFEM(fe, nsd + 1);

    /// Get gradient of pressure
    ZEROPTV dp, dp_pre;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::PRESSURE, i);
      dp_pre(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::NUM_VARS + NSHTNodeData::PRESSURE , i);
    }

    // NS terms
    /** We define the convection of Navier Stokes here from
     * using inner product of gradient tensor and fields we acquired above.
     *
      */
    ZEROPTV convec, convec_pre;
    ZEROPTV diffusion, diffusion_pre;
    for (int i = 0; i < nsd; i++) {
      convec(i) = 0.0;
      diffusion(i) = 0.0;

      convec_pre(i) = 0.0;
      diffusion_pre(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        convec(i) += du(i, j) * u(j);
        convec_pre(i) += du_pre(i, j) * u_pre(j);
      }
      diffusion(i) += Coe_diff * d2u(i);
      diffusion_pre(i) += Coe_diff * d2u_pre(i);
    }

    /** Construct the Navier Stokes equation without diffusion
     * Here diffusion is not present as its contribution to stabilizers for
     * linear basis functions is zero
     * Diffusion term is added for higher order basis functions
     * Equation 61 of Bazilevs et al. (2007)
     */
    ZEROPTV NS, NS_pre;
    for (int i = 0; i < nsd; i++) {
      NS(i) = (u(i) - u_pre(i)) / dt + convec(i) + dp(i) - diffusion(i);
      NS_pre(i) = (u(i) - u_pre(i)) / dt + convec_pre(i) + dp_pre(i) - diffusion_pre(i);
    }

    /** Calculate continuity residual for PSPG stabilizer
     * This residual is a essential for calculating the PSPG stabilizer
     * Equation 62 of Bazilevs et al. (2007)
     */
    double cont = 0.0, cont_pre = 0.0;
    for (int i = 0; i < nsd; i++) {
      cont += du(i, i);
      cont_pre += du_pre(i, i);
    }

    /** Calculating the Elemental matrices requires loop over basis functions
     * We loop over basis functions and calculate the Jacobian and the Residual
     * The equation we are solving is \vect{J} \cdot \delta u = -E
     * Here \vect{J} is the Jacobian matrix operating on \delta u and E is the
     * residual.  We will be using stabilized forms of Jacobian matrix and
     * residual
     */

    for (int a = 0; a < n_basis_functions; a++) {
      ///---------------Calculating the Jacobian operator--------------------///
      // NS terms

      /** Course scale terms for the cross terms (terms 4 and 5 in equation 52
       * in Bazilevs et al. (2007)
       * PS: these are not calculated using the SUPG class, and this is an
       * approximation of (u, grad{u})
       */
      double crossTermVelocityPart = 0.0, crossTermVelocityPart_pre = 0.0;
      for (int i = 0; i < nsd; i++) {
        crossTermVelocityPart += fe.dN(a, i) * u(i);
        crossTermVelocityPart_pre += fe.dN(a, i) * u_pre(i);
      }

      /// Actual fine scale contribution to cross terms from tauM(inverse
      /// estimate)
      double crossTermFineScalePart = 0.0, crossTermFineScalePart_pre = 0.0;
      for (int i = 0; i < nsd; i++) {
        crossTermFineScalePart += fe.dN(a, i) * tauM * NS(i);
        crossTermFineScalePart_pre += fe.dN(a, i) * tauM * NS_pre(i);
      }

      for (int b = 0; b < n_basis_functions; b++) {

        /// Convection term
        double conv = 0.0;
        for (int i = 0; i < nsd; i++) {
          conv += fe.dN(b, i) * u(i);
        }

        /// Adding terms to the Jacobian matrix.
        for (int i = 0; i < nsd; i++) {

          /// Transient terms and the convection term and part of the stress
          /// tensor in the diagonals
          Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
              (fe.N(a) * (fe.N(b) / dt + thetaTimeStepping * conv)) * detJxW;

          for (int j = 0; j < nsd; j++) {
            /// This term calculates (w, (delta_u . grad{u_n}))
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (fe.N(a) * du(i, j) * fe.N(b) * detJxW);
            /** This term calculates (grad{w},grad{delta_u}), goes only in
             * diagonals PS: In this case we are using the diffusion form not
             * the stress tensor form of the momentun equation
       */
            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
                (Coe_diff * fe.dN(a, j) * fe.dN(b, j) * detJxW);

//            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
//                (Coe_diff * fe.dN(a, j) * fe.dN(b, i) * detJxW);
          }
          /** This term calculates contribution of pressure to Jacobian matrix
           * mathematically the term is:  -(div{w},delta_p). This term gets
           * added to the last diagonal term of the matrix
           */
          Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) +=
              -thetaTimeStepping * fe.dN(a, i) * fe.N(b) * detJxW;
        }

        /** crossTerm 1: This term is written as, (w, grad{u u'}), which is
         * weakened to (grad{w}, u\innerproduct u') which is then linearised.
         * Here u' is fine scale velocity
         * and u is resolved velocity. The fine scale velocity is approximated
         * as -tau*Res{NS} (Equation 58 bazilevs et al. (2007)),
         *
         */
        for (int i = 0; i < nsd; i++) {

          /// Contribution of laplacian of velocity(diffusion) to the diagonal
          double diff_J = 0;
          for (int j = 0; j < nsd; j++) {
            diff_J += Coe_diff * fe.d2N(b, j, j);
          }
          /** When you linearise (u.grad{w},u'), (B_2 from Equation 57 from
           * Bazilevs et al. (2007)) you get two terms, one goes
           * in diagonal and other in the non-diagonal parts of the matrix
           * PS: crossTermVelocityPart is (u. grad{w} => u*dN(a))
           */

          /// Diagonal part
          Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
              (crossTermVelocityPart * tauM * (fe.N(b) / dt + conv - diff_J) *
                  detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (crossTermVelocityPart_pre * tauM * (fe.N(b) / dt) * detJxW);;
          for (int j = 0; j < nsd; j++) {
            /// Off diagonal part
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (crossTermVelocityPart * tauM * du(i, j) * fe.N(b) * detJxW);
            /** this term is essentially, (grad{w}*tauM*Residual(NS), delta u)
             * Equation 57 Bazilevs et al. (2007)
             * PS: u' = (tauM*residual(NS))
             */
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (tauM * fe.dN(a, j) * NS(i) * fe.N(b) * detJxW);
          }
          /// Pressure part goes in the last element of the diagonal
          Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
              (crossTermVelocityPart * tauM * fe.dN(b, i) * detJxW);
        }

        /** Crossterm2:This term can be mathematically written as,
         * (w, u'.grad{u}). In this term we do not further weaken it as done in
         * cross term 1
         */
        for (int i = 0; i < nsd; i++) {

          /// Contribution of laplacian of velocity(diffusion) to the diagonal
          double diff_J = 0;
          for (int j = 0; j < nsd; j++) {
            diff_J += Coe_diff * fe.d2N(b, j, j);
          }

          /** This term represents the contribution of,
           * (w, tau \delta{u} . grad{u}) term to the cross term, here \delta{} is
           * the linearisation operator (basically delta{u'} represents the
           * change in u').  This is the first term which arise due to
           * linearisation of the nonliner term of the
           * Navier-Stokes residual when u' is substitute for in
           * (w,\delta{u'}.grad{u}). PS: u' = - tauM*res{NS}
           */
          for (int j = 0; j < nsd; j++) {
            /// k is the dummy index
            for (int k = 0; k < nsd; k++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=  thetaTimeStepping *
                  (-du(i, k) * fe.N(a) * tauM * fe.N(b) * du(k, j) * detJxW);
            }
          }

          for (int j = 0; j < nsd; j++) {
            /** This term represents the contribution of,
             * (w, \delta{u} . grad{u\delta{u'}})
             */
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (-du(i, j) * fe.N(a) * tauM * (fe.N(b) / dt + conv - diff_J) *
                    detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (-du_pre(i, j) * fe.N(a) * tauM * (fe.N(b) / dt) * detJxW);
            /** This term is the contribution of (w, u'.grad{delta{u}}) to the
             * Jacobian operator.
             */
            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
                (-fe.N(a) * tauM * NS(j) * fe.dN(b, j) * detJxW);

            /// Pressure part of the term which arise from (w,\delta{u'}
            /// .grad{u}), always the last diagonal term
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
                (-du(i, j) * fe.N(a) * tauM * fe.dN(b, j) * detJxW);
          }
        }

        /** The Reynolds Stress term: (w, u'.grad{u'}), we subsitute u' as
         * -tau*Res(NS) and expand.
         */
        for (int i = 0; i < nsd; i++) {
          /// Contribution of laplacian of velocity(diffusion) to the diagonal
          double diff_J = 0;
          for (int j = 0; j < nsd; j++) {
            diff_J += Coe_diff * fe.d2N(b, j, j);
          }

          /** Three terms arising from (w,\delta(u').grad{u'})
           * u' has to be expanded to -tau*res{NS}: when the linearisation acts
           * on the res{NS} it gives three terms. First term which goes in the
           * diagonals of the matrix is
           * (-grad{w}* tauM*res(NS), tauM*(d_t{\delta{u}} + conv -diff_J))
           * PS: grad{w}*tau*res(NS) is corssTermFineScalePart
           */
          Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
              (-crossTermFineScalePart * tauM * (fe.N(b) / dt + conv - diff_J) *
                  detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (-crossTermFineScalePart_pre * tauM * (fe.N(b) / dt) * detJxW);

          /** Second term from (w,\delta(u').grad{u'}) which goes to the off
           * diagonals is: (-grad{w}* tauM*res(NS), tauM*(\delta{u}. grad{u}))
           */
          for (int j = 0; j < nsd; j++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (-crossTermFineScalePart * tauM * du(i, j) * fe.N(b) * detJxW);
          }

          /** Third term from (w,\delta(u').grad{u'}) which is the contribution
           * of pressure and goes in the last diagonal term
           */
          Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
              (-crossTermFineScalePart * tauM * fe.dN(b, i) * detJxW);

          for (int j = 0; j < nsd; j++) {
            for (int k = 0; k < nsd; k++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                  (-tauM * NS(i) * fe.dN(a, k) * tauM * fe.N(b) * du(k, j) *
                      detJxW);
            }
          }

          /** Just as above terms which arise when(w,(u').grad{\delta{u'}}) is
           * expanded is given below.
           */
          for (int j = 0; j < nsd; j++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (-tauM * NS(i) * fe.dN(a, j) * tauM *
                    (fe.N(b) / dt + conv - diff_J) * detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (-tauM * NS_pre(i) * fe.dN(a, j) * tauM * (fe.N(b) / dt) * detJxW);
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
                (-tauM * NS(i) * fe.dN(a, j) * tauM * fe.dN(b, j) * detJxW);
          }
        }

        /** This term represents the fine scale pressure given by
         * (grad{w}, \delta{tauC*res{Cont}}), where Cont is the continuity
         * equation. See third term in equation (71) in Bazilev et al. (2007)
         */
        for (int i = 0; i < nsd; i++) {
          for (int j = 0; j < nsd; j++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (fe.dN(a, i) * tauC * fe.dN(b, j) * detJxW);
          }
        }
        /** pspg term from VMS theory: See second term in equation (71)
         * from Bazilevs et al. (2007)
         */
        for (int i = 0; i < nsd; i++) {
          double diff_J = 0;
          for (int j = 0; j < nsd; j++) {
            diff_J += Coe_diff * fe.d2N(b, j, j);
          }
          Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
              fe.N(a) * fe.dN(b, i) * detJxW +
                  fe.dN(a, i) * tauM * (fe.N(b) / dt + conv - diff_J) * detJxW;

          for (int j = 0; j < nsd; j++) {
            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
                fe.dN(a, j) * tauM * du(j, i) * fe.N(b) * detJxW;
          }

          Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) +=
              fe.dN(a, i) * tauM * fe.dN(b, i) * detJxW;
        }
      }
    }
  }

  void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
    const int elmID = fe.elem()->elm_id();
    if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::IN_GP) {
      return;
    } else if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::INTERCEPTED_GP) {
      auto triangles = ibmSolver_->getTriInOctant(elmID);
      if (point_in_any_geometry(fe.position(), triangles)) {
        return;
      }
    }

    double tauMScale = 1.0;
    if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::INTERCEPTED_GP){
      tauMScale = idata_->tauM_scale4Side;
    }
    else{
      tauMScale = idata_->tauM_scale;
    }
    calcAe_vms_theta(fe,Ae,tauMScale);
    return;

    using namespace TALYFEMLIB;
    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double detJxW = fe.detJxW();

    double Coe_diff = idata_->getDiffusionNS(t_);
    double Coe_bdyforce = idata_->getBodyForce(t_);
    /** Get u from the NodeData
     * NOTE: ValueFEM is defined on NodeData object, therefore it follows
     * indices defined in NodeData subclass.
     * NOTE: Every iteration the solution is copied into NodeData, we use these
     * solved fields from the NodeData object to calculate the be (NS residual)
     * of current iteration.
     */
    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, NSHTNodeData::VEL_X + i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + i);
    }

    /// Define velocity gradient tensor ( grad{u} )
    ZeroMatrix<double> du;
    du.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
      }
    }

    /** Calculate the laplacian of velocity
     * This is required for course scale residual of Navier-Stokes for
     * diffusion term
     */
    ZEROPTV d2u;
    /// loop for three directions of velocity (MAX NSD is no of velocity
    /// directions in the problem)
    /// PS: This assumes the first three degrees are always velocity, it is
    /// wise to keep it that way
    for (int dof = 0; dof < nsd; dof++) {
      /// Summing over three directions of velocity as it is laplacian
      for (int dir = 0; dir < nsd; dir++) {
        d2u(dof) += this->p_data_->value2DerivativeFEM(fe, dof, dir, dir);
      }
    }

    ZEROPTV dp;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::PRESSURE, i);
    }

    double theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);

    if (idata_->solverTypeNS == NSHTInputData::STABILIZED_NS) {
      /// The forcing term goes into the RHS
      ZEROPTV U = {1.0, 0.0, 0.0};
      TALYFEMLIB::TezduyarUpwindFE PG;
      PG.calcSUPGWeightingFunction(fe, u, Coe_diff);
      PG.calcPSPGWeightingFunction(fe, U, 1.0, Coe_diff);

      for (int a = 0; a < nbf; a++) {
        for (int b = 0; b < nbf; b++) {
          /// NS terms
          double conv = 0.0;
          for (int i = 0; i < nsd; i++) {
            conv += fe.dN(b, i) * u(i);
          }

          /// normal
          for (int i = 0; i < nsd; i++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += fe.N(a) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                  fe.N(a) * du(i, j) * fe.N(b) * detJxW + Coe_diff * fe.dN(a, j) * fe.dN(b, i) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += Coe_diff * fe.dN(a, j) * fe.dN(b, j) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -fe.dN(a, i) * fe.N(b) * detJxW;
          }

          /// SUPG
          for (int i = 0; i < nsd; i++) {

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += PG.SUPG(a) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += PG.SUPG(a) * du(i, j) * fe.N(b) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += PG.SUPG(a) * fe.dN(b, i) * detJxW;
          }

          /// PSPG
          for (int i = 0; i < nsd; i++) {
            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
                fe.N(a) * fe.dN(b, i) * detJxW + PG.PSPG(a, i) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++)
              Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) += PG.PSPG(a, j) * du(j, i) * fe.N(b) * detJxW;

            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) += PG.PSPG(a, i) * fe.dN(b, i) * detJxW;
          }
        }
      }
    } else if (idata_->solverTypeNS == NSHTInputData::RBVMS_NS) {
      const double Ci_f = idata_->Ci_f;

      /// Get the cofactor matrix from FEELm class
      ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      /// G_{ij} in equation 65 in Bazilevs et al. (2007)
      ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++) {
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
          }
        }
      }

      /// Equation 67 in Bazilevs et al. (2007)
      double u_Gu = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          u_Gu += u(i) * Ge(i, j) * u(j);
        }
      }

      /// Last term in Equation 63 in Bazilevs et al. (2007)
      double G_G_u = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
        }
      }

      /// Calculate the tau_M based on the projections calculated, Equation 63 in
      /// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
      /// in the paper it is set to 1.
      double tauM = idata_->tauM_scale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
      /// Equation 68 in Bazilevs et al. (2007)
      ZEROARRAY<double> ge;
      ge.redim(nsd);
      for (int i = 0; i < nsd; i++) {
        ge(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          ge(i) += ksiX(j, i);
        }
      }

      /// Equation 69 Bazilevs et al. (2007)
      double g_g = 0.0;
      for (int i = 0; i < nsd; i++) {
        g_g += ge(i) * ge(i);
      }

      /// Calculate continuity residual based on
      double tauC = 1.0 / (tauM * g_g);

      /// NS terms
      ZEROPTV convec;
      ZEROPTV diffusion;

      for (int i = 0; i < nsd; i++) {
        convec(i) = 0.0;
        diffusion(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          convec(i) += du(i, j) * u(j);
        }
        diffusion(i) += Coe_diff * d2u(i);
      }

      ZEROPTV NS;
      for (int i = 0; i < nsd; i++) {
        NS(i) = (u(i) - u_pre(i)) / dt_ + convec(i) + dp(i) - diffusion(i);
        /// Gravity body force direction
        if (i == idata_->G_dir) {
          NS(i) += -Coe_bdyforce * theta;
        }
      }

      double cont = 0.0;
      for (int i = 0; i < nsd; i++) {
        cont += du(i, i);
      }

      ///---------------Calculating the Jacobian operator--------------------///
      /// NS terms

      /** Course scale terms for the cross terms (terms 4 and 5 in equation 52
       * in Bazilevs et al. (2007)
       * PS: these are not calculated using the SUPG class, and this is an
       * approximation of (u, grad{u})
       */
      for (int a = 0; a < nbf; a++) {

        double crossTermVelocityPart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermVelocityPart += fe.dN(a, i) * u(i);
        }

        /// Actual fine scale contribution to cross terms from tauM(inverse
        /// estimate)
        double crossTermFineScalePart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermFineScalePart += fe.dN(a, i) * tauM * NS(i);
        }

        for (int b = 0; b < nbf; b++) {
          /// Convection term
          double conv = 0.0;
          for (int i = 0; i < nsd; i++) {
            conv += fe.dN(b, i) * u(i);
          }


          /// Adding terms to the Jacobian matrix.
          for (int i = 0; i < nsd; i++) {

            /// Transient terms and the convection term and part of the stress
            /// tensor in the diagonals
            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += fe.N(a) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += fe.N(a) * du(i, j) * fe.N(b) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += Coe_diff * fe.dN(a, j) * fe.dN(b, j) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -fe.dN(a, i) * fe.N(b) * detJxW;
          }

          /** crossTerm 1: This term is written as, (w, grad{u u'}), which is
         * weakened to (grad{w}, u\innerproduct u') which is then linearised.
         * Here u' is fine scale velocity
         * and u is resolved velocity. The fine scale velocity is approximated
         * as -tau*Res{NS} (Equation 58 bazilevs et al. (2007)),
         *
         */

          /// diagonal
          for (int i = 0; i < nsd; i++) {

            /// Contribution of laplacian of velocity(diffusion) to the diagonal
            double diff_J = 0;
            for (int j = 0; j < nsd; j++) {
              diff_J += Coe_diff * fe.d2N(b, j, j);
            }

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
                crossTermVelocityPart * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += crossTermVelocityPart * tauM * du(i, j) * fe.N(b) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += tauM * fe.dN(a, j) * NS(i) * fe.N(b) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += crossTermVelocityPart * tauM * fe.dN(b, i) * detJxW;
          }

          /** Crossterm2: This term can be mathematically written as,
         * (w, u'.grad{u}). In this term we do not further weaken it as done in
         * cross term 1
         */
          for (int i = 0; i < nsd; i++) {

            /// Contribution of laplacian of velocity(diffusion) to the diagonal
            double diff_J = 0;
            for (int j = 0; j < nsd; j++) {
              diff_J += Coe_diff * fe.d2N(b, j, j);
            }

            for (int j = 0; j < nsd; j++) {
              for (int k = 0; k < nsd; k++) {
                Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += -du(i, k) * fe.N(a) * tauM * fe.N(b) * du(k, j) * detJxW;
              }
            }

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                  -du(i, j) * fe.N(a) * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += -fe.N(a) * tauM * NS(j) * fe.dN(b, j) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -du(i, j) * fe.N(a) * tauM * fe.dN(b, j) * detJxW;
            }
          }

          /** The Reynolds Stress term: (w, u'.grad{u'}), we substitute u' as
         * -tau*Res(NS) and expand.
         */
          for (int i = 0; i < nsd; i++) {
            /** Three terms arising from (w,\delta(u').grad{u'})
             * u' has to be expanded to -tau*res{NS}: when
             * the linearisation acts
             * on the res{NS} it gives three terms. First
             * term which goes in the
             * diagonals of the matrix is
             * (-grad{w}* tauM*res(NS),
             * tauM*(d_t{\delta{u}} + conv -diff_J))
             * PS: grad{w}*tau*res(NS) is
             * crossTermFineScalePart
             */


            /// Contribution of laplacian of velocity(diffusion) to the diagonal
            double diff_J = 0;
            for (int j = 0; j < nsd; j++) {
              diff_J += Coe_diff * fe.d2N(b, j, j);
            }

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
                -crossTermFineScalePart * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += -crossTermFineScalePart * tauM * du(i, j) * fe.N(b) * detJxW;
            }

            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -crossTermFineScalePart * tauM * fe.dN(b, i) * detJxW;

            for (int j = 0; j < nsd; j++) {
              for (int k = 0; k < nsd; k++) {
                Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                    -tauM * NS(i) * fe.dN(a, k) * tauM * fe.N(b) * du(k, j) * detJxW;
              }
            }

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                  -tauM * NS(i) * fe.dN(a, j) * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -tauM * NS(i) * fe.dN(a, j) * tauM * fe.dN(b, j) * detJxW;
            }
          }

          /** This term represents the fine scale pressure given by
         * (grad{w}, \delta{tauC*res{Cont}}), where Cont is the continuity
         * equation. See third term in equation (71) in Bazilev et al. (2007)
         */
          for (int i = 0; i < nsd; i++) {
            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += fe.dN(a, i) * tauC * fe.dN(b, j) * detJxW;
            }
          }

          /** pspg term from VMS theory: See second term in equation (71)
         * from Bazilevs et al. (2007). This is the contribution of momentum
         * residual to the continuity stabilization.
         */
          for (int i = 0; i < nsd; i++) {
            double diff_J = 0.0;
            for (int j = 0; j < nsd; j++) {
              diff_J += Coe_diff * fe.d2N(b, j, j);
            }

            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
                (fe.N(a) * fe.dN(b, i) * detJxW) + (fe.dN(a, i) * tauM * ((fe.N(b) / dt_) + conv - diff_J) * detJxW);

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) += fe.dN(a, j) * tauM * du(j, i) * fe.N(b) * detJxW;
            }
            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) += fe.dN(a, i) * tauM * fe.dN(b, i) * detJxW;
          }

        }
      }
    }

#ifndef NDEBUG
    if (idata_->Debug_Integrand) {
      int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
      FILE *fp = fopen("Ae_dendro5.dat", "w");
      for (int i = 0; i < 8; ++i) {
        for (int dof1 = 0; dof1 < 4; ++dof1) {
          for (int j = 0; j < 8; ++j) {
            for (int dof2 = 0; dof2 < 4; ++dof2) {
              fprintf(fp, "%.10f\t", Ae(interchangedmat[i] * 4 + dof1, interchangedmat[j] * 4 + dof2));
            }
          }
          fprintf(fp, "\n");
        }
      }
      fclose(fp);
    }
#endif
  }

  void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {
    const int elmID = fe.elem()->elm_id();
    if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::IN_GP) {
      return;
    } else if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::INTERCEPTED_GP) {
      auto triangles = ibmSolver_->getTriInOctant(elmID);
      if (point_in_any_geometry(fe.position(), triangles)) {
        return;
      }
    }
    double tauMScale = 1.0;
    if (ibmSolver_->getInOutElementGP(elmID) == inOutInfo::INTERCEPTED_GP){
      tauMScale = idata_->tauM_scale4Side;
    }
    else{
      tauMScale = idata_->tauM_scale;
    }
    calcbe_vms_theta(fe,be,tauMScale);
    return;
    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double detJxW = fe.detJxW();
    double Coe_diff = idata_->getDiffusionNS(t_);
    double Coe_bdyforce = idata_->getBodyForce(t_);

    /** Get u and u_pre from the NodeData
   * NOTE: ValueFEM is defined on NodeData object, therefore it follows
   * indices defined in NodeData subclass.
   * NOTE: Every iteration the solution is copied into NodeData, we use these
   * solved fields from the NodeData object to calculate the be (NS residual)
   * of current iteration.
   */
    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, NSHTNodeData::VEL_X + i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + i);
    }

    /// Define velocity gradient tensor ( grad{u} )
    TALYFEMLIB::ZeroMatrix<double> du;
    du.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
      }
    }

    /** Calculate the laplacian of velocity
    * This is required for course scale residual of Navier-Stokes for
    * diffusion term
    */
    ZEROPTV d2u;
    /// loop for three directions of velocity (MAX NSD is no of velocity
    /// directions in the problem)
    /// PS: This assumes the first three degrees are always velocity, it is
    /// wise to keep it that way
    for (int dof = 0; dof < nsd; dof++) {
      /// Summing over three directions of velocity as it is laplacian
      for (int dir = 0; dir < nsd; dir++) {
        d2u(dof) += this->p_data_->value2DerivativeFEM(fe, dof, dir, dir);
      }
    }

    double p = this->p_data_->valueFEM(fe, NSHTNodeData::PRESSURE);
    double theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);

    ZEROPTV dp;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::PRESSURE, i);
    }

    /// NS terms
    ZEROPTV convec;
    ZEROPTV diffusion;

    for (int i = 0; i < nsd; i++) {
      convec(i) = 0.0;
      diffusion(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        convec(i) += du(i, j) * u(j);
      }
      diffusion(i) += Coe_diff * d2u(i);
    }

    /** Construct the Navier Stokes equation without diffusion
     * Here diffusion is not present as its contribution to stabilizers for
     * linear basis functions is zero
     */
    /// NS = Coe_conv(du/dt + u . grad{u}) + grad{p}
    ZEROPTV NS;
    for (int i = 0; i < nsd; i++) {
      NS(i) = (u(i) - u_pre(i)) / dt_ + convec(i) + dp(i) - diffusion(i);
      /// Gravity body force direction
      if (i == idata_->G_dir) {
        NS(i) += -Coe_bdyforce * theta;
      }
    }

    /** Calculate continuity residual for PSPG stabilizer
     * This residual is a essential for calculating the PSPG stabilizer
     */
    double cont = 0.0;
    for (int i = 0; i < nsd; i++) {
      cont += du(i, i);
    }

    if (idata_->solverTypeNS == NSHTInputData::STABILIZED_NS) {
      /// set PSPG and SUPG
      ZEROPTV U = {1.0, 0.0, 0.0};

      TALYFEMLIB::TezduyarUpwindFE PG;
      PG.calcSUPGWeightingFunction(fe, u, Coe_diff);
      PG.calcPSPGWeightingFunction(fe, U, 1.0, Coe_diff);

      for (int a = 0; a < nbf; a++) {
        ZEROPTV NS_stable_terms, normal_NS, NS_contrib;
        /** Calculating diffusion term of Navier-Stokes at current iteration.
         * This is done using the stress form of the diffusion term given by
         * (Coe_diff* div{(grad{u_n}+T{grad{u_n}})}), here T{} is the transpose
         * operator.
         */
        ZEROPTV diff;
        for (int i = 0; i < nsd; i++) {
          diff(i) = 0.0;
          for (int j = 0; j < nsd; j++) {
            diff(i) += Coe_diff * fe.dN(a, j) * (du(i, j) + du(j, i));
          }
        }

        for (int i = 0; i < nsd; i++) {
          normal_NS(i) =
              fe.N(a) * (u(i) - u_pre(i)) / dt_ * detJxW
                  + fe.N(a) * (convec(i)) * detJxW
                  + (-fe.dN(a, i) * p) * detJxW
                  + (diff(i)) * detJxW;
          if (i == idata_->G_dir) {
            normal_NS(i) += -Coe_bdyforce * fe.N(a) * theta * detJxW;
          }

          NS_stable_terms(i) = (PG.SUPG(a) * NS(i)) * detJxW;
          NS_contrib(i) = normal_NS(i) + NS_stable_terms(i);
        }

        /** Now we have to add the contribution of pressure to the be vector.
         * This naturally will be the last term of the vector
         */

        ///< We first calculate the PSPG stabilisation to the pressure term, which
        ///< still takes the NS(i) residual
        double Cont_stable_term = 0.0;
        for (int i = 0; i < nsd; i++) {
          Cont_stable_term += PG.PSPG(a, i) * NS(i);
        }

        double Cont_contrib = fe.N(a) * (cont) * detJxW + Cont_stable_term * detJxW;

        for (int i = 0; i < nsd; i++) {
          be((nsd + 1) * a + i) += NS_contrib(i);
        }
        be((nsd + 1) * a + nsd) += Cont_contrib;
      }

    } else if (idata_->solverTypeNS == NSHTInputData::RBVMS_NS) {
      const double Ci_f = idata_->Ci_f;
      /// Get the cofactor matrix from FEELm class
      TALYFEMLIB::ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      /// G_{ij} in equation 65 in Bazilevs et al. (2007)
      TALYFEMLIB::ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++) {
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
          }
        }
      }

      /// Equation 67 in Bazilevs et al. (2007)
      double u_Gu = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          u_Gu += u(i) * Ge(i, j) * u(j);
        }
      }

      /// Last term in Equation 63 in Bazilevs et al. (2007)
      double G_G_u = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
        }
      }

      /// Calculate the tau_M based on the projections calculated, Equation 63 in
      /// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
      /// in the paper it is set to 1.
      double tauM = idata_->tauM_scale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
      /// Equation 68 in Bazilevs et al. (2007)
      TALYFEMLIB::ZEROARRAY<double> ge;
      ge.redim(nsd);
      for (int i = 0; i < nsd; i++) {
        ge(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          ge(i) += ksiX(j, i);
        }
      }

      /// Equation 69 Bazilevs et al. (2007)
      double g_g = 0.0;
      for (int i = 0; i < nsd; i++) {
        g_g += ge(i) * ge(i);
      }

      /// Calculate continuity residual based on
      double tauC = 1.0 / (tauM * g_g);

      for (int a = 0; a < nbf; a++) {

        double crossTermVelocityPart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermVelocityPart += fe.dN(a, i) * u(i);
        }

        ZEROPTV cross1, cross2, reystress, pressurep, normal_NS, NScontrib;

        ZEROPTV diff;
        for (int i = 0; i < nsd; i++) {
          diff(i) = 0.0;
          for (int j = 0; j < nsd; j++) {
            diff(i) += Coe_diff * fe.dN(a, j) * (du(i, j));
          }
        }

        for (int i = 0; i < nsd; i++) {
          normal_NS(i) =
              fe.N(a) * (u(i) - u_pre(i)) / dt_ * detJxW + fe.N(a) * convec(i) * detJxW + (-fe.dN(a, i) * p) * detJxW
                  + (diff(i)) * detJxW;


          /// Gravity body force direction
          if (i == idata_->G_dir) {
            normal_NS(i) += -Coe_bdyforce * fe.N(a) * theta * detJxW;
          }

          cross1(i) = (tauM * crossTermVelocityPart * NS(i)) * detJxW;
          cross2(i) = 0.0;
          reystress(i) = 0.0;
          for (int j = 0; j < nsd; j++) {
            /// Second cross term
            cross2(i) += fe.N(a) * (du(i, j) * tauM * NS(j)) * detJxW;
            /// The Reynolds stress term
            reystress(i) += fe.dN(a, j) * (tauM * NS(i) * tauM * NS(j)) * detJxW;
          }
          /// Contribution of pressure
          pressurep(i) = fe.dN(a, i) * (tauC * cont) * detJxW;

          /// Add all the terms to the vector
          NScontrib(i) = normal_NS(i) + cross1(i) - cross2(i) - reystress(i) + pressurep(i);
        }

        double pspg = 0.0;
        for (int i = 0; i < nsd; i++) {
          /// Fine scale contribution to pressure
          pspg += fe.dN(a, i) * tauM * NS(i);
        }

        /// Contribution of this term goes to last element of the vector
        double contContrib = (fe.N(a) * (cont) * detJxW) + (pspg * detJxW);

        /// Adding all the calculated terms to the vector
        for (int i = 0; i < nsd; i++) {
          be((nsd + 1) * a + i) += NScontrib(i);
        }
        be((nsd + 1) * a + nsd) += contContrib;
      }
    }
#ifndef NDEBUG
    if (idata_->Debug_Integrand) {
      int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
      FILE *fp = fopen("be_dendro5.dat", "w");
      for (int i = 0; i < 8; ++i) {
        for (int dof1 = 0; dof1 < 4; ++dof1) {
          fprintf(fp, "%.10f\t", be(interchangedmat[i] * 4 + dof1));
        }
      }
      fprintf(fp, "\n");
      fclose(fp);
    }
#endif
  }

  void Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZeroMatrix<double> &Ae) override {

  }

  void Integrands4side_be(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZEROARRAY<double> &be) override {
  }

  void ibm_Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe,
                              ZeroMatrix<double> &Ae,
                              const Geom<NSHTNodeData> *geo,
                              const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                              const Point &node,
                              const Point &h) {
    const ZEROPTV &normal = gpinfo.gp_normal;
    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double Cb_f = idata_->Cb_f;
    const double Coe_weak = Cb_f;
    const double detSideJxW = fe.detJxW();
    double Coe_diff = idata_->getDiffusionNS(t_);
    double Coe_bdyforce = idata_->getBodyForce(t_);
    /** Get u from the NodeData
     * NOTE: ValueFEM is defined on NodeData object, therefore it follows
     * indices defined in NodeData subclass.
     * NOTE: Every iteration the solution is copied into NodeData, we use these
     * solved fields from the NodeData object to calculate the be (NS residual)
     * of current iteration.
     */
    ZEROPTV u;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, i);
    }

    /// Define velocity gradient tensor ( grad{u} )
    ZeroMatrix<double> du;
    du.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
      }
    }

    /** Calculate the laplacian of velocity
    * This is required for course scale residual of Navier-Stokes for
    * diffusion term
    */
    ZEROPTV d2u;
    /// loop for three directions of velocity (MAX NSD is no of velocity
    /// directions in the problem)
    /// PS: This assumes the first three degrees are always velocity, it is
    /// wise to keep it that way
    for (int dof = 0; dof < nsd; dof++) {
      /// Summing over three directions of velocity as it is laplacian
      for (int dir = 0; dir < nsd; dir++) {
        d2u(dof) += this->p_data_->value2DerivativeFEM(fe, dof, dir, dir);
      }
    }

    ZEROPTV dp;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::PRESSURE, i);
    }

    double theta;
    theta = this->p_data_->valueFEM(fe, NSHTNodeData::TEMPERATURE);

    // wall normal distance
    double hb = MathOp::normalDistance(fe.position(), normal, node, h);
    if (hb < 0) {
      PrintStatus("Elm node:\t", ZEROPTV{node.x(), node.y(), node.z()});
      PrintStatus("gp_pos:\t", fe.position());
      PrintStatus("normal:\t", normal);
      for (int i = 0; i < 3; ++i) {
        PrintStatus("triangle:\t", gpinfo.tri_nodes[i]);
      }
    }

    /// Based on what kind of solver chosen, choose the corresponding solver (SUPG or VMS)
    if (idata_->solverTypeNS == NSHTInputData::STABILIZED_NS) {
      if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
        for (int a = 0; a < nbf; a++) {
          double supg = 0.0;
          for (int m = 0; m < nsd; m++) {
            supg += fe.N(a) * normal(m);
          }

          ZEROPTV supg1, supg2;
          for (int m = 0; m < nsd; m++) {
            supg1(m) = 0.0;
            supg2(m) = 0.0;
            for (int n = 0; n < nsd; n++) {
              supg1(m) += fe.dN(a, n) * normal(n);
              supg2(m) += fe.dN(a, m) * normal(n);
            }
          }

          for (int b = 0; b < nbf; b++) {
            double supgb = 0.0;
            for (int m = 0; m < nsd; m++)
              supgb += fe.dN(b, m) * normal(m);

            for (int j = 0; j < nsd; j++) {
              /// diffusion_1
              Ae((nsd + 1) * a + j, (nsd + 1) * b + j) += -Coe_diff * fe.N(a) * (supgb) * detSideJxW
                  /// adjoint diffusion
                  - Coe_diff * (supg1(j) + supg2(j)) * fe.N(b) * detSideJxW
                      /// penalty
                  + (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
//                + Coe_weak * fe.N(a) * fe.N(b) * detSideJxW;
              /// adjoint pressure
              Ae((nsd + 1) * (a + 1) - 1, (nsd + 1) * b + j) += -fe.N(a) * normal(j) * fe.N(b) * detSideJxW;
              /// pressure
              Ae((nsd + 1) * a + j, (nsd + 1) * (b + 1) - 1) += fe.N(a) * fe.N(b) * normal(j) * detSideJxW;

              for (int k = 0; k < nsd; k++) {
                /// diffusion_2
                Ae((nsd + 1) * a + j, (nsd + 1) * b + k) += -Coe_diff * fe.N(a) * fe.dN(b, j) * normal(k) * detSideJxW;
              }
            }
          }
        }
      } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
        for (int a = 0; a < nbf; a++) {
          for (int b = 0; b < nbf; b++) {
            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + j, (nsd + 1) * b + j) +=
                  /// penalty (from two sides)
                  2 * (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
//                2 * Coe_weak * fe.N(a) * fe.N(b) * detSideJxW;
            }
          }
        }
      }
    } else if (idata_->solverTypeNS == NSHTInputData::RBVMS_NS) {
      double thetaTimeStepping = idata_->thetaTimeStepping4Side;
      if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
        for (int a = 0; a < nbf; a++) {
          double supg = 0.0;
          for (int m = 0; m < nsd; m++) {
            supg += fe.N(a) * normal(m);
          }

          ZEROPTV supg1;
          for (int m = 0; m < nsd; m++) {
            supg1(m) = 0.0;
            for (int n = 0; n < nsd; n++) {
              supg1(m) += fe.dN(a, n) * normal(n);
            }
          }

          for (int b = 0; b < nbf; b++) {
            double supgb = 0.0;
            for (int m = 0; m < nsd; m++) {
              supgb += fe.dN(b, m) * normal(m);
            }
            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + j, (nsd + 1) * b + j) +=
                  /// diffusion_1
                  -thetaTimeStepping * Coe_diff * fe.N(a) * (supgb) * detSideJxW
                      /// adjoint diffusion
                      - thetaTimeStepping * Coe_diff * supg1(j) * fe.N(b) * detSideJxW
                          /// penalty
                      + thetaTimeStepping * (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
//                    + Coe_weak * fe.N(a) * fe.N(b) * detSideJxW;

              /// adjoint pressure
              Ae((nsd + 1) * (a + 1) - 1, (nsd + 1) * b + j) +=
                  -thetaTimeStepping*fe.N(a) * normal(j) * fe.N(b) * detSideJxW;
              /// pressure
              Ae((nsd + 1) * a + j, (nsd + 1) * (b + 1) - 1) +=
                  thetaTimeStepping*fe.N(a) * fe.N(b) * normal(j) * detSideJxW;
            }
          }
        }
      } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
        for (int a = 0; a < nbf; a++) {
          for (int b = 0; b < nbf; b++) {
            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + j, (nsd + 1) * b + j) +=
                  /// penalty
                  2 * (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * fe.N(b) * detSideJxW;
//                2 * Coe_weak * fe.N(a) * fe.N(b) * detSideJxW;
            }
          }
        }
      }

    }

#ifndef NDEBUG
    if (idata_->Debug_Integrand) {
      int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
      FILE *fp = fopen("Ae_ibm_dendro5.dat", "w");
      for (int i = 0; i < 8; ++i) {
        for (int dof1 = 0; dof1 < 4; ++dof1) {
          for (int j = 0; j < 8; ++j) {
            for (int dof2 = 0; dof2 < 4; ++dof2) {
              fprintf(fp, "%.10f\t", Ae(interchangedmat[i] * 4 + dof1, interchangedmat[j] * 4 + dof2));
            }
          }
          fprintf(fp, "\n");
        }
      }
      fclose(fp);
    }
#endif
  }

  void ibm_Integrands4side_be(const TALYFEMLIB::FEMElm &fe,
                                     ZEROARRAY<double> &be,
                                     const Geom<NSHTNodeData> *geo,
                                     const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                     const Point &node,
                                     const Point &h) {
    const ZEROPTV &normal = gpinfo.gp_normal;
    const ZEROPTV &vel = gpinfo.gp_velocity;
    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double Cb_f = idata_->Cb_f;
    const double Coe_weak = Cb_f;
    const double detSideJxW = fe.detJxW();
    double Coe_diff = idata_->getDiffusionNS(t_);
    double Coe_bdyforce = idata_->getBodyForce(t_);
    /** Get u from the NodeData
     * NOTE: ValueFEM is defined on NodeData object, therefore it follows
     * indices defined in NodeData subclass.
     * NOTE: Every iteration the solution is copied into NodeData, we use these
     * solved fields from the NodeData object to calculate the be (NS residual)
     * of current iteration.
     */
    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + i);
    }

    /// Define velocity gradient tensor ( grad{u} )
    ZeroMatrix<double> du,du_pre;


    du.redim(nsd, nsd);
    du_pre.redim(nsd,nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
        du_pre(i, j) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::NUM_VARS + i, j);
      }
    }

    double p = this->p_data_->valueFEM(fe, NSHTNodeData::PRESSURE);
    double p_pre = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + NSHTNodeData::PRESSURE);

    ZEROPTV diff1, diff2;
    ZEROPTV diff1_pre, diff2_pre;
    for (int i = 0; i < nsd; i++) {
      diff1(i) = 0.0;
      diff2(i) = 0.0;
      diff1_pre(i) = 0.0;
      diff2_pre(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        diff1(i) += du(i, j) * normal(j);
        diff2(i) += du(j, i) * normal(j);
        diff1_pre(i) += du_pre(i, j) * normal(j);
        diff2_pre(i) += du_pre(j, i) * normal(j);

      }
    }

    /// wall normal distance (the maximum of 8 nodes)
    double hb = MathOp::normalDistance(fe.position(), normal, node, h);
//  PrintStatus(hb);
    if (hb < 0) {
      PrintStatus("Elm node:\t", ZEROPTV{node.x(), node.y(), node.z()});
      PrintStatus("gp_pos:\t", fe.position());
      PrintStatus("normal:\t", normal);
      for (int i = 0; i < 3; ++i) {
        PrintStatus("triangle:\t", gpinfo.tri_nodes[i]);
      }
    }

    /// Based on what kind of solver chosen, choose the corresponding solver (SUPG or VMS)
    if (idata_->solverTypeNS == NSHTInputData::STABILIZED_NS) {
      /// switch between pure 2D and 3D
      if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
        for (int a = 0; a < nbf; a++) {
          double supg = 0.0;
          for (int m = 0; m < nsd; m++) {
            supg += fe.N(a) * normal(m);
          }

          ZEROPTV supg1, supg2;
          for (int m = 0; m < nsd; m++) {
            supg1(m) = 0.0;
            supg2(m) = 0.0;
            for (int n = 0; n < nsd; n++) {
              supg1(m) += fe.dN(a, n) * normal(n);
              supg2(m) += fe.dN(a, m) * normal(n);
            }
          }

          for (int j = 0; j < nsd; j++) {
            be((nsd + 1) * a + j) +=
                /// pressure
                 fe.N(a) * (p * normal(j)) * detSideJxW
                    /// diffusion
                    - fe.N(a) * (Coe_diff * (diff1(j) + diff2(j))) * detSideJxW
                        /// adjoint diffusion
                    - Coe_diff * (supg1(j) + supg2(j)) * (u(j) - vel(j)) * detSideJxW
                        /// penalty
                    + (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * (u(j) - vel(j)) * detSideJxW;
//                  + Coe_weak * fe.N(a) * (u(j) - vel(j)) * detSideJxW;

            /// adjoint pressure
            be((nsd + 1) * (a + 1) - 1) += -fe.N(a) * normal(j) * (u(j) - vel(j)) * detSideJxW;
          }
        }
      } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
        /// almost all the terms cancelled out
        for (int a = 0; a < nbf; a++) {
          for (int j = 0; j < nsd; j++) {
            be((nsd + 1) * a + j) +=
                /// penalty
                2 * (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * (u(j) - vel(j)) * detSideJxW;
//              2 * Coe_weak * fe.N(a) * (u(j) - vel(j)) * detSideJxW;
          }
        }
      }

    } else if (idata_->solverTypeNS == NSHTInputData::RBVMS_NS) {
      double thetaTimeStepping = idata_->thetaTimeStepping4Side;

      if (geo->def_.type != GeomDef::INVALID && geo->def_.type != GeomDef::MESHOBJECT_2D) {
        for (int a = 0; a < nbf; a++) {
          ZEROPTV supg1;
          for (int m = 0; m < nsd; m++) {
            supg1(m) = 0.0;
            for (int n = 0; n < nsd; n++) {
              supg1(m) += fe.dN(a, n) * normal(n);
            }
          }

          for (int j = 0; j < nsd; j++) {
            be((nsd + 1) * a + j) +=
                ///pressure
                ((thetaTimeStepping * fe.N(a) * (p * normal(j)))
                + ((1 - thetaTimeStepping)*fe.N(a) * (p_pre * normal(j)))) * detSideJxW

                ///diffusion
                - ((thetaTimeStepping*fe.N(a) * (Coe_diff * (diff1(j) /*+diff2(j)*/ )))
                +  ((1-thetaTimeStepping)*fe.N(a) * (Coe_diff * (diff1_pre(j) /*+diff2(j)*/ )))) * detSideJxW

                ///adjoint diffusion
                - ((thetaTimeStepping*Coe_diff * supg1(j) * (u(j) - vel(j)))
                +  ((1-thetaTimeStepping)*Coe_diff * supg1(j) * (u_pre(j) - vel(j)))) * detSideJxW

                /// penalty
                + ((thetaTimeStepping*(Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * (u(j) - vel(j)))
                + ((1-thetaTimeStepping)*(Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * (u_pre(j) - vel(j)))) * detSideJxW;
//                  + Coe_weak * fe.N(a) * (u(j) - vel(j)) * detSideJxW;


            /// adjoint pressure
            be((nsd + 1) * (a + 1) - 1) +=
                -(thetaTimeStepping*fe.N(a) * normal(j) * (u(j) - vel(j))
                +(1-thetaTimeStepping)*fe.N(a) * normal(j) * (u_pre(j) - vel(j))) * detSideJxW;
          }
        }
      } else if (geo->def_.type == GeomDef::MESHOBJECT_2D) {
        /// almost all the terms cancelled out
        for (int a = 0; a < nbf; a++) {
          for (int j = 0; j < nsd; j++) {
            be((nsd + 1) * a + j) +=
                /// penalty
                2 * (Coe_diff * Cb_f / pow(hb, idata_->orderOfCb)) * fe.N(a) * (u(j) - vel(j)) * detSideJxW;
//              2 * Coe_weak * fe.N(a) * (u(j) - vel(j)) * detSideJxW;
          }
        }
      }

    }
#ifndef NDEBUG
    if (idata_->Debug_Integrand) {
      int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
      FILE *fp = fopen("be_ibm_dendro5.dat", "w");
      for (int i = 0; i < 8; ++i) {
        for (int dof1 = 0; dof1 < 4; ++dof1) {
          fprintf(fp, "%.10f\t", be(interchangedmat[i] * 4 + dof1));
        }
      }
      fprintf(fp, "\n");
      fclose(fp);
    }
#endif
  }

  void calcbe_vms_theta(const FEMElm &fe, ZEROARRAY<double> &be, double tauMScale) {
    const double thetaTimeStepping = idata_->thetaTimeStepping;
    const double Coe_diff = idata_->getDiffusionNS(t_);
    const double Ci_f = idata_->Ci_f;
    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double detJxW = fe.detJxW();
    const bool timeStab = idata_->iftimeStab;
    const double dt = this->dt_;

    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, NSHTNodeData::VEL_X + i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS + i);
    }

    ZeroMatrix<double> du, du_pre;
    du.redim(nsd, nsd);
    du_pre.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::VEL_X + i, j);
        du_pre(i, j) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::NUM_VARS + i, j);
      }
    }

    ZEROPTV d2u(0, 0, 0), d2u_pre(0, 0, 0);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        d2u(i) += this->p_data_->value2DerivativeFEM(fe, NSHTNodeData::VEL_X + i, j, j);
        d2u_pre(i) += this->p_data_->value2DerivativeFEM(fe, NSHTNodeData::NUM_VARS + i, j, j);
      }
    }

    double p = this->p_data_->valueFEM(fe, NSHTNodeData::PRESSURE);
    double p_pre = this->p_data_->valueFEM(fe, NSHTNodeData::NUM_VARS+ NSHTNodeData::PRESSURE);
    ZEROPTV dp, dp_pre;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::PRESSURE, i);
      dp_pre(i) = this->p_data_->valueDerivativeFEM(fe, NSHTNodeData::NUM_VARS + NSHTNodeData::PRESSURE, i);
    }

    ZEROPTV convec, convec_pre;
    for (int i = 0; i < nsd; i++) {
      convec(i) = 0.0;
      convec_pre(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        convec(i) += du(i, j) * u(j);
        convec_pre(i) += du_pre(i, j) * u_pre(j);
      }
    }

    ZEROPTV diffusion, diffusion_pre;
    for (int i = 0; i < nsd; i++) {
      diffusion(i) = Coe_diff * d2u(i);
      diffusion_pre(i) = Coe_diff * d2u_pre(i);
    }

    ZEROPTV NS, NS_pre;
    for (int i = 0; i < nsd; i++) {
      NS(i) = (u(i) - u_pre(i)) / dt + convec(i) + dp(i) - diffusion(i);
      NS_pre(i) = (u(i) - u_pre(i)) / dt + convec_pre(i) + dp_pre(i) - diffusion_pre(i);
    }

    double cont = 0.0, cont_pre = 0.0;
    for (int i = 0; i < nsd; i++) {
      cont += du(i, i);
      cont_pre += du_pre(i, i);
    }

    TALYFEMLIB::ZeroMatrix<double> ksiX;
    ksiX.redim(nsd, nsd);

    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        ksiX(i, j) = fe.cof(j, i) / fe.jacc();
      }
    }

    /// G_{ij} in equation 65 in Bazilevs et al. (2007)
    TALYFEMLIB::ZeroMatrix<double> Ge;
    Ge.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        Ge(i, j) = 0.0;
        for (int k = 0; k < nsd; k++) {
          Ge(i, j) += ksiX(k, i) * ksiX(k, j);
        }
      }
    }

    /// Equation 67 in Bazilevs et al. (2007)
    double u_Gu = 0.0;
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        u_Gu += u(i) * Ge(i, j) * u(j);
      }
    }

    /// Last term in Equation 63 in Bazilevs et al. (2007)
    double G_G_u = 0.0;
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
      }
    }



    /// Calculate the tau_M based on the projections calculated, Equation 63 in
    /// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
    /// in the paper it is set to 1.
    double tauM = tauMScale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
    /// Equation 68 in Bazilevs et al. (2007)
    TALYFEMLIB::ZEROARRAY<double> ge;
    ge.redim(nsd);
    for (int i = 0; i < nsd; i++) {
      ge(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        ge(i) += ksiX(j, i);
      }
    }

    /// Equation 69 Bazilevs et al. (2007)
    double g_g = 0.0;
    for (int i = 0; i < nsd; i++) {
      g_g += ge(i) * ge(i);
    }

    /// Calculate continuity residual based on
    double tauC =idata_->tauC_scale / (tauM * g_g);
    tauM = tauMScale / sqrt((static_cast<int>(timeStab)*4.0 / (dt_ * dt_)) + u_Gu + G_G_u);
    for (int a = 0; a < nbf; a++) {
      double crossTermVelocityPart = 0.0, crossTermVelocityPart_pre = 0.0;
      for (int i = 0; i < nsd; i++) {
        crossTermVelocityPart += fe.dN(a, i) * u(i);
        crossTermVelocityPart_pre += fe.dN(a, i) * u_pre(i);
      }


      /** Constructing the RHS vector of the residual of the equation
     * This involves just defining the terms involved in the equation without
     * any linearisation, as this is calculated from the existing data on the
     * NodeData structure
     */

      /// All the terms involved in the equation
      ZEROPTV time_der;
      ZEROPTV cross1, cross2, reystress, pressurep, diff, press, normal_NS, NScontrib;
      ZEROPTV cross1_pre, cross2_pre, reystress_pre, pressurep_pre, diff_pre, press_pre, normal_NS_pre, NScontrib_pre;

      /// Diffusion term
      for (int i = 0; i < nsd; i++) {
        diff(i) = 0.0;
        diff_pre(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          diff(i) += Coe_diff * fe.dN(a, j) * (du(i, j) /*+ du(j, i)*/);
          diff_pre(i) += Coe_diff * fe.dN(a, j) * (du_pre(i, j) /*+ du_pre(j, i)*/);
        }
      }

      /// Terms of Normal Navier Stokes without any additional VMS terms
      for (int i = 0; i < nsd; i++) {
        time_der(i) = fe.N(a) * (u(i) - u_pre(i)) / dt * detJxW;
        press(i) = (-fe.dN(a, i) * p) * detJxW;
        press_pre(i) = (-fe.dN(a, i) * p_pre) * detJxW;
        normal_NS(i) = fe.N(a) * (convec(i)) * detJxW + (diff(i)) * detJxW ;
        normal_NS_pre(i) = fe.N(a) * convec_pre(i) * detJxW + diff_pre(i) * detJxW ;

        /// first cross term
        cross1(i) = (tauM * crossTermVelocityPart * NS(i)) * detJxW;
        cross1_pre(i) = (tauM * crossTermVelocityPart_pre * NS_pre(i)) * detJxW;

        cross2(i) = 0.0, cross2_pre(i) = 0.0;
        reystress(i) = 0.0, reystress_pre(i) = 0.0;

        for (int j = 0; j < nsd; j++) {
          /// Second cross term
          cross2(i) += fe.N(a) * (du(i, j) * tauM * NS(j)) * detJxW;
          cross2_pre(i) += fe.N(a) * (du_pre(i, j) * tauM * NS_pre(j)) * detJxW;

          /// The Reynolds stress term
          reystress(i) += fe.dN(a, j) * (tauM * NS(i) * tauM * NS(j)) * detJxW;
          reystress_pre(i) += fe.dN(a, j) * (tauM * NS_pre(i) * tauM * NS_pre(j)) * detJxW;
        }

        /// Contribution of pressure
        pressurep(i) = fe.dN(a, i) * (tauC * cont) * detJxW;

        /// Add all the terms to the vector
        NScontrib(i) = time_der(i) + thetaTimeStepping * (press(i) + normal_NS(i) + cross1(i) - cross2(i) - reystress(i) + pressurep(i));
        NScontrib_pre(i) = (1.0 - thetaTimeStepping) * (press_pre(i) + normal_NS_pre(i) + 1.0*cross1_pre(i) - 1.0*cross2_pre(i) - 1.0*reystress_pre(i));
      }

      double pspg = 0.0;

      for (int i = 0; i < nsd; i++) {
        /// Fine scale contribution to pressure
        pspg += fe.dN(a, i) * tauM * NS(i);
      }
      /// Contribution of this term goes to last element of the vector
      double contContrib = fe.N(a) * (cont) * detJxW + pspg * detJxW;

      /// Adding all the calculated terms to the vector
      for (int i = 0; i < nsd; i++) {
        be((nsd + 1) * a + i) += NScontrib(i) + NScontrib_pre(i);
      }
      be((nsd + 1) * a + nsd) += contContrib;
    }


  }


 private:
  NSHTInputData *idata_;
  IBMSolver<NSHTNodeData> *ibmSolver_;

  bool point_in_any_geometry(const ZEROPTV &pt, std::vector<triInfo> &triangles) const {
    std::vector<double> dots;
    for (const auto &tri : triangles) {
      dots.push_back(tri.normal.innerProduct(pt - tri.centroid));
    }

    if (triangles.empty()) {
      return ibmSolver_->point_in_any_geometry(pt);
    } else {
      if (std::all_of(dots.begin(), dots.end(), [](double d) { return d < 0; })) {
        return false;
      } else if (std::none_of(dots.begin(), dots.end(), [](double d) { return d < 0; })) {
        return true;
      } else {
        return ibmSolver_->point_in_any_geometry(pt);
      }
    }
    throw TALYFEMLIB::TALYException() << "can not happen!";
    return false;
  }

};

