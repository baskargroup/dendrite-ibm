Additional Dependencies
=======================
* LAPACK or Intel MKL

Compilation
============
```bash
mkdir build;
cd build;
cmake .. -DCMAKE_BUILD_TYPE=Release -DTENSOR=Yes
make
```

`DTENSOR=Yes` will enable the matrix assembly by using matrix - matrix multiplication,
otherwise the assembly is done by looping over the Gauss points.

Running
=======
* The config file from [NSHT_dendro5/tests/MatrixAssembly](../NSHT_dendro5/tests/MatrixAssembly) should be copied to the build directory.
* The Basis function can be changed from linear to quadratic by changing the option `BasisFunction = 1/2` for linear and quadratic respectively.
* Launch `mpirun -np $NPROC ./ns` for matrix tests
