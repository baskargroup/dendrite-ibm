
dt = 1
totalT = 2
surface_cost = 1000
gp_handle = 0
BasisFunction = 1

#NavierStokesSolverType = "stabilizedNS"
NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

### For comparison
HTOnly = false
SolveNS = false
SolveHT = true


background_mesh = {
  refine_l = 3
  refine_h = 3
  enable_subda = false
  min = [0, 0, 0]
  max = [2, 2, 2]
  refine_walls = false
}

### Coe setup
Cb_e = 50
Ci_e = 36
Cb_f = 50
Ci_f = 36
Ra = 2500
Pr = 1
tauM_scale = 1
G_dir = 2


### block iteration
blockTolerance = 1e-3
iterMaxBlock = 5

geometries = (
  {
    mesh_path = "sphere.stl"
    is_static = true
    position = [1.0, 1.0, 1.0]
    type = "sphere"
    radius = 0.5
    com = [1.0, 1.0, 1.0]
    #bc_type = "dirichlet"
    #temperature = 1.0
    
    refine_lvl = 3
    bc_type_V = ["dirichlet", "dirichlet", "dirichlet", "dirichlet", "dirichlet"]
    dirichlet_V = [0.0, 0.0, 0.0, 0.0, 1.0]
  }
#  {
#    mesh_path = "sphere.stl"
#    is_static = true
#    position = [13.0, 2.5, 2.5]
#    type = "meshobject"
#    bc_type = "dirichlet"
#    temperature = 0.0
#  }
)

### BCs
boundary = (
  {
    side = "x-"
    vel_type = "no_slip"
    ifRefine = true
  },
  {
    side = "x+"
    vel_type = "no_slip"
    ifRefine = true
  },
  {
    side = "y-"
    vel_type = "no_slip"
    temperature_type = "weak"
    temperature = 2
    ifRefine = true
  },
  {
    side = "y+"
    vel_type = "no_slip"
    temperature_type = "weak"
    temperature = -2
    ifRefine = true
  },
  {
    side = "z-"
    vel_type = "no_slip"
    temperature_type = "dirichlet"
    temperature = -1
    ifRefine = true
  },
  {
    side = "z+"
    vel_type = "no_slip"
    temperature_type = "dirichlet"
    temperature = -1
    ifRefine = true
  }
)

### ICs
initial_condition = {
  vel_ic_type = "user_defined"
  vel_ic = [0.0, 0.0, 0.0]
  pressure_ic_type = "user_defined"
  pressure_ic = 0.0
}

OutputStartTime = 0

#################### solver setting ####################
solver_options_ns = {
  snes_atol = 1e-8
  snes_rtol = 1e-8
  snes_stol = 1e-10
  snes_max_it = 40
  snes_max_funcs = 80000
  ksp_max_it = 2000
  ksp_rtol = 1e-12
  ksp_atol = 1e-10
  ksp_type = "bcgs"
  pc_type = "asm"
  #sub_pc_type = "lu"
#multigrid
  #ksp_type = "bcgs"
  #pc_type = "gamg"
  #pc_gamg_asm_use_agg = True
  #mg_levels_ksp_type = "bcgs"
  #mg_levels_pc_type = "asm"
  #mg_levels_sub_pc_type = "lu"
  #mg_levels_ksp_max_it = 50
#direct solver
  #ksp_type = "bcgs"
  #pc_type = "lu"
  #pc_factor_mat_solver_package = "mumps"
  #pc_factor_levels = 3
# monitor
  snes_monitor = ""
  #snes_converged_reason = ""
  #ksp_monitor = ""
  #ksp_converged_reason = ""
};
solver_options_ht = {
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_monitor = ""
  ksp_converged_reason = ""
}

