Immersogeometric Analysis on adaptive meshes
=============================================

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3879392.svg)](https://doi.org/10.5281/zenodo.3879392)


**Cloning the repository**
```bash
git clone https://bitbucket.org/baskargroup/dendrite-ibm.git
git submodule update --init
```
Dependencies
=============
#### Required Dependencies
* A C++ compiler with C++11 support is required. We aim for compatibility with gcc and icc (Intel C++ compiler).
* libconfig
* PETSc >= 3.7

Compilation
============
```bash
mkdir build;
cd build;
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

SC2020 tests
=============
* [Matrix Assembly](docs/MatrixAssembly.md)
* [Weighted partitioning](docs/WPart.md)
* [Adaptive Quadrature](docs/AdaptiveQuadrature.md)
* [In-Out](docs/in-out.md)



Contributors
============
* Boshun Gao
* Kumar Saurabh
* Milinda Fernando
* Hari Sundar
* Baskar Ganapathysubramanian

 