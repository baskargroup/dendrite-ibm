#pragma once

#include <talyfem/fem/cequation.h>
#include <talyfem/stabilizer/tezduyar_upwind.h>
#include "NSNodeData.h"
#include "MathOperation.h"
#ifdef TENSOR
#include <TensorOP/Mat.h>
#endif
class NSEquation : public TALYFEMLIB::CEquation<NSNodeData> {

 private:
  int npe_;
  DENDRITE_REAL *velx_;
  DENDRITE_REAL *vely_;
  DENDRITE_REAL *velz_;
  DENDRITE_REAL *p_;
  DENDRITE_REAL *dvelx_;
  DENDRITE_REAL *dvely_;
  DENDRITE_REAL *dvelz_;
  DENDRITE_REAL *dp_;
  DENDRITE_REAL *velxPre_;
  DENDRITE_REAL *velyPre_;
  DENDRITE_REAL *velzPre_;
  DENDRITE_REAL *tauC_;
  DENDRITE_REAL *tauM_;
  DENDRITE_REAL *quadX_;
  DENDRITE_REAL *quadY_;
  DENDRITE_REAL *quadZ_;
  DENDRITE_REAL *NS_;
  DENDRITE_REAL *conv_;

  DENDRITE_REAL scale_;

  inline void calcTauM() {
    DENDRITE_REAL Ci_f = this->idata_->Ci_f;
    DENDRITE_REAL Coe_diff = 1. / this->idata_->Re;
    DENDRITE_REAL timeStab = 4.0 / (dt_ * dt_);
    for (int i = 0; i < npe_; i++) {
      DENDRITE_REAL Ge = 4.0 / (scale_ * scale_);
      DENDRITE_REAL uGu = (Ge) * (velx_[i] * velx_[i] + vely_[i] * vely_[i] + velz_[i] * velz_[i]);
      DENDRITE_REAL GGu = 3 * Ci_f * Coe_diff * Coe_diff * Ge * Ge;
      tauM_[i] = 1. / sqrt(timeStab + uGu + GGu);
    }
  }

  inline void calcTauC() {
    DENDRITE_REAL gg = 12 / (scale_ * scale_);
    for (int i = 0; i < npe_; i++) {
      tauC_[i] = 1. / (tauM_[i] * gg);
    }
  }

 public:
  explicit NSEquation(NSInputData *idata)
      : TALYFEMLIB::CEquation<NSNodeData>(false, TALYFEMLIB::kAssembleGaussPoints) {
    idata_ = idata;
  }

  void init(int nPe) {
    npe_ = nPe;
    velx_ = new DENDRITE_REAL[nPe];
    vely_ = new DENDRITE_REAL[nPe];
    velz_ = new DENDRITE_REAL[nPe];
    p_ = new DENDRITE_REAL[nPe];

    velxPre_ = new DENDRITE_REAL[nPe];
    velyPre_ = new DENDRITE_REAL[nPe];
    velzPre_ = new DENDRITE_REAL[nPe];

    dvelx_ = new DENDRITE_REAL[nPe * m_uiDim];
    dvely_ = new DENDRITE_REAL[nPe * m_uiDim];
    dvelz_ = new DENDRITE_REAL[nPe * m_uiDim];
    dp_ = new DENDRITE_REAL[nPe * m_uiDim];
    tauC_ = new DENDRITE_REAL[nPe];
    tauM_ = new DENDRITE_REAL[nPe];

    quadX_ = new DENDRITE_REAL[nPe];
    quadY_ = new DENDRITE_REAL[nPe];
    quadZ_ = new DENDRITE_REAL[nPe];

    NS_ = new DENDRITE_REAL[nPe * m_uiDim];
    conv_ = new DENDRITE_REAL[nPe * nPe];
  }
  void destroy() {
    delete[] velx_;
    delete[] vely_;
    delete[] velz_;
    delete[] p_;
    delete[] dvelx_;
    delete[] dvely_;
    delete[] dvelz_;
    delete[] dp_;
    delete[] velxPre_;
    delete[] velyPre_;
    delete[] velzPre_;
    delete[] tauC_;
    delete[] tauM_;

    delete[] quadX_;
    delete[] quadY_;
    delete[] quadZ_;
    delete[] NS_;
    delete[] conv_;

  }

  void IntegrandsAe(TensorMat::Mat *tensorMat, DENDRITE_REAL *mat, DENDRITE_REAL *values, const DENDRITE_REAL *coords) {
    /**TODO::  remove and put in struct **/
    DENDRITE_REAL Re = this->idata_->Re;
    DENDRITE_REAL Fr = this->idata_->Fr;
    DENDRITE_REAL Coe_diff = 1 / Re;

    DENDRITE_REAL Ci_f = this->idata_->Ci_f;

    scale_ = tensorMat->getScale();

    tensorMat->calcValueFEM(values, velx_, NSNodeData::VEL_X);
    tensorMat->calcValueFEM(values, vely_, NSNodeData::VEL_Y);
    tensorMat->calcValueFEM(values, velz_, NSNodeData::VEL_Z);
    tensorMat->calcValueFEM(values, p_, NSNodeData::PRESSURE);

    tensorMat->calcValueFEM(values, velxPre_, NSNodeData::NUM_VARS + 0);
    tensorMat->calcValueFEM(values, velyPre_, NSNodeData::NUM_VARS + 1);
    tensorMat->calcValueFEM(values, velzPre_, NSNodeData::NUM_VARS + 2);

    tensorMat->calcValueDerivativeFEMX(values, &dvelx_[npe_ * 0], NSNodeData::VEL_X);
    tensorMat->calcValueDerivativeFEMY(values, &dvelx_[npe_ * 1], NSNodeData::VEL_X);
    tensorMat->calcValueDerivativeFEMZ(values, &dvelx_[npe_ * 2], NSNodeData::VEL_X);

    tensorMat->calcValueDerivativeFEMX(values, &dvely_[npe_ * 0], NSNodeData::VEL_Y);
    tensorMat->calcValueDerivativeFEMY(values, &dvely_[npe_ * 1], NSNodeData::VEL_Y);
    tensorMat->calcValueDerivativeFEMZ(values, &dvely_[npe_ * 2], NSNodeData::VEL_Y);

    tensorMat->calcValueDerivativeFEMX(values, &dvelz_[npe_ * 0], NSNodeData::VEL_Z);
    tensorMat->calcValueDerivativeFEMY(values, &dvelz_[npe_ * 1], NSNodeData::VEL_Z);
    tensorMat->calcValueDerivativeFEMZ(values, &dvelz_[npe_ * 2], NSNodeData::VEL_Z);

    tensorMat->calcValueDerivativeFEMX(values, &dp_[npe_ * 0], NSNodeData::PRESSURE);
    tensorMat->calcValueDerivativeFEMY(values, &dp_[npe_ * 1], NSNodeData::PRESSURE);
    tensorMat->calcValueDerivativeFEMZ(values, &dp_[npe_ * 2], NSNodeData::PRESSURE);
    calcTauM();
    calcTauC();



    for (int i = 0; i < npe_; i++) {
      NS_[npe_ * 0 + i] =
          (velx_[i] - velxPre_[i]) / dt_ + velx_[i] * dvelx_[npe_ * 0 + i] + vely_[i] * dvelx_[npe_ * 1 + i]
              + velz_[i] * dvelx_[npe_ * 2 + i]
              + dp_[npe_ * 0 + i];// - diffusion(i);

      NS_[npe_ * 1 + i] =
          (vely_[i] - velyPre_[i]) / dt_ + velx_[i] * dvely_[npe_ * 0 + i] + vely_[i] * dvely_[npe_ * 1 + i]
              + velz_[i] * dvely_[npe_ * 2 + i]
              + dp_[npe_ * 1 + i];

      NS_[npe_ * 2 + i] =
          (velz_[i] - velzPre_[i]) / dt_ + velx_[i] * dvelz_[npe_ * 0 + i] + vely_[i] * dvelz_[npe_ * 1 + i]
              + velz_[i] * dvelz_[npe_ * 2 + i]
              + dp_[npe_ * 2 + i];
    }



    tensorMat->computeUGradV(velx_,vely_,velz_);
    tensorMat->computeUGradW(velx_,vely_,velz_);
    /** Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
        (fe.N(a) * (fe.N(b) / dt + thetaTimeStepping * conv)) * detJxW;**/

    /** (w,u)/dt **/
    tensorMat->w_IP_v(mat, NSNodeData::VEL_X, NSNodeData::VEL_X, 1. / dt_);
    tensorMat->w_IP_v(mat, NSNodeData::VEL_Y, NSNodeData::VEL_Y, 1. / dt_);
    tensorMat->w_IP_v(mat, NSNodeData::VEL_Z, NSNodeData::VEL_Z, 1. / dt_);

    for(int i = 0; i < 3; i++) {
      tensorMat->W_IP_UdotgradV(mat,NSNodeData::VEL_X + i,NSNodeData::VEL_X + i);
    }


    /** Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
        (fe.N(a) * du(i, j) * fe.N(b) * detJxW); **/

    for(int i = 0; i < 3; i++){
      tensorMat->w_IP_v(mat, &dvelx_[npe_ * i], NSNodeData::VEL_X, NSNodeData::VEL_X + i);
      tensorMat->w_IP_v(mat, &dvely_[npe_ * i], NSNodeData::VEL_Y, NSNodeData::VEL_X + i);
      tensorMat->w_IP_v(mat, &dvelz_[npe_ * i], NSNodeData::VEL_Z, NSNodeData::VEL_X + i);
    }
    /**    Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
                (Coe_diff * fe.dN(a, j) * fe.dN(b, j) * detJxW); **/

    for(int i = 0; i < 3; i++){
      tensorMat->gradW_IP_gradV(mat, NSNodeData::VEL_X + i, NSNodeData::VEL_X +i, Coe_diff);
    }

    /**  Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) +=
                 -thetaTimeStepping * fe.dN(a, i) * fe.N(b) * detJxW;
    */


    tensorMat->gradWx_IP_V(mat, NSNodeData::VEL_X, NSNodeData::PRESSURE, -1.);
    tensorMat->gradWy_IP_V(mat, NSNodeData::VEL_Y, NSNodeData::PRESSURE, -1.);
    tensorMat->gradWz_IP_V(mat, NSNodeData::VEL_Z, NSNodeData::PRESSURE, -1.);

////--------------------------------------------------------------------------------------------//
    /**Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
        (crossTermVelocityPart * tauM * (fe.N(b) / dt + conv - diff_J) * detJxW)+
        1.0*(1.0 - thetaTimeStepping) * (crossTermVelocityPart_pre * tauM * (fe.N(b) / dt) * detJxW);**/

    tensorMat->uDotGradW_IP_V(mat,tauM_,NSNodeData::VEL_X,NSNodeData::VEL_X,1/dt_);
    tensorMat->uDotGradW_IP_V(mat,tauM_,NSNodeData::VEL_Y,NSNodeData::VEL_Y,1/dt_);
    tensorMat->uDotGradW_IP_V(mat,tauM_,NSNodeData::VEL_Z,NSNodeData::VEL_Z,1/dt_);


    tensorMat->U1DotGradW_IP_U2DotGradV(mat, nullptr,tauM_,NSNodeData::VEL_X,NSNodeData::VEL_X);
    tensorMat->U1DotGradW_IP_U2DotGradV(mat, nullptr,tauM_,NSNodeData::VEL_Y,NSNodeData::VEL_Y);
    tensorMat->U1DotGradW_IP_U2DotGradV(mat, nullptr,tauM_,NSNodeData::VEL_Z,NSNodeData::VEL_Z);

    /** Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
        (crossTermVelocityPart * tauM * du(i, j) * fe.N(b) * detJxW); **/


    for(int i = 0; i < npe_; i++){
      quadX_[i] = dvelx_[npe_*0 + i]*tauM_[i];
      quadY_[i] = dvelx_[npe_*1 + i]*tauM_[i];
      quadZ_[i] = dvelx_[npe_*2 + i]*tauM_[i];
    }
    tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_X);
    tensorMat->uDotGradW_IP_V(mat,quadY_,NSNodeData::VEL_X,NSNodeData::VEL_Y);
    tensorMat->uDotGradW_IP_V(mat,quadZ_,NSNodeData::VEL_X,NSNodeData::VEL_Z);

    for(int i = 0; i < npe_; i++){
      quadX_[i] = dvely_[npe_*0 + i]*tauM_[i];
      quadY_[i] = dvely_[npe_*1 + i]*tauM_[i];
      quadZ_[i] = dvely_[npe_*2 + i]*tauM_[i];
    }
    tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_Y,NSNodeData::VEL_X);
    tensorMat->uDotGradW_IP_V(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Y);
    tensorMat->uDotGradW_IP_V(mat,quadZ_,NSNodeData::VEL_Y,NSNodeData::VEL_Z);

    for(int i = 0; i < npe_; i++){
      quadX_[i] = dvelz_[npe_*0 + i]*tauM_[i];
      quadY_[i] = dvelz_[npe_*1 + i]*tauM_[i];
      quadZ_[i] = dvelz_[npe_*2 + i]*tauM_[i];
    }
    tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_Z,NSNodeData::VEL_X);
    tensorMat->uDotGradW_IP_V(mat,quadY_,NSNodeData::VEL_Z,NSNodeData::VEL_Y);
    tensorMat->uDotGradW_IP_V(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Z);

    /*  Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
          (tauM * fe.dN(a, j) * NS(i) * fe.N(b) * detJxW);*/

    for (int i = 0; i < npe_; i++) {
      quadX_[i] = tauM_[i] * NS_[npe_ * 0 + i];
      quadY_[i] = tauM_[i] * NS_[npe_ * 1 + i];
      quadZ_[i] = tauM_[i] * NS_[npe_ * 2 + i];
    }

    tensorMat->gradWx_IP_V(mat, quadX_, NSNodeData::VEL_X, NSNodeData::VEL_X);
    tensorMat->gradWy_IP_V(mat, quadX_, NSNodeData::VEL_X, NSNodeData::VEL_Y);
    tensorMat->gradWz_IP_V(mat, quadX_, NSNodeData::VEL_X, NSNodeData::VEL_Z);

    tensorMat->gradWx_IP_V(mat, quadY_, NSNodeData::VEL_Y, NSNodeData::VEL_X);
    tensorMat->gradWy_IP_V(mat, quadY_, NSNodeData::VEL_Y, NSNodeData::VEL_Y);
    tensorMat->gradWz_IP_V(mat, quadY_, NSNodeData::VEL_Y, NSNodeData::VEL_Z);

    tensorMat->gradWx_IP_V(mat, quadZ_, NSNodeData::VEL_Z, NSNodeData::VEL_X);
    tensorMat->gradWy_IP_V(mat, quadZ_, NSNodeData::VEL_Z, NSNodeData::VEL_Y);
    tensorMat->gradWz_IP_V(mat, quadZ_, NSNodeData::VEL_Z, NSNodeData::VEL_Z);

    /** Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
         (crossTermVelocityPart * tauM * fe.dN(b, i) * detJxW);**/
    tensorMat->uDotGradW_IP_gradWx(mat, tauM_, NSNodeData::VEL_X, NSNodeData::PRESSURE);
    tensorMat->uDotGradW_IP_gradWy(mat, tauM_, NSNodeData::VEL_Y, NSNodeData::PRESSURE);
    tensorMat->uDotGradW_IP_gradWz(mat, tauM_, NSNodeData::VEL_Z, NSNodeData::PRESSURE);


    /**for (int j = 0; j < nsd; j++) {
      /// k is the dummy index
      for (int k = 0; k < nsd; k++) {
        Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=  thetaTimeStepping *
            (-du(i, k) * fe.N(a) * tauM * fe.N(b) * du(k, j) * detJxW);
      }
    }**/

    for (int d = 0; d < m_uiDim; d++) {
      for (int i = 0; i < npe_; i++) {
        quadX_[i] =
            -tauM_[i] * (dvelx_[npe_ * 0 + i] * dvelx_[npe_ * d + i] + dvelx_[npe_ * 1 + i] * dvely_[npe_ * d + i]
                + dvelx_[npe_ * 2 + i] * dvelz_[npe_ * d + i]);
      }
      tensorMat->w_IP_v(mat, quadX_, NSNodeData::VEL_X, NSNodeData::VEL_X + d);

    }

    for (int d = 0; d < m_uiDim; d++) {
      for (int i = 0; i < npe_; i++) {
        quadX_[i] =
            -tauM_[i] * (dvely_[npe_ * 0 + i] * dvelx_[npe_ * d + i] + dvely_[npe_ * 1 + i] * dvely_[npe_ * d + i]
                + dvely_[npe_ * 2 + i] * dvelz_[npe_ * d + i]);
      }
      tensorMat->w_IP_v(mat, quadX_, NSNodeData::VEL_Y, NSNodeData::VEL_X + d);
    }

    for (int d = 0; d < m_uiDim; d++) {
      for (int i = 0; i < npe_; i++) {
        quadX_[i] =
            -tauM_[i] * (dvelz_[npe_ * 0 + i] * dvelx_[npe_ * d + i] + dvelz_[npe_ * 1 + i] * dvely_[npe_ * d + i]
                + dvelz_[npe_ * 2 + i] * dvelz_[npe_ * d + i]);
      }
      tensorMat->w_IP_v(mat, quadX_, NSNodeData::VEL_Z, NSNodeData::VEL_X + d);
    }

    /**  Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
          (-du(i, j) * fe.N(a) * tauM * (fe.N(b) / dt + conv - diff_J) *
              detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (-du_pre(i, j) * fe.N(a) * tauM * (fe.N(b) / dt) * detJxW);**/
    for (int i = 0; i < npe_; i++) {
      quadX_[i] =  - dvelx_[npe_*0 + i]*tauM_[i];
      quadY_[i] =  - dvelx_[npe_*1 + i]*tauM_[i];
      quadZ_[i] =  - dvelx_[npe_*2 + i]*tauM_[i];
    }
    tensorMat->w_IP_v(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_X,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_X);

    tensorMat->w_IP_v(mat,quadY_,NSNodeData::VEL_X,NSNodeData::VEL_Y,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadY_,NSNodeData::VEL_X,NSNodeData::VEL_Y);

    tensorMat->w_IP_v(mat,quadZ_,NSNodeData::VEL_X,NSNodeData::VEL_Z,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadZ_,NSNodeData::VEL_X,NSNodeData::VEL_Z);


    for (int i = 0; i < npe_; i++) {
      quadX_[i] = - dvely_[npe_*0 + i]*tauM_[i];
      quadY_[i] = - dvely_[npe_*1 + i]*tauM_[i];
      quadZ_[i] = - dvely_[npe_*2 + i]*tauM_[i];
    }
    tensorMat->w_IP_v(mat,quadX_,NSNodeData::VEL_Y,NSNodeData::VEL_X,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadX_,NSNodeData::VEL_Y,NSNodeData::VEL_X);

    tensorMat->w_IP_v(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Y,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Y);

    tensorMat->w_IP_v(mat,quadZ_,NSNodeData::VEL_Y,NSNodeData::VEL_Z,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadZ_,NSNodeData::VEL_Y,NSNodeData::VEL_Z);

    for (int i = 0; i < npe_; i++) {
      quadX_[i] = - dvelz_[npe_*0 + i]*tauM_[i];
      quadY_[i] = - dvelz_[npe_*1 + i]*tauM_[i];
      quadZ_[i] = - dvelz_[npe_*2 + i]*tauM_[i];
    }
    tensorMat->w_IP_v(mat,quadX_,NSNodeData::VEL_Z,NSNodeData::VEL_X,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadX_,NSNodeData::VEL_Z,NSNodeData::VEL_X);

    tensorMat->w_IP_v(mat,quadY_,NSNodeData::VEL_Z,NSNodeData::VEL_Y,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadY_,NSNodeData::VEL_Z,NSNodeData::VEL_Y);

    tensorMat->w_IP_v(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Z,1./dt_);
    tensorMat->W_IP_UdotgradV(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Z);

    /**Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
        (-fe.N(a) * tauM * NS(j) * fe.dN(b, j) * detJxW);**/
    for(int d = 0; d <3; d++){
      for (int i = 0; i < npe_; i++) {
        quadX_[i] = -NS_[npe_ * 0 + i] * tauM_[i];
        quadY_[i] = -NS_[npe_ * 1 + i] * tauM_[i];
        quadZ_[i] = -NS_[npe_ * 2 + i] * tauM_[i];
      }
      tensorMat->computeUGradV(quadX_,quadY_,quadZ_);
      tensorMat->W_IP_gradV(mat,quadX_,quadY_,quadZ_,NSNodeData::VEL_X+d,NSNodeData::VEL_X+d);
    }

    /**Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
        (-du(i, j) * fe.N(a) * tauM * fe.dN(b, j) * detJxW);**/

    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -dvelx_[npe_ * 0 + i] * tauM_[i];
      quadY_[i] = -dvelx_[npe_ * 1 + i] * tauM_[i];
      quadZ_[i] = -dvelx_[npe_ * 2 + i] * tauM_[i];
    }
    tensorMat->computeUGradV(quadX_,quadY_,quadZ_);
    tensorMat->W_IP_gradV(mat, quadX_, quadY_, quadZ_, NSNodeData::VEL_X, NSNodeData::PRESSURE);
    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -dvely_[npe_ * 0 + i] * tauM_[i];
      quadY_[i] = -dvely_[npe_ * 1 + i] * tauM_[i];
      quadZ_[i] = -dvely_[npe_ * 2 + i] * tauM_[i];
    }
    tensorMat->computeUGradV(quadX_,quadY_,quadZ_);
    tensorMat->W_IP_gradV(mat, quadX_, quadY_, quadZ_, NSNodeData::VEL_Y, NSNodeData::PRESSURE);
    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -dvelz_[npe_ * 0 + i] * tauM_[i];
      quadY_[i] = -dvelz_[npe_ * 1 + i] * tauM_[i];
      quadZ_[i] = -dvelz_[npe_ * 2 + i] * tauM_[i];
    }
    tensorMat->computeUGradV(quadX_,quadY_,quadZ_);
    tensorMat->W_IP_gradV(mat, quadX_, quadY_, quadZ_, NSNodeData::VEL_Z, NSNodeData::PRESSURE);

    /*Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += thetaTimeStepping *
        (-crossTermFineScalePart * tauM * (fe.N(b) / dt + conv - diff_J) *
            detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (-crossTermFineScalePart_pre * tauM * (fe.N(b) / dt) * detJxW);*/

    tensorMat->computeUGradW(&NS_[npe_*0],&NS_[npe_*1],&NS_[npe_*2]);
    tensorMat->computeUGradV(velx_,vely_,velz_);
    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -tauM_[i]*tauM_[i];
    }
    for(int d = 0; d < 3; d++){
      tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_X + d ,NSNodeData::VEL_X + d,1./dt_);
      tensorMat->U1DotGradW_IP_U2DotGradV(mat, nullptr,quadX_,NSNodeData::VEL_X + d,NSNodeData::VEL_X + d);
    }

    /**for (int j = 0; j < nsd; j++) {
      Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
          (-crossTermFineScalePart * tauM * du(i, j) * fe.N(b) * detJxW);
    }**/
    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -tauM_[i]*dvelx_[npe_ * 0 + i] * tauM_[i];
      quadY_[i] = -tauM_[i]*dvelx_[npe_ * 1 + i] * tauM_[i];
      quadZ_[i] = -tauM_[i]*dvelx_[npe_ * 2 + i] * tauM_[i];
    }
    tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_X  ,NSNodeData::VEL_X);
    tensorMat->uDotGradW_IP_V(mat,quadY_,NSNodeData::VEL_X  ,NSNodeData::VEL_Y);
    tensorMat->uDotGradW_IP_V(mat,quadZ_,NSNodeData::VEL_X  ,NSNodeData::VEL_Z);

    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -tauM_[i]*dvely_[npe_ * 0 + i] * tauM_[i];
      quadY_[i] = -tauM_[i]*dvely_[npe_ * 1 + i] * tauM_[i];
      quadZ_[i] = -tauM_[i]*dvely_[npe_ * 2 + i] * tauM_[i];
    }
    tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_Y  ,NSNodeData::VEL_X);
    tensorMat->uDotGradW_IP_V(mat,quadY_,NSNodeData::VEL_Y  ,NSNodeData::VEL_Y);
    tensorMat->uDotGradW_IP_V(mat,quadZ_,NSNodeData::VEL_Y  ,NSNodeData::VEL_Z);

    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -tauM_[i]*dvelz_[npe_ * 0 + i] * tauM_[i];
      quadY_[i] = -tauM_[i]*dvelz_[npe_ * 1 + i] * tauM_[i];
      quadZ_[i] = -tauM_[i]*dvelz_[npe_ * 2 + i] * tauM_[i];
    }
    tensorMat->uDotGradW_IP_V(mat,quadX_,NSNodeData::VEL_Z  ,NSNodeData::VEL_X);
    tensorMat->uDotGradW_IP_V(mat,quadY_,NSNodeData::VEL_Z  ,NSNodeData::VEL_Y);
    tensorMat->uDotGradW_IP_V(mat,quadZ_,NSNodeData::VEL_Z  ,NSNodeData::VEL_Z);

    /**Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
        (-crossTermFineScalePart * tauM * fe.dN(b, i) * detJxW);**/
    for (int i = 0; i < npe_; i++) {
      quadX_[i] = -tauM_[i]* tauM_[i];
      quadY_[i] = -tauM_[i]* tauM_[i];
      quadZ_[i] = -tauM_[i]* tauM_[i];
    }
    tensorMat->uDotGradW_IP_gradWx(mat,quadX_,NSNodeData::VEL_X,NSNodeData::PRESSURE);
    tensorMat->uDotGradW_IP_gradWy(mat,quadX_,NSNodeData::VEL_Y,NSNodeData::PRESSURE);
    tensorMat->uDotGradW_IP_gradWz(mat,quadX_,NSNodeData::VEL_Z,NSNodeData::PRESSURE);

    /**for (int j = 0; j < nsd; j++) {
      for (int k = 0; k < nsd; k++) {
        Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
            (-tauM * NS(i) * fe.dN(a, k) * tauM * fe.N(b) * du(k, j) *
                detJxW);
      }
    }**/

#pragma unroll(m_uiDim)
    for (int di = 0; di < m_uiDim; di++) {
#pragma unroll(m_uiDim)
      for (int dj = 0; dj < m_uiDim; dj++) {
        for (int i = 0; i < npe_; i++) {
          quadX_[i] = -tauM_[i] * tauM_[i] * NS_[npe_ * di + i] * dvelx_[npe_ * dj + i];
          quadY_[i] = -tauM_[i] * tauM_[i] * NS_[npe_ * di + i] * dvely_[npe_ * dj + i];
          quadZ_[i] = -tauM_[i] * tauM_[i] * NS_[npe_ * di + i] * dvelz_[npe_ * dj + i];
        }
        tensorMat->computeUGradW(quadX_,quadY_,quadZ_);
        tensorMat->uDotGradW_IP_V(mat,NSNodeData::VEL_X + di, NSNodeData::VEL_X + dj);
      }
    }

    /** for (int j = 0; j < nsd; j++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
                (-tauM * NS(i) * fe.dN(a, j) * tauM *
                    (fe.N(b) / dt + conv - diff_J) * detJxW)+ 1.0*(1.0 - thetaTimeStepping) * (-tauM * NS_pre(i) * fe.dN(a, j) * tauM * (fe.N(b) / dt) * detJxW);
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += thetaTimeStepping *
                (-tauM * NS(i) * fe.dN(a, j) * tauM * fe.dN(b, j) * detJxW);
          }**/
    for(int i = 0; i < npe_; i++){
      quadX_[i] = -tauM_[i]*tauM_[i]*NS_[npe_*0 + i];
      quadY_[i] = -tauM_[i]*tauM_[i]*NS_[npe_*1 + i];
      quadZ_[i] = -tauM_[i]*tauM_[i]*NS_[npe_*2 + i];
    }
    tensorMat->gradWx_IP_V(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_X,1/dt_);
    tensorMat->gradWx_IP_UDotgradV(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_X);

    tensorMat->gradWy_IP_V(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_Y,1/dt_);
    tensorMat->gradWy_IP_UDotgradV(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_Y);


    tensorMat->gradWz_IP_V(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_Z,1/dt_);
    tensorMat->gradWz_IP_UDotgradV(mat,quadX_,NSNodeData::VEL_X,NSNodeData::VEL_Z);


    tensorMat->gradWx_IP_V(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_X,1/dt_);
    tensorMat->gradWx_IP_UDotgradV(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_X);

    tensorMat->gradWy_IP_V(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Y,1/dt_);
    tensorMat->gradWy_IP_UDotgradV(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Y);

    tensorMat->gradWz_IP_V(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Z,1/dt_);
    tensorMat->gradWz_IP_UDotgradV(mat,quadY_,NSNodeData::VEL_Y,NSNodeData::VEL_Z);


    tensorMat->gradWx_IP_V(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_X,1/dt_);
    tensorMat->gradWx_IP_UDotgradV(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_X);

    tensorMat->gradWy_IP_V(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Y,1/dt_);
    tensorMat->gradWy_IP_UDotgradV(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Y);

    tensorMat->gradWz_IP_V(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Z,1/dt_);
    tensorMat->gradWz_IP_UDotgradV(mat,quadZ_,NSNodeData::VEL_Z,NSNodeData::VEL_Z);

    tensorMat->gradW_IP_gradV(mat,quadX_,quadX_,quadX_,NSNodeData::VEL_X,NSNodeData::PRESSURE);
    tensorMat->gradW_IP_gradV(mat,quadY_,quadY_,quadY_,NSNodeData::VEL_Y,NSNodeData::PRESSURE);
    tensorMat->gradW_IP_gradV(mat,quadZ_,quadZ_,quadZ_,NSNodeData::VEL_Z,NSNodeData::PRESSURE);

    /**for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += thetaTimeStepping *
            (fe.dN(a, i) * tauC * fe.dN(b, j) * detJxW);
      }
    }**/
    tensorMat->gradWx_IP_gradVx(mat, tauC_, NSNodeData::VEL_X, NSNodeData::VEL_X);
    tensorMat->gradWx_IP_gradVy(mat, tauC_, NSNodeData::VEL_X, NSNodeData::VEL_Y);
    tensorMat->gradWx_IP_gradVz(mat, tauC_, NSNodeData::VEL_X, NSNodeData::VEL_Z);

    tensorMat->gradWy_IP_gradVx(mat, tauC_, NSNodeData::VEL_Y, NSNodeData::VEL_X);
    tensorMat->gradWy_IP_gradVy(mat, tauC_, NSNodeData::VEL_Y, NSNodeData::VEL_Y);
    tensorMat->gradWy_IP_gradVz(mat, tauC_, NSNodeData::VEL_Y, NSNodeData::VEL_Z);

    tensorMat->gradWz_IP_gradVx(mat, tauC_, NSNodeData::VEL_Z, NSNodeData::VEL_X);
    tensorMat->gradWz_IP_gradVy(mat, tauC_, NSNodeData::VEL_Z, NSNodeData::VEL_Y);
    tensorMat->gradWz_IP_gradVz(mat, tauC_, NSNodeData::VEL_Z, NSNodeData::VEL_Z);

    /**Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
        fe.N(a) * fe.dN(b, i) * detJxW +
            fe.dN(a, i) * tauM * (fe.N(b) / dt + conv - diff_J) * detJxW;**/
    tensorMat->computeUGradV(velx_,vely_,velz_);
    tensorMat->W_IP_gradVx(mat,NSNodeData::PRESSURE,NSNodeData::VEL_X);
    tensorMat->W_IP_gradVy(mat,NSNodeData::PRESSURE,NSNodeData::VEL_Y);
    tensorMat->W_IP_gradVz(mat,NSNodeData::PRESSURE,NSNodeData::VEL_Z);

    tensorMat->gradWx_IP_V(mat,tauM_,NSNodeData::PRESSURE,NSNodeData::VEL_X,1/dt_);
    tensorMat->gradWy_IP_V(mat,tauM_,NSNodeData::PRESSURE,NSNodeData::VEL_Y,1/dt_);
    tensorMat->gradWz_IP_V(mat,tauM_,NSNodeData::PRESSURE,NSNodeData::VEL_Z,1/dt_);

    tensorMat->gradWx_IP_UDotgradV(mat,tauM_,NSNodeData::PRESSURE,NSNodeData::VEL_X);
    tensorMat->gradWy_IP_UDotgradV(mat,tauM_,NSNodeData::PRESSURE,NSNodeData::VEL_Y);
    tensorMat->gradWz_IP_UDotgradV(mat,tauM_,NSNodeData::PRESSURE,NSNodeData::VEL_Z);

    /** for (int j = 0; j < nsd; j++) {
       Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
           fe.dN(a, j) * tauM * du(j, i) * fe.N(b) * detJxW;
     }**/
    for (int d = 0; d < m_uiDim; d++) {
      for (int i = 0; i < npe_; i++) {
        quadX_[i] = tauM_[i] * dvelx_[npe_ * d + i];
        quadY_[i] = tauM_[i] * dvely_[npe_ * d + i];
        quadZ_[i] = tauM_[i] * dvelz_[npe_ * d + i];
      }
      tensorMat->gradW_IP_V(mat, quadX_, quadY_, quadZ_, NSNodeData::PRESSURE, NSNodeData::VEL_X + d);
    }

/**    Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) +=
       fe.dN(a, i) * tauM * fe.dN(b, i) * detJxW;**/
    tensorMat->gradW_IP_gradV(mat,tauM_,tauM_,tauM_,NSNodeData::PRESSURE,NSNodeData::PRESSURE);



  }

  void Solve(double dt, double t) override {
    assert(false);
  }

  void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
                  TALYFEMLIB::ZEROARRAY<double> &be) override {
    assert(false);
  }

  void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
    using namespace TALYFEMLIB;

    double Re = this->idata_->Re;
    double Fr = this->idata_->Fr;
    double Coe_diff = 1 / Re;
    double Ci_f = this->idata_->Ci_f;

    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double detJxW = fe.detJxW();



    /** Get u from the NodeData
     * NOTE: ValueFEM is defined on NodeData object, therefore it follows
     * indices defined in NodeData subclass.
     * NOTE: Every iteration the solution is copied into NodeData, we use these
     * solved fields from the NodeData object to calculate the be (NS residual)
     * of current iteration.
     */
    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, NSNodeData::VEL_X + i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSNodeData::NUM_VARS + i);
    }
    /// Define velocity gradient tensor ( grad{u} )
    ZeroMatrix<double> du;
    du.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
      }
    }

    /** Calculate the laplacian of velocity
     * This is required for course scale residual of Navier-Stokes for
     * diffusion term
     */
    ZEROPTV d2u;
    /// loop for three directions of velocity (MAX NSD is no of velocity
    /// directions in the problem)
    /// PS: This assumes the first three degrees are always velocity, it is
    /// wise to keep it that way
    for (int dof = 0; dof < nsd; dof++) {
      /// Summing over three directions of velocity as it is laplacian
      for (int dir = 0; dir < nsd; dir++) {
        d2u(dof) += this->p_data_->value2DerivativeFEM(fe, dof, dir, dir);
      }
    }

    ZEROPTV dp;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSNodeData::PRESSURE, i);
    }

    if (idata_->solverTypeNS == NSInputData::STABILIZED_NS) {
      /// The forcing term goes into the RHS
      ZEROPTV U = {1.0, 0.0, 0.0};
      TALYFEMLIB::TezduyarUpwindFE PG;
      PG.calcSUPGWeightingFunction(fe, u, Coe_diff);
      PG.calcPSPGWeightingFunction(fe, U, 1.0, Coe_diff);

      for (int a = 0; a < nbf; a++) {
        for (int b = 0; b < nbf; b++) {
          /// NS terms
          double conv = 0.0;
          for (int i = 0; i < nsd; i++) {
            conv += fe.dN(b, i) * u(i);
          }

          /// normal
          for (int i = 0; i < nsd; i++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += fe.N(a) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                  fe.N(a) * du(i, j) * fe.N(b) * detJxW + Coe_diff * fe.dN(a, j) * fe.dN(b, i) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += Coe_diff * fe.dN(a, j) * fe.dN(b, j) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -fe.dN(a, i) * fe.N(b) * detJxW;
          }

          /// SUPG
          for (int i = 0; i < nsd; i++) {

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += PG.SUPG(a) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += PG.SUPG(a) * du(i, j) * fe.N(b) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += PG.SUPG(a) * fe.dN(b, i) * detJxW;
          }

          /// PSPG
          for (int i = 0; i < nsd; i++) {
            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
                fe.N(a) * fe.dN(b, i) * detJxW + PG.PSPG(a, i) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++)
              Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) += PG.PSPG(a, j) * du(j, i) * fe.N(b) * detJxW;

            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) += PG.PSPG(a, i) * fe.dN(b, i) * detJxW;
          }
        }
      }
    } else if (idata_->solverTypeNS == NSInputData::RBVMS_NS) {
      const double Ci_f = idata_->Ci_f;




      /// Get the cofactor matrix from FEELm class
      ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      /// G_{ij} in equation 65 in Bazilevs et al. (2007)
      ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++) {
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
          }
        }
      }

      /// Equation 67 in Bazilevs et al. (2007)
      double u_Gu = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          u_Gu += u(i) * Ge(i, j) * u(j);
        }
      }

      /// Last term in Equation 63 in Bazilevs et al. (2007)
      double G_G_u = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
        }
      }

      /// Calculate the tau_M based on the projections calculated, Equation 63 in
      /// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
      /// in the paper it is set to 1.
      double tauM = idata_->tauM_scale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
      /// Equation 68 in Bazilevs et al. (2007)
      ZEROARRAY<double> ge;
      ge.redim(nsd);
      for (int i = 0; i < nsd; i++) {
        ge(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          ge(i) += ksiX(j, i);
        }
      }

      /// Equation 69 Bazilevs et al. (2007)
      double g_g = 0.0;
      for (int i = 0; i < nsd; i++) {
        g_g += ge(i) * ge(i);
      }

      /// Calculate continuity residual based on
      double tauC = 1.0 / (tauM * g_g);

      /// NS terms
      ZEROPTV convec;
      ZEROPTV diffusion;

      for (int i = 0; i < nsd; i++) {
        convec(i) = 0.0;
        diffusion(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          convec(i) += du(i, j) * u(j);
        }
        diffusion(i) += 0*Coe_diff * d2u(i);
      }

      /// Contribution of gravity in terms of Froude number
      ZEROPTV gravity;
      gravity.x() = 0.0;
      /// if Fr is specified to be zero in the config file turn gravity off
      if (Fr == 0.0) {
        gravity.y() = 0;
      } else {
        gravity.y() = -(1.0 / Fr);
      }
      gravity.z() = 0.0;

      ZEROPTV NS;
      for (int i = 0; i < nsd; i++) {
        NS(i) = (u(i) - u_pre(i)) / dt_ + convec(i) + dp(i) - diffusion(i);// - gravity(i);
      }

      double cont = 0.0;
      for (int i = 0; i < nsd; i++) {
        cont += du(i, i);
      }

      ///---------------Calculating the Jacobian operator--------------------///
      /// NS terms

      /** Course scale terms for the cross terms (terms 4 and 5 in equation 52
       * in Bazilevs et al. (2007)
       * PS: these are not calculated using the SUPG class, and this is an
       * approximation of (u, grad{u})
       */
      for (int a = 0; a < nbf; a++) {

        double crossTermVelocityPart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermVelocityPart += fe.dN(a, i) * u(i);
        }

        /// Actual fine scale contribution to cross terms from tauM(inverse
        /// estimate)
        double crossTermFineScalePart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermFineScalePart += fe.dN(a, i) * tauM * NS(i);
        }

        for (int b = 0; b < nbf; b++) {
          /// Convection term
          double conv = 0;
          for (int i = 0; i < nsd; i++) {
            conv += fe.dN(b, i) * u(i);
          }

          /// Adding terms to the Jacobian matrix.
          for (int i = 0; i < nsd; i++) {

            /// Transient terms and the convection term and part of the stress
            /// tensor in the diagonals

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += fe.N(a) * (fe.N(b) / dt_ + conv) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += fe.N(a) * du(i, j) * fe.N(b) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += Coe_diff * fe.dN(a, j) * fe.dN(b, j) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -fe.dN(a, i) * fe.N(b) * detJxW;
          }

          /** crossTerm 1: This term is written as, (w, grad{u u'}), which is
         * weakened to (grad{w}, u\innerproduct u') which is then linearised.
         * Here u' is fine scale velocity
         * and u is resolved velocity. The fine scale velocity is approximated
         * as -tau*Res{NS} (Equation 58 bazilevs et al. (2007)),
         *
         */



          /// diagonal
          for (int i = 0; i < nsd; i++) {

            /// Contribution of laplacian of velocity(diffusion) to the diagonal
            double diff_J = 0;
//            for (int j = 0; j < nsd; j++) {
//              diff_J += Coe_diff * fe.d2N(b, j, j);
//            }

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
                crossTermVelocityPart * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += crossTermVelocityPart * tauM * du(i, j) * fe.N(b) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += tauM * fe.dN(a, j) * NS(i) * fe.N(b) * detJxW;
            }
            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += crossTermVelocityPart * tauM * fe.dN(b, i) * detJxW;
          }

          /* Crossterm2: This term can be mathematically written as,
         * (w, u'.grad{u}). In this term we do not further weaken it as done in
         * cross term 1*/

          for (int i = 0; i < nsd; i++) {

            /// Contribution of laplacian of velocity(diffusion) to the diagonal
            double diff_J = 0;
//            for (int j = 0; j < nsd; j++) {
//              diff_J += Coe_diff * fe.d2N(b, j, j);
//            }

            for (int j = 0; j < nsd; j++) {

              for (int k = 0; k < nsd; k++) {
                Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += -du(i, k) * fe.N(a) * tauM * fe.N(b) * du(k, j) * detJxW;
              }
            }

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                  -du(i, j) * fe.N(a) * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += -fe.N(a) * tauM * NS(j) * fe.dN(b, j) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -du(i, j) * fe.N(a) * tauM * fe.dN(b, j) * detJxW;
            }
          }

          /** The Reynolds Stress term: (w, u'.grad{u'}), we substitute u' as
         * -tau*Res(NS) and expand.
         */
          for (int i = 0; i < nsd; i++) {
            /* Three terms arising from (w,\delta(u').grad{u'})
             * u' has to be expanded to -tau*res{NS}: when
             * the linearisation acts
             * on the res{NS} it gives three terms. First
             * term which goes in the
             * diagonals of the matrix is
             * (-grad{w}* tauM*res(NS),
             * tauM*(d_t{\delta{u}} + conv -diff_J))
             * PS: grad{w}*tau*res(NS) is
             * crossTermFineScalePart*/


//
//            /// Contribution of laplacian of velocity(diffusion) to the diagonal
            double diff_J = 0;
//            for (int j = 0; j < nsd; j++) {
//              diff_J += Coe_diff * fe.d2N(b, j, j);
//            }

            Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
                -crossTermFineScalePart * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += -crossTermFineScalePart * tauM * du(i, j) * fe.N(b) * detJxW;
            }

            Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -crossTermFineScalePart * tauM * fe.dN(b, i) * detJxW;

            for (int j = 0; j < nsd; j++) {
              for (int k = 0; k < nsd; k++) {
                Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                    -tauM * NS(i) * fe.dN(a, k) * tauM * fe.N(b) * du(k, j) * detJxW;
              }
            }

//
            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                  -tauM * NS(i) * fe.dN(a, j) * tauM * (fe.N(b) / dt_ + conv - diff_J) * detJxW;
              Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) += -tauM * NS(i) * fe.dN(a, j) * tauM * fe.dN(b, j) * detJxW;
            }
          }
//
          /* This term represents the fine scale pressure given by
         * (grad{w}, \delta{tauC*res{Cont}}), where Cont is the continuity
         * equation. See third term in equation (71) in Bazilev et al. (2007)*/

          for (int i = 0; i < nsd; i++) {
            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + i, (nsd + 1) * b + j) += fe.dN(a, i) * tauC * fe.dN(b, j) * detJxW;
            }
          }

          /** pspg term from VMS theory: See second term in equation (71)
         * from Bazilevs et al. (2007). This is the contribution of momentum
         * residual to the continuity stabilization.
         */
          for (int i = 0; i < nsd; i++) {
            double diff_J = 0.0;
//            for (int j = 0; j < nsd; j++) {
//              diff_J += Coe_diff * fe.d2N(b, j, j);
//            }

            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
                (fe.N(a) * fe.dN(b, i) * detJxW) + fe.dN(a, i) * tauM * ((fe.N(b) / dt_) + conv - diff_J) * detJxW;

            for (int j = 0; j < nsd; j++) {
              Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) += fe.dN(a, j) * tauM * du(j, i) * fe.N(b) * detJxW;
            }
            Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) += fe.dN(a, i) * tauM * fe.dN(b, i) * detJxW;
          }

        }
      }
    }

#ifndef NDEBUG
    if (idata_->Debug_Integrand) {
      int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
      FILE *fp = fopen("Ae_dendro5.dat", "w");
      for (int i = 0; i < 8; ++i) {
        for (int dof1 = 0; dof1 < 4; ++dof1) {
          for (int j = 0; j < 8; ++j) {
            for (int dof2 = 0; dof2 < 4; ++dof2) {
              fprintf(fp, "%.10f\t", Ae(interchangedmat[i] * 4 + dof1, interchangedmat[j] * 4 + dof2));
            }
          }
          fprintf(fp, "\n");
        }
      }
      fclose(fp);
    }
#endif
  }

  void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {

    double Re = this->idata_->Re;
    double Fr = this->idata_->Fr;
    double Coe_diff = 1 / Re;
    double Ci_f = this->idata_->Ci_f;

    const int nsd = fe.nsd();
    const int nbf = fe.nbf();
    const double detJxW = fe.detJxW();

    /** Get u and u_pre from the NodeData
   * NOTE: ValueFEM is defined on NodeData object, therefore it follows
   * indices defined in NodeData subclass.
   * NOTE: Every iteration the solution is copied into NodeData, we use these
   * solved fields from the NodeData object to calculate the be (NS residual)
   * of current iteration.
   */
    ZEROPTV u, u_pre;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, NSNodeData::VEL_X + i);
      u_pre(i) = this->p_data_->valueFEM(fe, NSNodeData::NUM_VARS + i);
    }

    /// Define velocity gradient tensor ( grad{u} )
    TALYFEMLIB::ZeroMatrix<double> du;
    du.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
      }
    }

    /** Calculate the laplacian of velocity
    * This is required for course scale residual of Navier-Stokes for
    * diffusion term
    */
    ZEROPTV d2u;
    /// loop for three directions of velocity (MAX NSD is no of velocity
    /// directions in the problem)
    /// PS: This assumes the first three degrees are always velocity, it is
    /// wise to keep it that way
    for (int dof = 0; dof < nsd; dof++) {
      /// Summing over three directions of velocity as it is laplacian
      for (int dir = 0; dir < nsd; dir++) {
        d2u(dof) += this->p_data_->value2DerivativeFEM(fe, dof, dir, dir);
      }
    }

    double p = this->p_data_->valueFEM(fe, NSNodeData::PRESSURE);

    ZEROPTV dp;
    for (int i = 0; i < nsd; i++) {
      dp(i) = this->p_data_->valueDerivativeFEM(fe, NSNodeData::PRESSURE, i);
    }

    /// NS terms
    ZEROPTV convec;
    ZEROPTV diffusion;

    for (int i = 0; i < nsd; i++) {
      convec(i) = 0.0;
      diffusion(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        convec(i) += du(i, j) * u(j);
      }
      diffusion(i) += Coe_diff * d2u(i);
    }

    /// Contribution of gravity in terms of Froude number
    ZEROPTV gravity;
    gravity.x() = 0.0;
    /// if Fr is specified to be zero in the config file turn gravity off
    if (Fr == 0.0) {
      gravity.y() = 0;
    } else {
      gravity.y() = -(1.0 / Fr);
    }
    gravity.z() = 0.0;

    /// Residual constructed with diffusion
    /// NS = Coe_conv(du/dt + u . grad{u}) + grad{p}
    ZEROPTV NS;
    for (int i = 0; i < nsd; i++) {
      NS(i) = (u(i) - u_pre(i)) / dt_ + convec(i) + dp(i) - diffusion(i) - gravity(i);
    }

    /** Calculate continuity residual for PSPG stabilizer
     * This residual is a essential for calculating the PSPG stabilizer
     */
    double cont = 0.0;
    for (int i = 0; i < nsd; i++) {
      cont += du(i, i);
    }

    if (idata_->solverTypeNS == NSInputData::STABILIZED_NS) {
      /// set PSPG and SUPG
      ZEROPTV U = {1.0, 0.0, 0.0};

      TALYFEMLIB::TezduyarUpwindFE PG;
      PG.calcSUPGWeightingFunction(fe, u, Coe_diff);
      PG.calcPSPGWeightingFunction(fe, U, 1.0, Coe_diff);

      for (int a = 0; a < nbf; a++) {
        ZEROPTV NS_stable_terms, normal_NS, NS_contrib;
        /** Calculating diffusion term of Navier-Stokes at current iteration.
         * This is done using the stress form of the diffusion term given by
         * (Coe_diff* div{(grad{u_n}+T{grad{u_n}})}), here T{} is the transpose
         * operator.
         */
        ZEROPTV diff;
        for (int i = 0; i < nsd; i++) {
          diff(i) = 0.0;
          for (int j = 0; j < nsd; j++) {
            diff(i) += Coe_diff * fe.dN(a, j) * (du(i, j) + du(j, i));
          }
        }

        for (int i = 0; i < nsd; i++) {
          normal_NS(i) =
              fe.N(a) * (u(i) - u_pre(i)) / dt_ * detJxW
                  + fe.N(a) * (convec(i)) * detJxW
                  + (-fe.dN(a, i) * p) * detJxW
                  + (diff(i)) * detJxW;

          normal_NS(i) += -fe.N(a) * gravity(i) * detJxW;

          NS_stable_terms(i) = (PG.SUPG(a) * NS(i)) * detJxW;
          NS_contrib(i) = normal_NS(i) + NS_stable_terms(i);
        }

        /** Now we have to add the contribution of pressure to the be vector.
         * This naturally will be the last term of the vector
         */

        ///< We first calculate the PSPG stabilisation to the pressure term, which
        ///< still takes the NS(i) residual
        double Cont_stable_term = 0.0;
        for (int i = 0; i < nsd; i++) {
          Cont_stable_term += PG.PSPG(a, i) * NS(i);
        }

        double Cont_contrib = fe.N(a) * (cont) * detJxW + Cont_stable_term * detJxW;

        for (int i = 0; i < nsd; i++) {
          be((nsd + 1) * a + i) += NS_contrib(i);
        }
        be((nsd + 1) * a + nsd) += Cont_contrib;
      }

    } else if (idata_->solverTypeNS == NSInputData::RBVMS_NS) {
      const double Ci_f = idata_->Ci_f;
      /// Get the cofactor matrix from FEELm class
      TALYFEMLIB::ZeroMatrix<double> ksiX;
      ksiX.redim(nsd, nsd);

      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          ksiX(i, j) = fe.cof(j, i) / fe.jacc();
        }
      }

      /// G_{ij} in equation 65 in Bazilevs et al. (2007)
      TALYFEMLIB::ZeroMatrix<double> Ge;
      Ge.redim(nsd, nsd);
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          Ge(i, j) = 0.0;
          for (int k = 0; k < nsd; k++) {
            Ge(i, j) += ksiX(k, i) * ksiX(k, j);
          }
        }
      }

      /// Equation 67 in Bazilevs et al. (2007)
      double u_Gu = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          u_Gu += u(i) * Ge(i, j) * u(j);
        }
      }

      /// Last term in Equation 63 in Bazilevs et al. (2007)
      double G_G_u = 0.0;
      for (int i = 0; i < nsd; i++) {
        for (int j = 0; j < nsd; j++) {
          G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
        }
      }

      /// Calculate the tau_M based on the projections calculated, Equation 63 in
      /// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
      /// in the paper it is set to 1.
      double tauM = idata_->tauM_scale / sqrt(4.0 / (dt_ * dt_) + u_Gu + G_G_u);
      /// Equation 68 in Bazilevs et al. (2007)
      TALYFEMLIB::ZEROARRAY<double> ge;
      ge.redim(nsd);
      for (int i = 0; i < nsd; i++) {
        ge(i) = 0.0;
        for (int j = 0; j < nsd; j++) {
          ge(i) += ksiX(j, i);
        }
      }

      /// Equation 69 Bazilevs et al. (2007)
      double g_g = 0.0;
      for (int i = 0; i < nsd; i++) {
        g_g += ge(i) * ge(i);
      }

      /// Calculate continuity residual based on
      double tauC = 1.0 / (tauM * g_g);

      for (int a = 0; a < nbf; a++) {

        double crossTermVelocityPart = 0.0;
        for (int i = 0; i < nsd; i++) {
          crossTermVelocityPart += fe.dN(a, i) * u(i);
        }

        ZEROPTV cross1, cross2, reystress, pressurep, normal_NS, NScontrib;

        ZEROPTV diff;
        for (int i = 0; i < nsd; i++) {
          diff(i) = 0.0;
          for (int j = 0; j < nsd; j++) {
            diff(i) += Coe_diff * fe.dN(a, j) * (du(i, j));
          }
        }

        for (int i = 0; i < nsd; i++) {
          normal_NS(i) =
              fe.N(a) * (u(i) - u_pre(i)) / dt_ * detJxW + fe.N(a) * convec(i) * detJxW + (-fe.dN(a, i) * p) * detJxW
                  + (diff(i)) * detJxW;


          /// Gravity body force direction
          normal_NS(i) += -fe.N(a) * gravity(i) * detJxW;

          cross1(i) = (tauM * crossTermVelocityPart * NS(i)) * detJxW;
          cross2(i) = 0.0;
          reystress(i) = 0.0;
          for (int j = 0; j < nsd; j++) {
            /// Second cross term
            cross2(i) += fe.N(a) * (du(i, j) * tauM * NS(j)) * detJxW;
            /// The Reynolds stress term
            reystress(i) += fe.dN(a, j) * (tauM * NS(i) * tauM * NS(j)) * detJxW;
          }
          /// Contribution of pressure
          pressurep(i) = fe.dN(a, i) * (tauC * cont) * detJxW;

          /// Add all the terms to the vector
          NScontrib(i) = normal_NS(i) + cross1(i) - cross2(i) - reystress(i) + pressurep(i);
        }

        double pspg = 0.0;
        for (int i = 0; i < nsd; i++) {
          /// Fine scale contribution to pressure
          pspg += fe.dN(a, i) * tauM * NS(i);
        }

        /// Contribution of this term goes to last element of the vector
        double contContrib = (fe.N(a) * (cont) * detJxW) + (pspg * detJxW);

        /// Adding all the calculated terms to the vector
        for (int i = 0; i < nsd; i++) {
          be((nsd + 1) * a + i) += NScontrib(i);
        }
        be((nsd + 1) * a + nsd) += contContrib;
      }
    }
#ifndef NDEBUG
    if (idata_->Debug_Integrand) {
      int interchangedmat[8]{0, 1, 3, 2, 4, 5, 7, 6};
      FILE *fp = fopen("be_dendro5.dat", "w");
      for (int i = 0; i < 8; ++i) {
        for (int dof1 = 0; dof1 < 4; ++dof1) {
          fprintf(fp, "%.10f\t", be(interchangedmat[i] * 4 + dof1));
        }
      }
      fprintf(fp, "\n");
      fclose(fp);
    }
#endif
  }

  void Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZeroMatrix<double> &Ae) override {

  }

  void Integrands4side_be(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZEROARRAY<double> &be) override {
  }

 private:
  NSInputData *idata_;
};

