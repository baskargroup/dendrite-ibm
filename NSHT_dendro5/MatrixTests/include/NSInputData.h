#pragma once

#include <iomanip>
#include <talyfem/input_data/input_data.h>
#include "NSNodeData.h"

using TALYFEMLIB::ZEROPTV;
using TALYFEMLIB::PrintStatusStream;
using TALYFEMLIB::PrintWarning;
using TALYFEMLIB::PrintInfo;
using TALYFEMLIB::PrintStatus;
using TALYFEMLIB::PrintError;

/**
 * Parameters for background mesh
 */
struct MeshDef {
  ZEROPTV channel_min;  ///< start of domain
  ZEROPTV channel_max;  ///< end of domain

  int refine_l = 0;
  int refine_h = 0;
  ///< whether or not fx_refine refines at channel_min/channel_max
  bool refine_walls = false;

  ///< scale for main domain, always the same on all axes to prevent skewed elements
  double daScalingFactor[3] = {0.0, 0.0, 0.0};

  /**
   * read channel mesh from config
   * @param root
   */
  void read_from_config(const libconfig::Setting &root) {
    refine_l = (int) root["refine_l"];
    refine_h = (int) root["refine_h"];
    refine_walls = (bool) root["refine_walls"];

    PrintStatus("refine_l: ", refine_l);
    PrintStatus("refine_h: ", refine_h);

    channel_min = ZEROPTV((double) root["min"][0], (double) root["min"][1], (double) root["min"][2]);
    channel_max = ZEROPTV((double) root["max"][0], (double) root["max"][1], (double) root["max"][2]);

    daScalingFactor[0] = channel_max[0];
    daScalingFactor[1] = channel_max[1];
    daScalingFactor[2] = channel_max[2];

    PrintStatus("daScalingFactor: ", daScalingFactor[0], ", ", daScalingFactor[1], ", ", daScalingFactor[2]);

  }
};

/**
 * Parameters for boundary condition
 */
struct BoundaryDef {
  enum Side {
    INVALID = -1,
    X_MINUS = 0,
    X_PLUS = 1,
    Y_MINUS = 2,
    Y_PLUS = 3,
    Z_MINUS = 4,
    Z_PLUS = 5,
  };
  enum Vel_Type {
    NO_SLIP = 0,
    NO_GRADIENT_VEL = 1,
    DIRICHLET_VEL = 2,
  };
  enum Pressure_Type {
    NO_GRADIENT_P = 0,
    DIRICHLET_P = 1,
  };

  Side side = INVALID;

  Vel_Type vel_type = NO_GRADIENT_VEL;

  Pressure_Type pressure_type = NO_GRADIENT_P;

  std::vector<ZEROPTV> wall_velocity;

  double pressure = 0.0;

  /// For slowly ramping up boundary conditions
  std::vector<double> ramping;

  bool ifRefine = false;


  Side read_side_from_config(const libconfig::Setting &root) {
    return str_to_side(root["side"]);
  }

  void read_from_config(const libconfig::Setting &root) {
    if (root.exists("ifRefine")) {
      ifRefine = (bool) root["ifRefine"];
    }
    if (root.exists("vel_type")) {
      vel_type = read_vel_type(root["vel_type"]);
    }
    if (root.exists("pressure_type")) {
      pressure_type = read_pressure_type(root["pressure_type"]);
    }

    if (root.exists("ramping_vec")) {
      const libconfig::Setting &ramping_time_vec = root["ramping_vec"];
      for (int i = 0; i < ramping_time_vec.getLength(); ++i) {
        ramping.push_back(ramping_time_vec[i]);
      }
    } else {
      ramping.push_back(0.0);
    }

    if (vel_type == Vel_Type::DIRICHLET_VEL) {
      const libconfig::Setting &wall_vel_vec = root["wall_vel"];
      for (int i = 0; i < wall_vel_vec.getLength() / 3; ++i) {
        ZEROPTV temp_vel = ZEROPTV((double) root["wall_vel"][3 * i],
                                   (double) root["wall_vel"][3 * i + 1],
                                   (double) root["wall_vel"][3 * i + 2]);
        wall_velocity.push_back(temp_vel);
      }
      if (wall_velocity.size() != ramping.size()) {
        throw TALYFEMLIB::TALYException() << "Wall_vel vector not matching with ramping";
      }
    }

    if (pressure_type == Pressure_Type::DIRICHLET_P) {
      pressure = (double) root["pressure"];
    }

  }

  template<class T>
  T getRamping(const double time, const std::vector<T> &v) const {
    unsigned int interval = ramping.size();
    for (unsigned int i = 0; i < ramping.size(); i++) {
      if (time < ramping[i]) {
        interval = i;
        break;
      }
    }
    T current_variable;
    if (interval == 0) {
      current_variable = v[0] * (time / ramping[0]);
    } else if (interval == ramping.size()) {
      current_variable = v[interval - 1];
    } else {
      double per1 = (time - ramping[interval - 1]) / (ramping[interval] - ramping[interval - 1]);
      double per2 = (ramping[interval] - time) / (ramping[interval] - ramping[interval - 1]);
      current_variable = v[interval] * per1 + v[interval - 1] * per2;
    }
    return current_variable;
  }

 protected:
  Side str_to_side(const std::string &str) const {
    if (str == "x-") {
      return Side::X_MINUS;
    } else if (str == "x+") {
      return Side::X_PLUS;
    } else if (str == "y-") {
      return Side::Y_MINUS;
    } else if (str == "y+") {
      return Side::Y_PLUS;
    } else if (str == "z-") {
      return Side::Z_MINUS;
    } else if (str == "z+") {
      return Side::Z_PLUS;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid BC side";
  }

  Vel_Type read_vel_type(const std::string &str) const {
    if (str == "no_slip") {
      return Vel_Type::NO_SLIP;
    } else if (str == "no_gradient") {
      return Vel_Type::NO_GRADIENT_VEL;
    } else if (str == "dirichlet") {
      return Vel_Type::DIRICHLET_VEL;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid BC type for vel";
  }

  Pressure_Type read_pressure_type(const std::string &str) const {
    if (str == "no_gradient") {
      return Pressure_Type::NO_GRADIENT_P;
    } else if (str == "dirichlet") {
      return Pressure_Type::DIRICHLET_P;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid BC type for pressure";
    }
  }
};

/**
 * Parameters for initial condition
 */
struct InitialConditionDef {
  enum Vel_IC {
    ZERO_VEL = 0,
    USER_DEFINED_VEL = 1,
  };
  enum Pressure_IC {
    ZERO_P = 0,
    USER_DEFINED_P = 1,
  };

  Vel_IC vel_ic_type = ZERO_VEL;
  Pressure_IC pressure_ic_type = ZERO_P;
  ZEROPTV vel_ic;
  double p_ic = 0.0;

  void read_from_config(const libconfig::Setting &root) {
    if (root.exists("vel_ic_type")) {
      vel_ic_type = read_vel_ic(root["vel_ic_type"]);
    }
    if (root.exists("p_ic_type")) {
      pressure_ic_type = read_pressure_ic(root["p_ic_type"]);
    }

    if (vel_ic_type == Vel_IC::USER_DEFINED_VEL) {
      vel_ic = ZEROPTV((double) root["vel_ic"][0], (double) root["vel_ic"][1], (double) root["vel_ic"][2]);
    }
    if (pressure_ic_type == Pressure_IC::USER_DEFINED_P) {
      p_ic = (double) root["p_ic"];
    }

  }

 protected:
  Vel_IC read_vel_ic(const std::string &str) const {
    if (str == "zero") {
      return Vel_IC::ZERO_VEL;
    } else if (str == "user_defined") {
      return Vel_IC::USER_DEFINED_VEL;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid IC type for vel";
  }
  Pressure_IC read_pressure_ic(const std::string &str) const {
    if (str == "zero") {
      return Pressure_IC::ZERO_P;
    } else if (str == "user_defined") {
      return Pressure_IC::USER_DEFINED_P;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid IC type for pressure";
    }
  }

};

/// Solver Options
struct SolverOptions {
  std::map<std::string, std::string> vals;

  /**
   * Applies val to a PetscOptions database, i.e. adds "[prefix][key] [value]"
   * to a PETSc options database
   * (i.e. the command line).
   * Note that prefix MUST START WITH A -. (PETSc automatically strips the
   * first character...)
   * If you do not call this function, SolverOptions basically does nothing.
   * @param prefix command line prefix, must start with a hyphen (-)
   * @param opts PETSc options database, use NULL for the global options
   * database
   */
  inline void apply_to_petsc_options(const std::string &prefix = "-",
                                     PetscOptions opts = nullptr) {
    for (auto it = vals.begin(); it != vals.end(); it++) {
      const std::string name = prefix + it->first;

      // warn if the option was also specified as a command line option
      PetscBool exists = PETSC_FALSE;
      PetscOptionsHasName(opts, nullptr, name.c_str(), &exists);
      if (exists)
        PrintWarning("Overriding command-line option '", name,
                     "' with config file value '", it->second, "'.");

      PetscOptionsSetValue(opts, name.c_str(), it->second.c_str());
    }
  }
};

struct NSInputData : public TALYFEMLIB::InputData {

  /// solver (STABILIZED_NS) and Variational multiscale based solver (RBVMS_NS)
  enum typeNSsolver { STABILIZED_NS = 0, RBVMS_NS = 1 };

  /// Declare the Solver type for NS
  typeNSsolver solverTypeNS;
  /// Declare the Solver type for HT

  /// Time stepper
  std::vector<double> dt;
  std::vector<double> totalT;

  double OutputStartTime;

  int OutputInterval;
  double thetaTimeStepping = 1;

  /// VMS and weakBC parameters
  double Ci_f;
  double tauM_scale;

  /// Non-dimensional parameter
  double Re;
  double Fr;

  MeshDef mesh_def;

  std::vector<BoundaryDef> boundary_def;
  InitialConditionDef ic_def;

  /// Solver options for PETSc are handled in these structures
  SolverOptions solverOptionsNS;

  /// dump_vec (for regression test)
  bool dump_vec = false;

  /// Debug options
  bool Debug_Integrand = false;

  NSInputData()
      : InputData(),
        OutputStartTime(0),
        OutputInterval(1) {}

  ~NSInputData() = default;

  static SolverOptions read_solver_options(libconfig::Config &cfg,
                                           const char *name,
                                           bool required = true) {
    if (!cfg.exists(name)) {
      if (!required) {
        PrintInfo(name, " not found (no additional options in config file)");
        return SolverOptions();
      } else {
        throw TALYFEMLIB::TALYException() << "Missing required solver options '" << name
                                          << "'";
      }
    }

    SolverOptions opts;
    const auto &setting = cfg.getRoot()[name];

    for (int i = 0; i != setting.getLength(); ++i) {
      std::string val;
      switch (setting[i].getType()) {
        case libconfig::Setting::TypeInt:
        case libconfig::Setting::TypeInt64:val = std::to_string((long) (setting[i]));
          break;
        case libconfig::Setting::TypeFloat: {
          std::stringstream ss;
          ss << std::setprecision(16) << ((double) setting[i]);
          val = ss.str();
          break;
        }
        case libconfig::Setting::TypeBoolean:val = ((bool) setting[i]) ? "1" : "0";
          break;
        case libconfig::Setting::TypeString:val = (const char *) setting[i];
          break;
        default:
          throw TALYFEMLIB::TALYException() << "Invalid solver option value type ("
                                            << setting[i].getPath() << ") "
                                            << "(type ID " << setting[i].getType() << ")";
      }

      PrintInfo(name, ".", setting[i].getName(), " = ", val);
      opts.vals[setting[i].getName()] = val;
    }
    return opts;
  }

  /// read configure from file
  bool ReadFromFile(const std::string &filename = std::string("config.txt")) {
    /// fill cfg, but don't initialize default fields
    ReadConfigFile(filename);
    nsd = 3;
    ifDD = true;

    mesh_def.read_from_config(cfg.getRoot()["background_mesh"]);

    /// timestep control
    {
      if (cfg.exists("dt_V")) {
        const libconfig::Setting &dt_ = cfg.getRoot()["dt_V"];
        for (int i = 0; i < dt_.getLength(); ++i) {
          dt.push_back(dt_[i]);
        }
      } else {
        double dt_const;
        ReadValueRequired("dt", dt_const);
        dt.push_back(dt_const);
      }

      if (cfg.exists("totalT_V")) {
        const libconfig::Setting &totalT_ = cfg.getRoot()["totalT_V"];
        for (int i = 0; i < totalT_.getLength(); ++i) {
          totalT.push_back(totalT_[i]);
        }
      } else {
        double totalT_const;
        ReadValueRequired("totalT", totalT_const);
        totalT.push_back(totalT_const);
      }
    }


    /// Output control
    if (ReadValue("OutputStartTime", OutputStartTime)) {}
    if (ReadValue("OutputInterval", OutputInterval)) {}
    if (ReadValue("dump_vec", dump_vec)) {}
    if (ReadValue("thetaTimeStepping", thetaTimeStepping)) {}
    PrintStatus("theta for Time Stepping ", thetaTimeStepping);

    /// Solver selection
    /// Read type of NS solver
    solverTypeNS = read_NSsolver(cfg.getRoot(), "NavierStokesSolverType");

    /// VMS parameters
    if (solverTypeNS == typeNSsolver::RBVMS_NS) {
      ReadValueRequired("Ci_f", Ci_f);
      ReadValueRequired("tauM_scale", tauM_scale);
    }


    /// Non-dimensional parameters
    ReadValue("Re", Re);
    ReadValue("Fr", Fr);

    /// always have 6 boundary_def in the order of x-, x+, y-, y+, z-, z+
    boundary_def.resize(6);
    boundary_def[0].side = BoundaryDef::Side::X_MINUS;
    boundary_def[1].side = BoundaryDef::Side::X_PLUS;
    boundary_def[2].side = BoundaryDef::Side::Y_MINUS;
    boundary_def[3].side = BoundaryDef::Side::Y_PLUS;
    boundary_def[4].side = BoundaryDef::Side::Z_MINUS;
    boundary_def[5].side = BoundaryDef::Side::Z_PLUS;
    if (cfg.exists("boundary")) {
      const auto &bc = cfg.getRoot()["boundary"];
      for (unsigned int i = 0; i < boundary_def.size(); i++) {
        for (int j = 0; j < bc.getLength(); j++) {
          /// If the side of bc in config matches with the preset bc
          if (boundary_def[i].side == boundary_def[i].read_side_from_config(bc[j])) {
            boundary_def[i].read_from_config(bc[j]);
          }
        }
      }
    }

    /// read Initial condition
    if (cfg.exists("initial_condition")) {
      const auto &ic = cfg.getRoot()["initial_condition"];
      ic_def.read_from_config(ic);
    }

    /// Solver Options
    /// Simulation parameters and tolerances
    solverOptionsNS = read_solver_options(cfg, "solver_options");

    /// Debug options
    if (ReadValue("Debug_Integrand", Debug_Integrand)) {}
    return true;
  }

  /// check if the input are valid
  bool CheckInputData() {
    if ((typeOfIC == 0) && (inputFilenameGridField.empty())) {
      PrintWarning("IC not set properly check!", typeOfIC, " ",
                   inputFilenameGridField);
      return false;
    }

    /// check timestep
    if (dt.size() != totalT.size()) {
      PrintWarning("Mismatch in timestep control, check dt and totalT!");
      return false;
    }
    if (!OutputInterval) {
      PrintWarning("OutputInterval = 0, error!");
      return false;
    }
    /// checkout bc for all six faces
    if (boundary_def.size() != 6) {
      PrintWarning("BC side have wrong number:", boundary_def.size());
      return false;
    }

    return true;
  }

 private:
  /// Function for reading case dependent parameters
  template<typename T>
  void ReadValueRequired(const std::string &name, T &out) {
    if (!ReadValue(name, out))
      throw TALYFEMLIB::TALYException() << "Required parameter missing: " << name;
  }

  /// Function for reading type of Navier Stokes solver
  static typeNSsolver read_NSsolver(libconfig::Setting &root,
                                    const char *name) {
    std::string str;
    /// If nothing specified stays stabilizedNS
    if (root.lookupValue(name, str)) {
      if (str == "stabilizedNS") {
        return STABILIZED_NS;
      } else if (str == "rbvmsNS") {
        return RBVMS_NS;
      } else {
        throw TALYFEMLIB::TALYException() << "Unknown solver name for NS: " << name << str;
      }
    } else {
      throw TALYFEMLIB::TALYException() << "Must specify NavierStokesSolverType: stabilizedNS or rbvmsNS";
    }

  }

};
