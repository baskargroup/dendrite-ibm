//
// Created by maksbh on 5/27/20.
//

#include <iostream>
#include <DendriteUtils.h>
#include "../include/NSHTNodeData.h"
#include "../include/NSHTInputData.h"
#include <IBM/IBMSolver.h>

int main(int argc, char **argv){
  dendrite_init(argc,argv);
  NSHTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }

  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  enum Timings:int{
    NORMAL = 0,
    RAY_TRACING = 1,
    ONLY_RAY_TRACING=2
  };

  DENDRITE_UINT  refineLvl = idata.mesh_def.refine_l;
  ot::DA * octDA = createRegularDA(refineLvl);

  idata.solverOptionsHT.apply_to_petsc_options("-ht_");
  idata.solverOptionsNS.apply_to_petsc_options("-ns_");
  Point d_min(idata.mesh_def.channel_min.x(),
              idata.mesh_def.channel_min.y(),
              idata.mesh_def.channel_min.z());
  Point d_max(idata.mesh_def.channel_max.x(),
              idata.mesh_def.channel_max.y(),
              idata.mesh_def.channel_max.z());

  const double scalingFactor[3]{idata.mesh_def.channel_max.x(),
                                idata.mesh_def.channel_max.y(),
                                idata.mesh_def.channel_max.z()};
  /// load IBM meshes
  std::vector<Geom<NSHTNodeData> *> geoms;
  for (const auto &geom_def : idata.geom_defs) {
    geoms.push_back(new Geom<NSHTNodeData>(geom_def));
  }
  auto ibmBC = [&geoms](unsigned int dof) {
    if (dof == NSHTNodeData::TEMPERATURE) {
      return 1.0;
    }
    return 0.0;
  };
  auto ibmSolver = new IBMSolver<NSHTNodeData>(octDA,geoms,scalingFactor,ibmBC);
  ibmSolver->partitionGeometries();
  MPI_Barrier(MPI_COMM_WORLD);

  const int nPe = octDA->getNumNodesPerElement();
  TalyDendroSync sync;

  double coords[8*3];
  GRID grid;
  grid.redimArrays(nPe, 1);
  for (DENDRITE_INT i = 0; i < nPe; i++) {
    grid.node_array_[i] = new NODE();
  }

  ELEM *elem = new ELEM3dHexahedral();
  grid.elm_array_[0] = elem;

  DENDRITE_INT *node_id_array = new DENDRITE_INT[nPe];
  for (int i = 0; i < nPe; i++) {
    node_id_array[i] = i;
  }
  elem->redim(nPe, node_id_array);

  FEMElm fe(&grid, BASIS_ALL);
  std::chrono::high_resolution_clock::time_point t1,t2 ;
  double timings[3]{0.0,0.0,0.0};
  int counter[2]{0,0};

  for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    octDA->getElementalCoords(octDA->curr(),coords);
    for(int i = 0; i < 8; i++){
      convertOctToPhys(&coords[3*i],scalingFactor);
      grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }
    fe.refill(elem, BASIS_LINEAR, idata.gp_rel_order);

    auto triangles = ibmSolver->getTriInOctant(octDA->curr());


    if(not(triangles.empty())) {
      std::vector<bool> dots(triangles.size());
      while (fe.next_itg_pt()) {
        bool isNormalBased = false;
        t1 = std::chrono::high_resolution_clock::now();
        int count = 0;
        for (const auto &tri : triangles) {
          dots[count++] = (tri.normal.innerProduct(fe.position() - tri.centroid) >= 0);
        }
        if ((std::all_of(dots.begin(), dots.end(), [](bool v) { return v; }))
            or (std::none_of(dots.begin(), dots.end(), [](bool v) { return v; }))) {
          counter[NORMAL]++;
          isNormalBased = true;
        }
        else {
          isNormalBased = false;
          counter[RAY_TRACING]++;
          ibmSolver->point_in_any_geometry(fe.position());
        }

        t2 = std::chrono::high_resolution_clock::now();
        if(isNormalBased){
          timings[NORMAL] += std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        }
        else{
          timings[RAY_TRACING] += std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        }
      }
    }
  }

  for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    octDA->getElementalCoords(octDA->curr(),coords);
    for(int i = 0; i < 8; i++){
      convertOctToPhys(&coords[3*i],scalingFactor);
      grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }
    fe.refill(elem, BASIS_LINEAR, idata.gp_rel_order);

    auto & triangles = ibmSolver->getTriInOctant(octDA->curr());
    if(not(triangles.empty())){
     while (fe.next_itg_pt()) {
       t1 = std::chrono::high_resolution_clock::now();
       ibmSolver->point_in_any_geometry(fe.position());
       t2 = std::chrono::high_resolution_clock::now();
       timings[ONLY_RAY_TRACING] += std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
    }
    }
  }

  double globalTimings[3];
  int globalCounters[2];
  MPI_Reduce(timings,globalTimings,3,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Reduce(counter,globalCounters,2,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);

  TALYFEMLIB::PrintStatus("Number of successful In - out tests ", globalCounters[NORMAL], " out of ", globalCounters[NORMAL] + globalCounters[RAY_TRACING] );
  TALYFEMLIB::PrintStatus("Fraction of successful in - out tests ", (globalCounters[NORMAL]*1.0)/((globalCounters[NORMAL] + globalCounters[RAY_TRACING])*1.0));

  TALYFEMLIB::PrintStatus("Time for in - out tests: Normal computation =  ", globalTimings[NORMAL]);
  TALYFEMLIB::PrintStatus("Time for in - out tests: Ray - tracing based =  ", globalTimings[RAY_TRACING]);
  TALYFEMLIB::PrintStatus("Time for in - out tests:  =  ", globalTimings[NORMAL] + globalTimings[RAY_TRACING]);

  TALYFEMLIB::PrintStatus("Time for ray - tracing ", globalTimings[ONLY_RAY_TRACING]);
  MPI_Barrier(MPI_COMM_WORLD);
  dendrite_finalize(octDA);
}
