Running
=======
* The config file from [NSHT_dendro5/tests/In-Out](../NSHT_dendro5/tests/In-Out) should be copied to the run directory.
* The following parameters in the config file needs to be updated.
* The geometry should be updated in the config file under:
```bash
geometries = (
  {
    mesh_path = "bunny-done.stl" #path of gepmetry
    is_static = true
    position = [0, 0, 0]
    type = "meshobject"
    refine_lvl = 7
    gp_level = 0
    bc_type_V = ["dirichlet", "dirichlet", "dirichlet", "dirichlet", "dirichlet"]
    dirichlet_V = [0.0, 0.0, 0.0, 0.0, 0.0]
  }
)
```
* Launch `mpirun -np $NPROC ./in-out` for weighted partitioing.