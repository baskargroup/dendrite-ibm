//
// Created by maksbh on 4/14/20.
//

#ifndef NAVIER_STOKES_HEAT_TRANSFER_UTILS_H
#define NAVIER_STOKES_HEAT_TRANSFER_UTILS_H
#include <DendriteUtils.h>
#include "IBM/IBMSolver.h"
/**
 * Resets the interior od the geometry to 0.
 * @param octDA
 * @param vec
 * @param ndof
 * @param ibmSolver
 * @param scalingFactor
 */
 template <typename NodeData>
void resetVecs(ot::DA * octDA,Vec & vec,const DENDRITE_UINT ndof,const IBMSolver<NodeData> * ibmSolver, const double * scalingFactor){
  double *localArray;
  VecGetArray(vec,&localArray);

  double * ghostedArray;
  octDA->nodalVecToGhostedNodal(localArray,ghostedArray, false,ndof);
  octDA->readFromGhostBegin(ghostedArray,ndof);
  octDA->readFromGhostEnd(ghostedArray,ndof);
  int npe = octDA->getNumNodesPerElement();
  double * coords = new double[m_uiDim*npe];
  int eleOrder = octDA->getElementOrder();
  int totalNumNodes = octDA->getTotalNodalSz();

  DendroIntL * localnodeIdx = new DendroIntL[npe];

  for (octDA->init<ot::DA_FLAGS::WRITABLE>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::WRITABLE>(); octDA->next<ot::DA_FLAGS::WRITABLE>()){
    octDA->getElementalCoords(octDA->curr(),coords);
    octDA->getNodeIndices(localnodeIdx,octDA->curr(),true);

    for(int i = 0; i < npe; i++){
      convertOctToPhys(&coords[m_uiDim*i],scalingFactor);
    }

    int counter = 0;
    for (DENDRITE_UINT iz = 0; iz < (eleOrder + 1); iz++) {
      for (DENDRITE_UINT iy = 0; iy < (eleOrder + 1); iy++) {
        for (DENDRITE_UINT ix = 0; ix < (eleOrder + 1); ix++) {
          ZEROPTV pt {coords[counter*m_uiDim + 0],coords[counter*m_uiDim + 1],coords[counter*m_uiDim + 2]};
          if(ibmSolver->point_in_any_geometry(pt)) {
            if (not(octDA->getMesh()->isNodeHanging(octDA->curr(), ix, iy, iz))) {
              for (int dof = 0; dof < ndof; dof++) {
                ghostedArray[dof * totalNumNodes + localnodeIdx[counter]] = 0;
              }
            }
          }
          counter++;
        }
      }
    }
  }
  octDA->ghostedNodalToNodalVec(ghostedArray,localArray,true,ndof);
  delete[] coords;
  delete[] ghostedArray;
  delete[] localnodeIdx;


  VecRestoreArray(vec,&localArray);
}
#endif //NAVIER_STOKES_HEAT_TRANSFER_UTILS_H
