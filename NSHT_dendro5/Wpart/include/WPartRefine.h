//
// Created by maksbh on 6/1/20.
//

#ifndef NAVIER_STOKES_HEAT_TRANSFER_WPARTREFINE_H
#define NAVIER_STOKES_HEAT_TRANSFER_WPARTREFINE_H

#include <Refinement/Refinement.h>


class WpartRefine : public Refinement {
 public:
  WpartRefine(ot::DA *da, const DENDRITE_REAL *problemSize);
  ot::DA_FLAGS::Refine calcRefineFlags() override;
};

WpartRefine::WpartRefine(ot::DA *da, const DENDRITE_REAL *problemSize)
    : Refinement(da, problemSize) {
    this->calcRefinementVector();
}

ot::DA_FLAGS::Refine WpartRefine::calcRefineFlags(){
  return ot::DA_FLAGS::Refine::DA_NO_CHANGE;
  if((TALYFEMLIB::GetMPIRank() == 0) and (baseDA_->curr() == 0)){
    return ot::DA_FLAGS::Refine::DA_REFINE;
  }
  else{
    return  ot::DA_FLAGS::Refine::DA_NO_CHANGE;
  }
}
#endif //NAVIER_STOKES_HEAT_TRANSFER_WPARTREFINE_H
