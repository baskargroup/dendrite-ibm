#pragma once

#include "NSEquation.h"
#include "NSHTNodeData.h"
#include "NSHTInputData.h"
#include "IBM/IBMSolver.h"

using TALYFEMLIB::FEMElm;

/// Force info
struct ForceInfo {
  ZEROPTV Force_pressure;
  ZEROPTV Force_viscous;
  ZEROPTV Force_penalty;
  double mass_conservation;
  ZEROPTV Torque;
  ZEROPTV Force_all() {
    return Force_pressure + Force_viscous + Force_penalty;
  }

  /**
   * Adds rhs to *this.
   * @param rhs other vector to add to this one
   * @returns *this
   */
  inline ForceInfo &operator+=(const ForceInfo &rhs) {
    this->Force_pressure += rhs.Force_pressure;
    this->Force_viscous += rhs.Force_viscous;
    this->Force_penalty += rhs.Force_penalty;
    this->mass_conservation += rhs.mass_conservation;
    this->Torque += rhs.Torque;
    return *this;
  }

};

/**
 * Class responsible for doing post-processing work for NS equation
 */
class NSPostProcess {
 public:
  NSPostProcess(NSHTInputData *inputData,
                IBMSolver<NSHTNodeData> *ibmSolver,
                TalyEquation<NSEquation, NSHTNodeData> *talyEq,
                TimeInfo ti);
/*  NSPostProcess(NSHTInputData *inputData,
                TalyEquation<NSEquation, NSHTNodeData> *talyEq,
                unsigned int surface2cal,
                TimeInfo ti);*/

  /// functions to calc force and torque
  std::vector<ForceInfo> calcForceObject(ot::DA &m_octDA,
                                         const double *daScalingFactor);
  void PrintForceInfo();
  void PrintKinematicStatus();

 private:
  ForceInfo calcForceElementIBM(const FEMElm &fe,
                                const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                const ZEROPTV com,
                                const Point &node,
                                const Point &h);
  ForceInfo calcForcePerMpiIBM(ot::DA &da,
                               const double *daScalingFactor,
                               const IBMSolver<NSHTNodeData>::GeomData &geom_data,
                               bool visitGhostElements = false);

  /// member variables
  const NSHTInputData *idata_;
  unsigned int surface2cal_ = 0;
  TalyEquation<NSEquation, NSHTNodeData> *talyEq_;
  TimeInfo ti_;
  IBMSolver<NSHTNodeData> *ibmSolver_;
  std::vector<ForceInfo> forceInfo;

};

NSPostProcess::NSPostProcess(NSHTInputData *inputData,
                             IBMSolver<NSHTNodeData> *ibmSolver,
                             TalyEquation<NSEquation, NSHTNodeData> *talyEq,
                             TimeInfo ti)
    : idata_(inputData), ibmSolver_(ibmSolver), talyEq_(talyEq), ti_(ti) {
}

/*NSPostProcess::NSPostProcess(NSHTInputData *inputData,
                             TalyEquation<NSEquation, NSHTNodeData> *talyEq,
                             unsigned int surface2cal,
                             TimeInfo ti)
    : idata_(inputData), talyEq_(talyEq), surface2cal_(surface2cal), ti_(ti) {
}*/

/**
 * calculate the force and torque of one element
 * @param fe
 * @param gpinfo
 * @param node
 * @param h
 * @return
 */
ForceInfo NSPostProcess::calcForceElementIBM(const FEMElm &fe,
                                             const Geom<NSHTNodeData>::GaussPointInfo &gpinfo,
                                             const ZEROPTV com,
                                             const Point &node,
                                             const Point &h) {
  const int nsd = idata_->nsd;
  const double Coe_diff = idata_->getDiffusionNS(ti_.getCurrentTime());
  const double Cb_f = idata_->Cb_f;
  const double detSideJxW = fe.detJxW();
  const ZEROPTV &normal = gpinfo.gp_normal;
  const ZEROPTV &vel = gpinfo.gp_velocity;

  ZEROPTV u;
  for (int i = 0; i < nsd; i++) {
    u(i) = talyEq_->field()->valueFEM(fe, i);
  }

  double p = talyEq_->field()->valueFEM(fe, NSHTNodeData::PRESSURE);

  ZeroMatrix<double> du;
  du.redim(nsd, nsd);
  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      du(i, j) = talyEq_->field()->valueDerivativeFEM(fe, i, j) + talyEq_->field()->valueDerivativeFEM(fe, j, i);
    }
  }

  ZEROPTV diff;
  for (int i = 0; i < nsd; i++) {
    diff(i) = 0;
    for (int j = 0; j < nsd; j++) {
      diff(i) += Coe_diff * du(i, j) * normal(j);
    }
  }

  // wall normal distance
  double hb = MathOp::normalDistance(fe.position(), normal, node, h);
  /// Calculate the force and torque
  ForceInfo ForceInfo;
  ZEROPTV force_all;
  /// Force of object on the fluids, because the normals are pointed INSIDE (in the loading process)
  /// This needs extra case when you are calculating the kinematic info of the object
  for (int dim = 0; dim < nsd; dim++) {
    ForceInfo.Force_pressure(dim) = (-p * normal(dim)) * detSideJxW;
    ForceInfo.Force_viscous(dim) = (diff(dim)) * detSideJxW;
    ForceInfo.Force_penalty(dim) = -Cb_f * (u(dim) - vel(dim)) * detSideJxW;
    ForceInfo.mass_conservation = du(0, 0) + du(1, 1) + du(2, 2);
    force_all(dim) = ForceInfo.Force_pressure(dim) + ForceInfo.Force_viscous(dim) + ForceInfo.Force_penalty(dim);
  }
//ForceInfo.Force_all()
  for (int dim = 0; dim < nsd; dim++) {
    ForceInfo.Torque(dim) =
        force_all((dim + 2) % nsd)
            * (fe.position()((dim + 1) % nsd) - com((dim + 1) % nsd))
            - force_all((dim + 1) % nsd)
                * (fe.position()((dim + 2) % nsd) - com((dim + 2) % nsd));
  }
//  PrintStatus(geo->pI.center_of_mass);
  return ForceInfo;
}

/**
 * calculate force per mpi, need to be summed up
 * @param da
 * @param daScalingFactor
 * @param _in
 * @param gps
 * @param visitGhostElements
 * @return
 */
ForceInfo NSPostProcess::calcForcePerMpiIBM(ot::DA &da,
                                            const double *daScalingFactor,
                                            const IBMSolver<NSHTNodeData>::GeomData &geom_data,
                                            bool visitGhostElements) {
  const unsigned int ndof = NSHTNodeData::NS_DOF;
  auto syncVal = talyEq_->mat->getSyncValuesVec();
  ForceInfo force_sum;

  for (const auto &s : syncVal) {
    if (s.val == nullptr) {
      return force_sum;
    }
  }

  int npe = da.getNumNodesPerElement();
  int eleOrder = da.getElementOrder();

  DENDRITE_REAL **_val_;
  DENDRITE_REAL *coords;
  std::vector<ZEROPTV> nodal_pos(npe);
  coords = new DENDRITE_REAL[m_uiDim * npe];
  _val_ = new DENDRITE_REAL *[syncVal.size()];
  for (int i = 0; i < syncVal.size(); i++) {
    _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
  }
  talyEq_->mat->preMat();
  TalyDendroSync sync;


  /** Looping over all the background element**/
  ZEROPTV ptvl; /// Local point in isoparameteric space
  ZEROPTV ptvg; /// Gauss point
  for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size(); gpID++) {

    if (geom_data.my_gauss_points[gpID].isLocal == true) {
      /// get element coordinates and data
      unsigned int eleID = geom_data.backgroundElementGP[gpID];

      da.getElementalCoords(eleID, coords);
      coordsToZeroptv(coords, nodal_pos, daScalingFactor, eleOrder, true);
      talyEq_->mat->getSyncValues(_val_, eleID);

      if (eleOrder == 1) {
        sync.syncCoords<1, m_uiDim>(coords, talyEq_->grid());
      } else if (eleOrder == 2) {
        sync.syncCoords<2, m_uiDim>(coords, talyEq_->grid());
      }
      for (int i = 0; i < syncVal.size(); i++) {
        if (eleOrder == 1) {
          sync.syncValues<NSHTNodeData, 1, m_uiDim>(_val_[i], talyEq_->field(), syncVal[i].ndof,
                                                    syncVal[i].nodeDataIndex);
        } else if (eleOrder == 2) {
          sync.syncValues<NSHTNodeData, 2, m_uiDim>(_val_[i], talyEq_->field(), syncVal[i].ndof,
                                                    syncVal[i].nodeDataIndex);
        }
      }

      /// element corner and size
      Point position(coords[0], coords[1], coords[2]);
      int level = da.getLevel(eleID);
      double h1 = daScalingFactor[0] / (1u << level);
      double h2 = daScalingFactor[1] / (1u << level);
      double h3 = daScalingFactor[2] / (1u << level);
      Point h(h1, h2, h3);

      /// Prepare background element data
      FEMElm fe(talyEq_->grid(), TALYFEMLIB::BASIS_ALL);

      if (eleOrder == 1) {
        fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
      } else if (eleOrder == 2) {
        fe.refill(talyEq_->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
      }

      ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
      ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
      ptvg.z() = geom_data.my_gauss_points[gpID].values[2];
      GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
      const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label);
      fe.calc_at(ptvl);
      fe.set_jacc_x_w(gpinfo.area * (1.0 / 3.0));

      /// Calculate elemental force
      ForceInfo force_elem;
      ZEROPTV com = geom_data.geo->pI.center_of_mass;
      force_elem = calcForceElementIBM(fe, gpinfo, com, position, h);
      force_sum += force_elem;
    }
  }

  /** Post Mat **/
  for (int i = 0; i < syncVal.size(); i++) {
    delete[] _val_[i];
  }
  delete[] coords;
  talyEq_->mat->postMat();

  return force_sum;
}

/// this is the function to be called when calculating the force on a object
std::vector<ForceInfo> NSPostProcess::calcForceObject(ot::DA &m_octDA,
                                                      const double *daScalingFactor) {

  PrintStatus("Solve for force");
  forceInfo.clear();
  for (const auto &geom_data : ibmSolver_->getGeometries()) {
    ForceInfo forceInfo_local;
    ForceInfo forceInfo_all;

    forceInfo_local = calcForcePerMpiIBM(m_octDA, daScalingFactor, geom_data);
    MPI_Comm comm = m_octDA.getCommActive();
    MPI_Allreduce(forceInfo_local.Force_pressure.data(),
                  forceInfo_all.Force_pressure.data(),
                  3,
                  MPI_DOUBLE,
                  MPI_SUM,
                  comm);
    MPI_Allreduce(forceInfo_local.Force_viscous.data(),
                  forceInfo_all.Force_viscous.data(),
                  3,
                  MPI_DOUBLE,
                  MPI_SUM,
                  comm);
    MPI_Allreduce(forceInfo_local.Force_penalty.data(),
                  forceInfo_all.Force_penalty.data(),
                  3,
                  MPI_DOUBLE,
                  MPI_SUM,
                  comm);
    MPI_Allreduce(forceInfo_local.Torque.data(), forceInfo_all.Torque.data(), 3, MPI_DOUBLE, MPI_SUM, comm);
    MPI_Allreduce(&forceInfo_local.mass_conservation,
                  &forceInfo_all.mass_conservation,
                  1,
                  MPI_DOUBLE,
                  MPI_SUM,
                  comm);
    forceInfo.push_back(forceInfo_all);
  }
  PrintForceInfo();
  return forceInfo;
}

void NSPostProcess::PrintForceInfo() {
  for (int i = 0; i < ibmSolver_->getGeometries().size(); i++) {
    const auto geom_data = ibmSolver_->getGeometries()[i];
    ForceInfo geomForce = forceInfo[i];
    /// Print results
    if (surface2cal_) {
//      PrintStatus("surface:\t", surface2cal_, "\tNu:\t", Nu[i][0], " + ", Nu[i][1], " = ", Nu[i][0] + Nu[i][1]);
    } else {
      /// Print parameters
      PrintStatus(ti_.getCurrentTime(), " " , geom_data.geo->def_.name, " force: ", geomForce.Force_all() * (-1));
      PrintStatus(ti_.getCurrentTime()," " ,geom_data.geo->def_.name, " torque: ", geomForce.Torque * (-1));
    }
    if (geom_data.geo->def_.ifPostprocess) {
      /// print force info to file for logging
      int rank, size;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
      MPI_Comm_size(PETSC_COMM_WORLD, &size);
      /// Save to file
      if (!rank) {
        std::string fname = "Force_" + geom_data.geo->def_.name + ".dat";
        if (surface2cal_) {
          fname = "Force_" + to_string(surface2cal_) + ".dat";
        }
        FILE *fp = fopen(fname.c_str(), "a");
        assert (fp != nullptr);
        fprintf(fp,
                "Timestep: %1d, Time: %.3f\n"
                "Fx_pre = % .10e, Fy_pre = % .10e, Fz_pre = % .10e\n"
                "Fx_vis = % .10e, Fy_vis = % .10e, Fz_vis = % .10e\n"
                "Fx_pen = % .10e, Fy_pen = % .10e, Fz_pen = % .10e\n"
                "mass_conservation = % .10e\n"
                "Tx = % .10e, Ty = % .10e, Tz = % .10e\n",
                ti_.getTimeStepNumber(),
                ti_.getCurrentTime(),
                geomForce.Force_pressure(0), geomForce.Force_pressure(1), geomForce.Force_pressure(2),
                geomForce.Force_viscous(0), geomForce.Force_viscous(1), geomForce.Force_viscous(2),
                geomForce.Force_penalty(0), geomForce.Force_penalty(1), geomForce.Force_penalty(2),
                geomForce.mass_conservation,
                geomForce.Torque(0), geomForce.Torque(1), geomForce.Torque(2));
        fclose(fp);
      }
    }
  }

}

/**
 * Output Kinematic info for the moving geometry only.
 * @param ti
 * @param geoms
 */
void NSPostProcess::PrintKinematicStatus() {
  for (int i = 0; i < ibmSolver_->getGeometries().size(); i++) {
    const auto geom = ibmSolver_->getGeometries()[i].geo;
    if (!geom->def_.is_static) {
      ZEROPTV vel = geom->kI.vel;
      ZEROPTV vel_ang = geom->kI.vel_ang;
      ZEROPTV center = geom->kI.center;
      PrintStatus("object velocity: ", vel(0), " ", vel(1), " ", vel(2));
      PrintStatus("object rotation_velocity: ", vel_ang(0), " ", vel_ang(1), " ", vel_ang(2));
      PrintStatus("object center: ", center(0), " ", center(1), " ", center(2));
      if (geom->def_.ifPostprocess) {
        /// print KinematicInfo to file for logging
        int rank, size;
        MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
        MPI_Comm_size(PETSC_COMM_WORLD, &size);
        /// Save to file
        if (!rank) {
          string fname = "KinematicInfo_" + geom->def_.name + ".dat";
          if (geom->def_.name.empty()) {
            fname = "KinematicInfo.dat";
          }
          FILE *fp = fopen(fname.c_str(), "a");
          assert (fp != nullptr);
          fprintf(fp, "Timestep: %1d, Time: %.3f\n"
                      "vel_x = % .10e, vel_y = % .10e, vel_z = % .10e\n"
                      "rot_vel_x = % .10e, rot_vel_y = % .10e, rot_vel_z = % .10e\n"
                      "center_x = % .10e, center_y = % .10e, center_z = % .10e\n",
                  ti_.getTimeStepNumber(),
                  ti_.getCurrentTime(),
                  vel(0), vel(1), vel(2),
                  vel_ang(0), vel_ang(1), vel_ang(2),
                  center(0), center(1), center(2));
          fclose(fp);
        }
      }
    }
  }
}