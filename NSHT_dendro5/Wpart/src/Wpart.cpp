//
// Created by maksbh on 6/1/20.
//
#include <iostream>
#include <DendriteUtils.h>
#include <NSHTInputData.h>
#include <NSEquation.h>
#include <IBM/IBMUtils.h>
#include <NSHTBCSetup.h>
#include <PETSc/PetscUtils.h>
#include "../include/WPartUtils.h"
#include "../include/WPartRefine.h"


static unsigned int getNodeWeight(const ot::TreeNode *t) {

  return t->getWeight();
}


int main(int argc, char **argv){
  dendrite_init(argc,argv);
  NSHTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }

  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }
  /// load IBM meshes
  std::vector<Geom<NSHTNodeData> *> geoms;
  for (const auto &geom_def : idata.geom_defs) {
    geoms.push_back(new Geom<NSHTNodeData>(geom_def));
  }

  const double factor = idata.surface_cost;
  ot::DA * octDA = createRegularDA(idata.mesh_def.refine_l);

  TimeInfo ti(0, idata.dt, idata.totalT);
  Point d_min(idata.mesh_def.channel_min.x(),
              idata.mesh_def.channel_min.y(),
              idata.mesh_def.channel_min.z());
  Point d_max(idata.mesh_def.channel_max.x(),
              idata.mesh_def.channel_max.y(),
              idata.mesh_def.channel_max.z());

  const double scalingFactor[3]{idata.mesh_def.channel_max.x(),
                                idata.mesh_def.channel_max.y(),
                                idata.mesh_def.channel_max.z()};
  auto ibmBC = [&geoms](unsigned int dof) {
    return 0.0;
  };
  auto ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);

  auto nsEq = new TalyEquation<NSEquation,NSHTNodeData>(octDA,d_min,d_max,NSHTNodeData::NS_DOF,false,&idata,ibm);


  auto nsMat = nsEq->mat;
  auto nsVec = nsEq->vec;
  nsMat->setTime(&ti);
  nsVec->setTime(&ti);
  if (octDA->isActive()) {
    ibm->partitionGeometries();
  }
  std::vector<DENDRITE_UINT>numVolumeGaussPoint,numSurfaceGaussPoint;
  getNumberofOutGaussPoints(octDA,numVolumeGaussPoint,ibm,scalingFactor);
  getNumSurfaceGaussPoint(octDA,numSurfaceGaussPoint,ibm);
  DENDRITE_UINT numSurfaceGP = std::accumulate(numSurfaceGaussPoint.begin(),numSurfaceGaussPoint.end(),0);
  DENDRITE_UINT numVolumeGP = std::accumulate(numVolumeGaussPoint.begin(),numVolumeGaussPoint.end(),0);

  delete ibm;
  delete nsEq;
  std::vector<double > __weight;
  __weight.resize(octDA->getLocalElemSz());
  if(idata.surface_cost == 1000){
    TALYFEMLIB::PrintStatus("uniform weight");
    for(int i = 0;i < octDA->getLocalElemSz(); i++){
      __weight[i] = 1;
    }
  }
  else {
    auto treeNode = octDA->getMesh()->getAllElements();
    int counter = 0;
    for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
         octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
      DENDRITE_UINT weight = static_cast<DENDRITE_UINT >(factor*(numVolumeGaussPoint[counter] / 8.0) + factor*(numSurfaceGaussPoint[counter] / (8.0 * idata.ratio)));
      octDA->setOctantWeight(octDA->curr(),weight);
      __weight[counter] = weight;
      counter++;
    }
  }

  {
    double perProcWeight = std::accumulate(__weight.begin(),__weight.end(),0.0);
    int lsize = octDA->getLocalElemSz();
    std::vector<double> initialPerProcWeight(octDA->getNpesActive(), 0.0);
    std::vector<int> initialNumElements(octDA->getNpesActive(), 0);
    MPI_Gather(&perProcWeight, 1, MPI_DOUBLE, initialPerProcWeight.data(), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(&lsize, 1, MPI_INT, initialNumElements.data(), 1,MPI_INT, 0, MPI_COMM_WORLD);

    if ((TALYFEMLIB::GetMPIRank() == 0)) {
      DENDRITE_REAL maxWeight = *std::max_element(initialPerProcWeight.begin(),initialPerProcWeight.end());
      int maxNumElement = *std::max_element(initialNumElements.begin(),initialNumElements.end());
      std::cout << maxWeight << " "<< maxNumElement << "\n";
      std::ofstream fout("InitialWeight.dat");
      for (int i = 0; i < initialNumElements.size(); i++) {
        fout << (initialPerProcWeight[i]/maxWeight) << " " << (initialNumElements[i]/(maxNumElement*1.0)) << "\n";
      }
      fout.close();

    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  ot::DA * newDA = octDA->repartition(idata.grainSz,0.3,2,getNodeWeight);
  std::swap(newDA,octDA);
  delete newDA;

  MPI_Barrier(MPI_COMM_WORLD);

  ibm = new IBMSolver<NSHTNodeData>(octDA, geoms, scalingFactor, ibmBC, idata.gp_handle, idata.gp_max_splitting,idata.dirichletOnInternalNodes);
  if (octDA->isActive()) {
    ibm->partitionGeometries();
  }
  getNumberofOutGaussPoints(octDA,numVolumeGaussPoint,ibm,scalingFactor);
  getNumSurfaceGaussPoint(octDA,numSurfaceGaussPoint,ibm);
  numSurfaceGP = std::accumulate(numSurfaceGaussPoint.begin(),numSurfaceGaussPoint.end(),0);
  numVolumeGP = std::accumulate(numVolumeGaussPoint.begin(),numVolumeGaussPoint.end(),0);


  std::vector<double> predictedWeight(octDA->getLocalElemSz());

  for(int i = 0; i < predictedWeight.size(); i++){
    predictedWeight[i] =factor*(numVolumeGaussPoint[i]/8.0) + factor*(numSurfaceGaussPoint[i]/(8*idata.ratio));
  }

  {
    double perProcWeight = std::accumulate(predictedWeight.begin(),predictedWeight.end(),0.0);
    int lsize = octDA->getLocalElemSz();
    std::vector<double> finalPerProcWeight(octDA->getNpesActive(), 0.0);
    std::vector<int> finalNumElements(octDA->getNpesActive(), 0);
    MPI_Gather(&perProcWeight, 1, MPI_DOUBLE, finalPerProcWeight.data(), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(&lsize, 1, MPI_INT, finalNumElements.data(), 1,MPI_INT, 0, MPI_COMM_WORLD);

    if ((TALYFEMLIB::GetMPIRank() == 0)) {
      DENDRITE_REAL maxWeight = *std::max_element(finalPerProcWeight.begin(),finalPerProcWeight.end());
      int maxNumElement = *std::max_element(finalNumElements.begin(),finalNumElements.end());
      std::cout << maxWeight << " "  << maxNumElement << "\n";
      std::ofstream fout("FinalWeight.dat");
      for (int i = 0; i < finalPerProcWeight.size(); i++) {
        fout << (finalPerProcWeight[i]/maxWeight) << " " << (finalNumElements[i]/(maxNumElement*1.0)) << "\n";
      }
      fout.close();

    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  nsEq = new TalyEquation<NSEquation,NSHTNodeData>(octDA,d_min,d_max,NSHTNodeData::NS_DOF,false,&idata,ibm);


  nsMat = nsEq->mat;
  nsVec = nsEq->vec;
  nsMat->setTime(&ti);
  nsVec->setTime(&ti);

  auto NSSolver =
      setIBMNonLinearSolver<NSEquation, NSHTNodeData>(nsEq, octDA, geoms, ibm, NSHTNodeData::NS_DOF, false);

  Vec  prev_solution_ns,prev_solution_ht;
  octDA->petscCreateVector(prev_solution_ns, false, false, NSHTNodeData::NS_DOF);
  octDA->petscCreateVector(prev_solution_ht, false, false, NSHTNodeData::HT_DOF);
  VecSet(prev_solution_ht,0);
  idata.solverOptionsNS.apply_to_petsc_options("-ns_");

  const auto resetDAVecs = [&] {
    if (octDA->isActive()) {

      nsMat->setVector({
                           VecInfo(PLACEHOLDER_GUESS, NSHTNodeData::NS_DOF, NSHTNodeData::VEL_X),
                           VecInfo(prev_solution_ns,
                                   NSHTNodeData::NS_DOF,
                                   NSHTNodeData::NUM_VARS + NSHTNodeData::VEL_X,
                                   PLACEHOLDER_NONE),
                           VecInfo(prev_solution_ht,
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::TEMPERATURE),
                       });
      nsVec->setVector({
                           VecInfo(PLACEHOLDER_GUESS, NSHTNodeData::NS_DOF, NSHTNodeData::VEL_X),
                           VecInfo(prev_solution_ns,
                                   NSHTNodeData::NS_DOF,
                                   NSHTNodeData::NUM_VARS + NSHTNodeData::VEL_X,
                                   PLACEHOLDER_NONE),
                           VecInfo(prev_solution_ht,
                                   NSHTNodeData::HT_DOF,
                                   NSHTNodeData::TEMPERATURE),
                       });

      {
        SNES m_snes = NSSolver->snes();
        SNESSetOptionsPrefix(m_snes, "ns_");
        SNESSetFromOptions(m_snes);
      }
    }

  };

  resetDAVecs();

  /// set B.C. for HT & NS
  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Setting BC for HT");
  NSHTBCSetup bc(&idata, &ti);


  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Setting BC for NS");
  NSSolver->setBoundaryCondition([&](double x, double y, double z, unsigned int nodeID) -> Boundary {
    Boundary b;
    ZEROPTV position = {x, y, z};
    bc.setBoundaryConditionsNS(b, position, idata.boundary_def);
    return b;
  });

  const auto initial_condition_ns = [&](double x, double y, double z, int dof) {
    if (dof != NSHTNodeData::PRESSURE) {
      return idata.ic_def.vel_ic[dof];
    }

    return idata.ic_def.p_ic;
  };
  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Setting IC for HT");

  petscSetInitialCondition(octDA, prev_solution_ns, NSHTNodeData::NS_DOF, initial_condition_ns, d_min, d_max);
  resetDAVecs();
  while (ti.getCurrentTime() < (ti.getEndTime() - 1e-16)) {
    NSSolver->solve();
    VecCopy(NSSolver->getCurrentSolution(), prev_solution_ns);
    ti.increment();
  }
  MPI_Barrier(MPI_COMM_WORLD);
  auto timers = NSSolver->getTimers();
  std::vector<double> globalTimersMax (Timers::MAX_TIMERS,0.0);
  std::vector<double> globalTimersMin (Timers::MAX_TIMERS,0.0);
  std::vector<double> globalTimersAvg (Timers::MAX_TIMERS,0.0);
  MPI_Reduce(timers.data(),globalTimersMax.data(),Timers::MAX_TIMERS,MPI_DOUBLE,MPI_MAX,0,octDA->getCommActive());
  MPI_Reduce(timers.data(),globalTimersMin.data(),Timers::MAX_TIMERS,MPI_DOUBLE,MPI_MIN,0,octDA->getCommActive());
  MPI_Reduce(timers.data(),globalTimersAvg.data(),Timers::MAX_TIMERS,MPI_DOUBLE,MPI_SUM,0,octDA->getCommActive());
  for(int i = 0; i < Timers::MAX_TIMERS; i++){
    globalTimersAvg[i] = globalTimersAvg[i]/(octDA->getNpesActive()*1.0);
  }

  PrintStatus("Max time = ",globalTimersMax[0], " " , globalTimersMax[1], " ", globalTimersMax[2], " ", globalTimersMax[3], " ", globalTimersMax[4]," ",globalTimersMax[5]," ",globalTimersMax[6]);
  PrintStatus("Min time = ",globalTimersMin[0], " " , globalTimersMin[1], " ", globalTimersMin[2], " ", globalTimersMin[3], " ", globalTimersMin[4]," ",globalTimersMin[5]," ",globalTimersMin[6]);
  PrintStatus("Avg time = ",globalTimersAvg[0], " " , globalTimersAvg[1], " ", globalTimersAvg[2], " ", globalTimersAvg[3], " ", globalTimersAvg[4]," ",globalTimersAvg[5]," ",globalTimersAvg[6]);
  delete nsEq;
  delete NSSolver;

  MPI_Barrier(MPI_COMM_WORLD);

  dendrite_finalize(octDA);


}