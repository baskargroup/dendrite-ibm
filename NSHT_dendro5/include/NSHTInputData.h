#pragma once

#include <iomanip>
#include <talyfem/input_data/input_data.h>
#include <IBM/GeomDef.h>
#include "NSHTNodeData.h"

using TALYFEMLIB::ZEROPTV;
using TALYFEMLIB::PrintStatusStream;
using TALYFEMLIB::PrintWarning;
using TALYFEMLIB::PrintInfo;
using TALYFEMLIB::PrintStatus;
using TALYFEMLIB::PrintError;

/**
 * Parameters for background mesh
 */
struct MeshDef {
  ZEROPTV channel_min;  ///< start of domain
  ZEROPTV channel_max;  ///< end of domain

  int refine_l = 0;
  int refine_h = 0;
  ///< whether or not fx_refine refines at channel_min/channel_max
  bool refine_walls = false;

  /// these are automatically detected from above parameters
  ///< if channel_min to channel_max is square and channel_min is 0,0,0, false, otherwise true
  bool enable_subda = true;
  ///< scale for main domain, always the same on all axes to prevent skewed elements
  double daScalingFactor[3] = {0.0, 0.0, 0.0};

  /**
   * read channel mesh from config
   * @param root
   */
  void read_from_config(const libconfig::Setting &root) {
    refine_l = (int) root["refine_l"];
    refine_h = (int) root["refine_h"];
    refine_walls = (bool) root["refine_walls"];

    PrintStatus("refine_l: ", refine_l);
    PrintStatus("refine_h: ", refine_h);

    channel_min = ZEROPTV((double) root["min"][0], (double) root["min"][1], (double) root["min"][2]);
    channel_max = ZEROPTV((double) root["max"][0], (double) root["max"][1], (double) root["max"][2]);

    if (!root.lookupValue("enable_subda", enable_subda)) {
      /// enable_subda setting not supplied - auto detect
      bool origin_at_zero = (channel_min[0] == 0 && channel_min[1] == 0 && channel_min[2] == 0);
      /// bool is_perfect_square = (channel_max[0] == channel_max[1] && channel_max[0] == channel_max[2]);
      double aspect_ratio_xy = (channel_max[0] - channel_min[0]) / (channel_max[1] - channel_min[1]);
      double aspect_ratio_xz = (channel_max[0] - channel_min[0]) / (channel_max[2] - channel_min[2]);

      if (!origin_at_zero) {
        PrintInfo("Mesh origin is not at zero - enabling subDA");
        enable_subda = true;
      } else if (fabs(aspect_ratio_xy - 1.0) > 1e-3) {
        PrintWarning("X/Y aspect ratio is ", aspect_ratio_xy, " - enabling subDA to prevent skewed elements");
        enable_subda = true;
      } else if (fabs(aspect_ratio_xz - 1.0) > 1e-3) {
        PrintWarning("X/Z aspect ratio is ", aspect_ratio_xz, " - enabling subDA to prevent skewed elements");
        enable_subda = true;
      } else {
        PrintInfo("Mesh is nearly a perfect square. Using normal DA.");
        enable_subda = false;
      }
    }
    PrintStatus("enable_subDA: ", enable_subda);
    correctChannelSize();
    PrintStatus("Corrected channel_min: ", channel_min);
    PrintStatus("Corrected channel_max: ", channel_max);
    if ((channel_min[0] != 0 || channel_min[1] != 0 || channel_min[2] != 0) && !enable_subda) {
      throw TALYFEMLIB::TALYException() << "channel_min is not zero, but subDA is disabled!";
    }

    if (enable_subda) {
      double max_length = std::max(std::max(channel_max[0], channel_max[1]), channel_max[2]);
      daScalingFactor[0] = daScalingFactor[1] = daScalingFactor[2] = max_length;
    } else {
      daScalingFactor[0] = channel_max[0];
      daScalingFactor[1] = channel_max[1];
      daScalingFactor[2] = channel_max[2];
    }
    PrintStatus("daScalingFactor: ", daScalingFactor[0], ", ", daScalingFactor[1], ", ", daScalingFactor[2]);

  }

  /// Calculate the real channel_min and channel_max based on the refinement level on the walls
  void correctChannelSize() {
    double eps = 1e-12;
    if (enable_subda) {
      double max_length = std::max(std::max(channel_max[0], channel_max[1]), channel_max[2]);
//      int refine_lvl_at_wall = refine_walls ? refine_h : refine_l;
      // retain whole element of the coarsest level to avoid hanging nodes on the boundary.
      int refine_lvl_at_wall = refine_l;
      double element_size_at_wall = max_length / pow(2, refine_lvl_at_wall);
      for (int dim = 0; dim < 3; dim++) {
        bool exact_cut_on_max = fmod(channel_max(dim), element_size_at_wall) < eps;
        bool exact_cut_on_min = fmod(channel_min(dim), element_size_at_wall) < eps;
        channel_max(dim) = exact_cut_on_max ? channel_max(dim) : int(channel_max(dim) / element_size_at_wall + 1)
            * element_size_at_wall;
        channel_min(dim) =
            exact_cut_on_min ? channel_min(dim) : int(channel_min(dim) / element_size_at_wall) * element_size_at_wall;
      }
    }
  }

  /// Return the maximum length of the channel
  double maxChannelLength() {
    ZEROPTV channelSize = channel_max - channel_min;
    double max_length = std::max(std::max(channelSize[0], channelSize[1]), channelSize[2]);
    return max_length;
  }

};

/**
 * Parameters for boundary condition
 */
struct BoundaryDef {
  enum Side {
    INVALID = -1,
    X_MINUS = 0,
    X_PLUS = 1,
    Y_MINUS = 2,
    Y_PLUS = 3,
    Z_MINUS = 4,
    Z_PLUS = 5,
  };
  enum Vel_Type {
    NO_SLIP = 0,
    NO_GRADIENT_VEL = 1,
    DIRICHLET_VEL = 2,
  };
  enum Pressure_Type {
    NO_GRADIENT_P = 0,
    DIRICHLET_P = 1,
  };
  enum Temperature_Type {
    NO_GRADIENT_T = 0,
    DIRICHLET_T = 1,
    NEUMANN_T = 2,
    ROBIN_T = 3,
    WEAK = 4,
  };

  Side side = INVALID;
  Vel_Type vel_type = NO_GRADIENT_VEL;
  Pressure_Type pressure_type = NO_GRADIENT_P;
  Temperature_Type temperature_type = NO_GRADIENT_T;
  std::vector<ZEROPTV> wall_velocity;
  double pressure = 0.0;
  /// for Dirichlet BC
  std::vector<double> temperature;
  /// for Neumann BC
  std::vector<double> flux;
  /// for robin BC
  std::vector<double> G_vec;
  std::vector<double> a_vec;
  /// for Transient BC
  std::vector<double> ramping;
  bool ifRefine = false;

  Side read_side_from_config(const libconfig::Setting &root) {
    return str_to_side(root["side"]);
  }

  void read_from_config(const libconfig::Setting &root) {
    if (root.exists("ifRefine")) {
      ifRefine = (bool) root["ifRefine"];
    }
    if (root.exists("vel_type")) {
      vel_type = read_vel_type(root["vel_type"]);
    }
    if (root.exists("pressure_type")) {
      pressure_type = read_pressure_type(root["pressure_type"]);
    }
    if (root.exists("temperature_type")) {
      temperature_type = read_temperature_type(root["temperature_type"]);
    }
    if (root.exists("ramping_vec")) {
      const libconfig::Setting &ramping_time_vec = root["ramping_vec"];
      for (int i = 0; i < ramping_time_vec.getLength(); ++i) {
        ramping.push_back(ramping_time_vec[i]);
      }
    } else {
      ramping.push_back(0.0);
    }

    if (vel_type == Vel_Type::DIRICHLET_VEL) {
      const libconfig::Setting &wall_vel_vec = root["wall_vel"];
      for (int i = 0; i < wall_vel_vec.getLength() / 3; ++i) {
        ZEROPTV temp_vel = ZEROPTV((double) root["wall_vel"][3 * i],
                                   (double) root["wall_vel"][3 * i + 1],
                                   (double) root["wall_vel"][3 * i + 2]);
        wall_velocity.push_back(temp_vel);
      }
      if (wall_velocity.size() != ramping.size()) {
        throw TALYFEMLIB::TALYException() << "Wall_vel vector not matching with ramping";
      }
    }

    if (pressure_type == Pressure_Type::DIRICHLET_P) {
      pressure = (double) root["pressure"];
    }

    if ((temperature_type == Temperature_Type::DIRICHLET_T) || (temperature_type == Temperature_Type::WEAK)) {
      const libconfig::Setting &temperature_vec = root["temperature"];
      if (temperature_vec.isNumber()) {
        temperature.push_back(temperature_vec);
      } else {
        for (int i = 0; i < temperature_vec.getLength(); ++i) {
          temperature.push_back(temperature_vec[i]);
        }
      }
      if (temperature.size() != ramping.size()) {
        throw TALYFEMLIB::TALYException() << "Temperature vector not matching with ramping";
      }
    }

    if (temperature_type == Temperature_Type::NEUMANN_T) {
      const libconfig::Setting &flux_vec = root["flux"];
      if (flux_vec.isNumber()) {
        flux.push_back(flux_vec);
      } else {
        for (int i = 0; i < flux_vec.getLength(); ++i) {
          flux.push_back(flux_vec[i]);
        }
      }
      if (flux.size() != ramping.size()) {
        throw TALYFEMLIB::TALYException() << "Flux vector not matching with ramping";
      }
    }

    if (temperature_type == Temperature_Type::ROBIN_T) {
      /// b * \frac{\partial u}{\partial n} = g - a * u
      const libconfig::Setting &G_vec_config = root["G_constant"];
      if (G_vec_config.isNumber()) {
        G_vec.push_back(G_vec_config);
      } else {
        for (int i = 0; i < G_vec_config.getLength(); ++i) {
          G_vec.push_back(G_vec_config[i]);
        }
      }
      if (G_vec.size() != ramping.size()) {
        throw TALYFEMLIB::TALYException() << "G_constant vector not matching with ramping";
      }

      const libconfig::Setting &a_vec_config = root["a_constant"];
      if (a_vec_config.isNumber()) {
        a_vec.push_back(a_vec_config);
      } else {
        for (int i = 0; i < a_vec_config.getLength(); ++i) {
          a_vec.push_back(a_vec_config[i]);
        }
      }
      if (a_vec.size() != ramping.size()) {
        throw TALYFEMLIB::TALYException() << "a_constant vector not matching with ramping";
      }

    }

  }
  template<class T>
  T getRamping(const double time, const std::vector<T> &v) const {
    unsigned int interval = ramping.size();
    for (unsigned int i = 0; i < ramping.size(); i++) {
      if (time < ramping[i]) {
        interval = i;
        break;
      }
    }
    T current_variable;
    if (interval == 0) {
      current_variable = v[0] * (time / ramping[0]);
    } else if (interval == ramping.size()) {
      current_variable = v[interval - 1];
    } else {
      double per1 = (time - ramping[interval - 1]) / (ramping[interval] - ramping[interval - 1]);
      double per2 = (ramping[interval] - time) / (ramping[interval] - ramping[interval - 1]);
      current_variable = v[interval] * per1 + v[interval - 1] * per2;
    }
    return current_variable;
  }

 protected:
  Side str_to_side(const std::string &str) const {
    if (str == "x-") {
      return Side::X_MINUS;
    } else if (str == "x+") {
      return Side::X_PLUS;
    } else if (str == "y-") {
      return Side::Y_MINUS;
    } else if (str == "y+") {
      return Side::Y_PLUS;
    } else if (str == "z-") {
      return Side::Z_MINUS;
    } else if (str == "z+") {
      return Side::Z_PLUS;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid BC side";
  }

  Vel_Type read_vel_type(const std::string &str) const {
    if (str == "no_slip") {
      return Vel_Type::NO_SLIP;
    } else if (str == "no_gradient") {
      return Vel_Type::NO_GRADIENT_VEL;
    } else if (str == "dirichlet") {
      return Vel_Type::DIRICHLET_VEL;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid BC type for vel";
  }

  Pressure_Type read_pressure_type(const std::string &str) const {
    if (str == "no_gradient") {
      return Pressure_Type::NO_GRADIENT_P;
    } else if (str == "dirichlet") {
      return Pressure_Type::DIRICHLET_P;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid BC type for pressure";
    }
  }

  Temperature_Type read_temperature_type(const std::string &str) const {
    if (str == "no_gradient") {
      return Temperature_Type::NO_GRADIENT_T;
    } else if (str == "dirichlet") {
      return Temperature_Type::DIRICHLET_T;
    } else if (str == "neumann") {
      return Temperature_Type::NEUMANN_T;
    } else if (str == "robin") {
      return Temperature_Type::ROBIN_T;
    } else if (str == "weak") {
      return Temperature_Type::WEAK;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid BC type for temperature";
    }
  }
};

/**
 * Parameters for initial condition
 */
struct InitialConditionDef {
  enum Vel_IC {
    ZERO_VEL = 0,
    USER_DEFINED_VEL = 1,
  };
  enum Pressure_IC {
    ZERO_P = 0,
    USER_DEFINED_P = 1,
  };
  enum T_IC {
    ZERO_T = 0,
    USER_DEFINED_T = 1
  };

  Vel_IC vel_ic_type = ZERO_VEL;
  Pressure_IC pressure_ic_type = ZERO_P;
  T_IC t_ic_type = ZERO_T;
  ZEROPTV vel_ic;
  double p_ic = 0.0;
  double t_ic = 0.0;

  void read_from_config(const libconfig::Setting &root) {
    if (root.exists("vel_ic_type")) {
      vel_ic_type = read_vel_ic(root["vel_ic_type"]);
    }
    if (root.exists("p_ic_type")) {
      pressure_ic_type = read_pressure_ic(root["p_ic_type"]);
    }
    if (root.exists("t_ic_type")) {
      t_ic_type = read_temperature_ic(root["t_ic_type"]);
    }
    if (vel_ic_type == Vel_IC::USER_DEFINED_VEL) {
      vel_ic = ZEROPTV((double) root["vel_ic"][0], (double) root["vel_ic"][1], (double) root["vel_ic"][2]);
    }
    if (pressure_ic_type == Pressure_IC::USER_DEFINED_P) {
      p_ic = (double) root["p_ic"];
    }
    if (t_ic_type == T_IC::USER_DEFINED_T) {
      t_ic = (double) root["t_ic"];
    }
  }

 protected:
  Vel_IC read_vel_ic(const std::string &str) const {
    if (str == "zero") {
      return Vel_IC::ZERO_VEL;
    } else if (str == "user_defined") {
      return Vel_IC::USER_DEFINED_VEL;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid IC type for vel";
  }
  Pressure_IC read_pressure_ic(const std::string &str) const {
    if (str == "zero") {
      return Pressure_IC::ZERO_P;
    } else if (str == "user_defined") {
      return Pressure_IC::USER_DEFINED_P;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid IC type for pressure";
    }
  }

  T_IC read_temperature_ic(const std::string &str) const {
    if (str == "zero") {
      return T_IC::ZERO_T;
    } else if (str == "user_defined") {
      return T_IC::USER_DEFINED_T;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid IC type for temperature";
    }
  }
};

/**
 * Parameters for Solver Options
 */
struct SolverOptions {
  std::map<std::string, std::string> vals;

  /**
   * Applies val to a PetscOptions database, i.e. adds "[prefix][key] [value]"
   * to a PETSc options database
   * (i.e. the command line).
   * Note that prefix MUST START WITH A -. (PETSc automatically strips the
   * first character...)
   * If you do not call this function, SolverOptions basically does nothing.
   * @param prefix command line prefix, must start with a hyphen (-)
   * @param opts PETSc options database, use NULL for the global options
   * database
   */
  inline void apply_to_petsc_options(const std::string &prefix = "-",
                                     PetscOptions opts = nullptr) {
    for (auto it = vals.begin(); it != vals.end(); it++) {
      const std::string name = prefix + it->first;

      // warn if the option was also specified as a command line option
      PetscBool exists = PETSC_FALSE;
      PetscOptionsHasName(opts, nullptr, name.c_str(), &exists);
      if (exists)
        PrintWarning("Overriding command-line option '", name,
                     "' with config file value '", it->second, "'.");

      PetscOptionsSetValue(opts, name.c_str(), it->second.c_str());
    }
  }
};

/**
 * Parameters for Body force
 */
struct BodyForceDef {
  double rho_fluid = 1.0;
  double g_star = 9.8;
  ZEROPTV G_dir = {0.0, 0.0, -1.0};
  void read_from_config(const libconfig::Setting &root) {
    rho_fluid = (double) root["rho_fluid"];
    g_star = (double) root["g_star"];
    G_dir = ZEROPTV((double) root["G_dir"][0], (double) root["G_dir"][1], (double) root["G_dir"][2]);
    if (fabs(G_dir.norm() - 1) > 1e-6) {
      PrintWarning("G_dir for body force is not normalized:", G_dir);
      G_dir.Normalize();
      PrintWarning("G_dir Normalized to:", G_dir);
    }
  }

  /// net body force for solid (include gravity, buoyancy and other terms)
  ZEROPTV calcBodyForce(double rho_solid, double volume_solid) {
    ZEROPTV buoyancy = G_dir * ((rho_solid - rho_fluid) * volume_solid * g_star);
    return buoyancy;
  }
};

/**
 * Parameters for Regional refine
 */
struct RegionalRefine {
  enum Type {
    INVALID = -1,
    CUBE = 0,
    SPHERE = 1,
    CYLINDER = 2,
  };
  // max refinement level
  int refine_region_lvl = 0;
  void read_from_config(const libconfig::Setting &root) {
    refine_type = read_type(root["type"]);
    refine_region_lvl = (int) root["refine_region_lvl"];
    if (refine_type == CUBE) {
      min_corner = ZEROPTV((double) root["min_c"][0], (double) root["min_c"][1], (double) root["min_c"][2]);
      max_corner = ZEROPTV((double) root["max_c"][0], (double) root["max_c"][1], (double) root["max_c"][2]);
    }
    if (refine_type == SPHERE) {
      center_sphere = ZEROPTV((double) root["center"][0], (double) root["center"][1], (double) root["center"][2]);
      radius_sphere = (double) root["radius"];
      if (root.exists("radius_in")) {
        radius_sphere_in = (double) root["radius_in"];
      }

    }
    if (refine_type == CYLINDER) {
      min_corner = ZEROPTV((double) root["c1"][0], (double) root["c1"][1], (double) root["c1"][2]);
      max_corner = ZEROPTV((double) root["c2"][0], (double) root["c2"][1], (double) root["c2"][2]);
      radius_cylinder = (double) root["radius"];
      if (root.exists("radius_in")) {
        radius_cylinder_in = (double) root["radius_in"];
      }
    }
  }

  /**
   * @return false for outside region, true for inside region
   */
  bool in_region(ZEROPTV p) {
    switch (refine_type) {
      case CUBE: {
        if (p.x() < min_corner.x() ||
            p.y() < min_corner.y() ||
            p.z() < min_corner.z() ||
            p.x() > max_corner.x() ||
            p.y() > max_corner.y() ||
            p.z() > max_corner.z()) {
          return false;
        } else {
          return true;
        }
      }
        break;
      case SPHERE: {
        double distance = (p - center_sphere).norm();
        return (distance < radius_sphere and distance > radius_sphere_in);
      }
        break;
      case CYLINDER: {
        ZEROPTV AB = min_corner - max_corner;
        ZEROPTV temp = AB * (1.0 / AB.innerProduct(AB));
        ZEROPTV AP = p - max_corner;
        ZEROPTV proj_point = max_corner + temp * AP.innerProduct(AB);
        double distance_s = (proj_point - p).innerProduct(proj_point - p);
        if (distance_s < radius_cylinder * radius_cylinder and distance_s > radius_cylinder_in * radius_cylinder_in) {
          double max_x = std::max(max_corner.x(), min_corner.x());
          double max_y = std::max(max_corner.y(), min_corner.y());
          double max_z = std::max(max_corner.z(), min_corner.z());
          double min_x = std::min(max_corner.x(), min_corner.x());
          double min_y = std::min(max_corner.y(), min_corner.y());
          double min_z = std::min(max_corner.z(), min_corner.z());
          if (proj_point.x() > max_x ||
              proj_point.y() > max_y ||
              proj_point.z() > max_z ||
              proj_point.x() < min_x ||
              proj_point.y() < min_y ||
              proj_point.z() < min_z) {
            return false;
          } else {
            return true;
          }
        } else {
          return false;
        }
      }
      default:throw TALYFEMLIB::TALYException() << "Wrong type!";
    }
  }

 protected:
  Type refine_type = INVALID;
  // for sphere
  ZEROPTV center_sphere;
  double radius_sphere = 0.0;
  double radius_sphere_in = 0.0;
  // for cube
  ZEROPTV min_corner;
  ZEROPTV max_corner;
  // for cylinder
  double radius_cylinder = 0.0;
  double radius_cylinder_in = 0.0;
  Type read_type(const std::string &str) const {
    if (str == "sphere") {
      return SPHERE;
    } else if (str == "cube") {
      return CUBE;
    } else if (str == "cylinder") {
      return CYLINDER;
    } else
      throw TALYFEMLIB::TALYException() << "Invalid Regional refinement type";
  }
};

struct NSHTInputData : public TALYFEMLIB::InputData {
  bool SolveHT = true;
  bool SolveNS = true;

  double ratio = 1.0;

  bool resetVecs = false;
  /// solver (STABILIZED_NS) and Variational multiscale based solver (RBVMS_NS)
  enum typeNSsolver { STABILIZED_NS = 0, RBVMS_NS = 1 };
  enum typeHTsolver { STABILIZED_HT = 0, RBVMS_HT = 1 };
  enum typeNondimension { FREECONV = 0, MIXCONV = 1 };
  /// Declare the Solver type for NS
  typeNSsolver solverTypeNS;
  /// Declare the Solver type for HT
  typeHTsolver solverTypeHT;

  double tauC_scale;
  /// Time stepper
  std::vector<double> dt;
  std::vector<double> totalT;
  double OutputStartTime;
  unsigned int grainSz;
  int OutputInterval;

  bool dirichletOnInternalNodes = false;

  /// Gauss Point Relative Order, 0 for no change, 1 ~ 10 for relative order, 32 for splitting
  unsigned int gp_handle = 0;
  unsigned int gp_rel_order = 0;
  unsigned int gp_max_splitting = 0;

  /// surface cost (for assembly)
  unsigned int surface_cost;

  /// Direction of gravity
  unsigned int G_dir;

  /// weakBC parameters
  double Cb_f;
  double Cb_e;
  double orderOfCb = 1.0;

  /// VMS parameters
  double Ci_f;
  double Ci_e;
  double tauM_scale;
  double tauM_scale4Side;
  double thetaTimeStepping = 1; /// Default is backward Euler.
  double thetaTimeStepping4Side = 1; /// Default is backward Euler.

  bool iftimeStab = true;
  /// Non-dimensional parameter
  typeNondimension nondimType;
  /// for parameter that changes with time
  std::vector<double> ramping;
  std::vector<double> Ra;
  std::vector<double> Pr;
  std::vector<double> Re;
  std::vector<double> Pe;
  std::vector<double> Gr;

  /// tolerance for the block iterations
  double blockTolerance;
  /// maximum number of iterations for block iteration
  unsigned int iterMaxBlock;

  /// postprocessing (surface indicator to monitor)
  std::vector<unsigned int> SurfaceMonitor;

  std::vector<GeomDef> geom_defs;
  MeshDef mesh_def;
  std::vector<RegionalRefine> region_refine;
  BodyForceDef bodyforce_def;

  std::vector<BoundaryDef> boundary_def;
  InitialConditionDef ic_def;

  /// Solver options for PETSc are handled in these structures
  SolverOptions solverOptionsNS;
  SolverOptions solverOptionsHT;

  /// Debug options
  bool Debug_Integrand = false;

  NSHTInputData()
      : InputData(),
        surface_cost(1000),
        OutputStartTime(0),
        OutputInterval(1) {}

  ~NSHTInputData() = default;

  static SolverOptions read_solver_options(libconfig::Config &cfg,
                                           const char *name,
                                           bool required = true) {
    if (!cfg.exists(name)) {
      if (!required) {
        PrintInfo(name, " not found (no additional options in config file)");
        return SolverOptions();
      } else {
        throw TALYFEMLIB::TALYException() << "Missing required solver options '" << name
                                          << "'";
      }
    }

    SolverOptions opts;
    const auto &setting = cfg.getRoot()[name];

    for (int i = 0; i != setting.getLength(); ++i) {
      std::string val;
      switch (setting[i].getType()) {
        case libconfig::Setting::TypeInt:
        case libconfig::Setting::TypeInt64:val = std::to_string((long) (setting[i]));
          break;
        case libconfig::Setting::TypeFloat: {
          std::stringstream ss;
          ss << std::setprecision(16) << ((double) setting[i]);
          val = ss.str();
          break;
        }
        case libconfig::Setting::TypeBoolean:val = ((bool) setting[i]) ? "1" : "0";
          break;
        case libconfig::Setting::TypeString:val = (const char *) setting[i];
          break;
        default:
          throw TALYFEMLIB::TALYException() << "Invalid solver option value type ("
                                            << setting[i].getPath() << ") "
                                            << "(type ID " << setting[i].getType() << ")";
      }

      PrintInfo(name, ".", setting[i].getName(), " = ", val);
      opts.vals[setting[i].getName()] = val;
    }
    return opts;
  }

  /// read configure from file
  bool ReadFromFile(const std::string &filename = std::string("config.txt")) {
    /// fill cfg, but don't initialize default fields
    ReadConfigFile(filename);
    nsd = 3;
    ifDD = true;

    if (ReadValue("SolveHT", SolveHT)) {}
    if (ReadValue("SolveNS", SolveNS)) {}
    if (ReadValue("ifTimeStab",iftimeStab)){}
    if (ReadValue("internalDirichlet",dirichletOnInternalNodes)){};
    if (ReadValue("resetVecs",resetVecs)){};
    if (ReadValue("grainSz",grainSz)){};
    if (ReadValue("RatioWeight",ratio)){}
    ReadValueRequired("tauCScale",tauC_scale);
    PrintStatus("Timestab:", iftimeStab);
    mesh_def.read_from_config(cfg.getRoot()["background_mesh"]);
    if (cfg.exists("region_refine")) {
      const auto &cfg_refine = cfg.getRoot()["region_refine"];
      region_refine.resize(cfg_refine.getLength());
      for (unsigned int i = 0; i < region_refine.size(); i++) {
        region_refine[i].read_from_config(cfg_refine[i]);
      }
    }

    /// timestep control
    {
      if (cfg.exists("dt_V")) {
        const libconfig::Setting &dt_ = cfg.getRoot()["dt_V"];
        for (int i = 0; i < dt_.getLength(); ++i) {
          dt.push_back(dt_[i]);
        }
      } else {
        double dt_const;
        ReadValueRequired("dt", dt_const);
        dt.push_back(dt_const);
      }

      if (cfg.exists("totalT_V")) {
        const libconfig::Setting &totalT_ = cfg.getRoot()["totalT_V"];
        for (int i = 0; i < totalT_.getLength(); ++i) {
          totalT.push_back(totalT_[i]);
        }
      } else {
        double totalT_const;
        ReadValueRequired("totalT", totalT_const);
        totalT.push_back(totalT_const);
      }
    }

    /// Gauss Point Relative Order
    ReadValueRequired("gp_handle", gp_handle);
    if (gp_handle == 0) {
      gp_handle = VariableGP::NoChange;
    } else if (gp_handle < 10) {
      gp_handle = VariableGP::IncreasedRelativeOrder;
      ReadValueRequired("gp_rel_order", gp_rel_order);
    } else if (gp_handle == 32) {
      gp_handle = VariableGP::RefineBySplitting;
      ReadValueRequired("gp_max_splitting", gp_max_splitting);
    } else {
      throw TALYFEMLIB::TALYException() << "Wrong gp handle method!";
    }

    /// surface assembly cost
    if (ReadValue("surface_cost", surface_cost)) {}

    /// Block Iteration control
    ReadValueRequired("blockTolerance", blockTolerance);
    ReadValueRequired("iterMaxBlock", iterMaxBlock);

    /// Output control
    if (ReadValue("OutputStartTime", OutputStartTime)) {}
    if (ReadValue("OutputInterval", OutputInterval)) {}

    /// Solver selection
    /// Read type of NS solver
    solverTypeNS = read_NSsolver(cfg.getRoot(), "NavierStokesSolverType");
    /// Read type of HT solver
    solverTypeHT = read_HTsolver(cfg.getRoot(), "HeatSolverType");



    /// VMS and weakBC parameters
    if (solverTypeNS == typeNSsolver::RBVMS_NS || solverTypeHT == typeHTsolver::RBVMS_HT) {
      ReadValueRequired("Ci_f", Ci_f);
      ReadValueRequired("Ci_e", Ci_e);
      ReadValueRequired("tauM_scale", tauM_scale);
      ReadValueRequired("tauM_scale4Side", tauM_scale4Side);
    }

    /// WeakBC parameters
    ReadValueRequired("Cb_f", Cb_f);
    ReadValueRequired("Cb_e", Cb_e);
    if (ReadValue("orderOfCb", orderOfCb)) {}
    if (ReadValue("thetaTimeStepping", thetaTimeStepping)) {}
    if (ReadValue("thetaTimeStepping4Side", thetaTimeStepping4Side)) {}
    PrintStatus("theta for Time Stepping ", thetaTimeStepping);
    /// gravity direction
    ReadValueRequired("G_dir", G_dir);

    /// Non-dimensional parameters
    /// Read type of Non-dimensional
    nondimType = read_NondimensionType(cfg.getRoot(), "NondimensionType");

    if (cfg.exists("ramping_parameter")) {
      const libconfig::Setting &ramping_time_vec = cfg.getRoot()["ramping_parameter"];
      for (int i = 0; i < ramping_time_vec.getLength(); ++i) {
        ramping.push_back(ramping_time_vec[i]);
      }
    } else {
      ramping.push_back(0.0);
    }

    {
      if (nondimType == FREECONV) {
        if (cfg.exists("Ra_V")) {
          const libconfig::Setting &Ra_ = cfg.getRoot()["Ra_V"];
          for (int i = 0; i < Ra_.getLength(); ++i) {
            Ra.push_back(Ra_[i]);
          }
        } else {
          double Ra_const;
          ReadValueRequired("Ra", Ra_const);
          Ra.push_back(Ra_const);
        }

        if (cfg.exists("Pr_V")) {
          const libconfig::Setting &Pr_ = cfg.getRoot()["Pr_V"];
          for (int i = 0; i < Pr_.getLength(); ++i) {
            Pr.push_back(Pr_[i]);
          }
        } else {
          double Pr_const;
          ReadValueRequired("Pr", Pr_const);
          Pr.push_back(Pr_const);
        }
      } else if (nondimType == MIXCONV) {
        if (cfg.exists("Re_V")) {
          const libconfig::Setting &Re_ = cfg.getRoot()["Re_V"];
          for (int i = 0; i < Re_.getLength(); ++i) {
            Re.push_back(Re_[i]);
          }
        } else {
          double Re_const;
          ReadValueRequired("Re", Re_const);
          Re.push_back(Re_const);
        }

        if (cfg.exists("Pe_V")) {
          const libconfig::Setting &Pe_ = cfg.getRoot()["Pe_V"];
          for (int i = 0; i < Pe_.getLength(); ++i) {
            Pe.push_back(Pe_[i]);
          }
        } else {
          double Pe_const;
          ReadValueRequired("Pe", Pe_const);
          Pe.push_back(Pe_const);
        }
        if (cfg.exists("Gr_V")) {
          const libconfig::Setting &Gr_ = cfg.getRoot()["Gr_V"];
          for (int i = 0; i < Gr_.getLength(); ++i) {
            Gr.push_back(Gr_[i]);
          }
        } else {
          double Gr_const;
          ReadValueRequired("Gr", Gr_const);
          Gr.push_back(Gr_const);
        }
      }
    }

    /// read geometries
    if (cfg.exists("geometries")) {
      const auto &geometries = cfg.getRoot()["geometries"];
      geom_defs.resize(geometries.getLength());
      for (unsigned int i = 0; i < geom_defs.size(); i++) {
        geom_defs[i].read_from_config(geometries[i]);
      }
    }
    /// read environment for moving IBM
    bool any_buoyancy = false;
    for (const auto &g : geom_defs) {
      any_buoyancy = any_buoyancy or g.if_buoyancy;
    }
    if (any_buoyancy) {
      bodyforce_def.read_from_config(cfg.getRoot()["bodyforce"]);
    }

    /// always have 6 boundary_def in the order of x-, x+, y-, y+, z-, z+
    boundary_def.resize(6);
    boundary_def[0].side = BoundaryDef::Side::X_MINUS;
    boundary_def[1].side = BoundaryDef::Side::X_PLUS;
    boundary_def[2].side = BoundaryDef::Side::Y_MINUS;
    boundary_def[3].side = BoundaryDef::Side::Y_PLUS;
    boundary_def[4].side = BoundaryDef::Side::Z_MINUS;
    boundary_def[5].side = BoundaryDef::Side::Z_PLUS;
    if (cfg.exists("boundary")) {
      const auto &bc = cfg.getRoot()["boundary"];
      for (unsigned int i = 0; i < boundary_def.size(); i++) {
        for (int j = 0; j < bc.getLength(); j++) {
          /// If the side of bc in config matches with the preset bc
          if (boundary_def[i].side == boundary_def[i].read_side_from_config(bc[j])) {
            boundary_def[i].read_from_config(bc[j]);
          }
        }
      }
    }

    /// read Initial condition
    if (cfg.exists("initial_condition")) {
      const auto &ic = cfg.getRoot()["initial_condition"];
      ic_def.read_from_config(ic);
    }

    /// Solver Options
    /// Simulation parameters and tolerances
    solverOptionsHT = read_solver_options(cfg, "solver_options_ht");
    solverOptionsNS = read_solver_options(cfg, "solver_options_ns");

    /// Debug options
    if (ReadValue("Debug_Integrand", Debug_Integrand)) {}

    /// Load surface postprocessing vector, or Nu calculation
    if (cfg.exists("SurfaceMonitor")) {
      const libconfig::Setting &settings = cfg.getRoot()["SurfaceMonitor"];
      for (int i = 0; i < settings.getLength(); ++i) {
        SurfaceMonitor.push_back(settings[i]);
      }
    }

    return true;
  }

  /// check if the input are valid
  bool CheckInputData() {
    if ((typeOfIC == 0) && (inputFilenameGridField.empty())) {
      PrintWarning("IC not set properly check!", typeOfIC, " ",
                   inputFilenameGridField);
      return false;
    }

    /// check timestep
    if (dt.size() != totalT.size()) {
      PrintWarning("Mismatch in timestep control, check dt and totalT!");
      return false;
    }
    if (!OutputInterval) {
      PrintWarning("OutputInterval = 0, error!");
      return false;
    }
    /// checkout bc for all six faces
    if (boundary_def.size() != 6) {
      PrintWarning("BC side have wrong number:", boundary_def.size());
      return false;
    }

    /// Calculate non-dimensional variables for RB DEMO case
    if (nondimType == FREECONV and getParameter(0, Ra) < 0) {
      if (nsd != 3) {
        PrintWarning("Wrong dimension!");
        return false;
      }
      double g = 9.8;
      double beta = 0.0034;
      ZEROPTV Lc = mesh_def.channel_max - mesh_def.channel_min;
      double nu = 1.53e-5;
      // bc for z- and z+
      const auto bcZminus = boundary_def[BoundaryDef::Z_MINUS];
      const auto bcZplus = boundary_def[BoundaryDef::Z_PLUS];

      double Gr_calc =
          g * beta * (bcZminus.getRamping(0, bcZminus.temperature) - bcZplus.getRamping(0, bcZplus.temperature))
              * pow(Lc.z(), 3.0) / (nu * nu);
      Ra.back() = Gr_calc * getParameter(0, Pr);
      /// Define Re_L
      double Re_L = sqrt(getParameter(0, Pr) * getParameter(0, Ra));
      /// Define eta
      double eta = pow(Re_L, -3.0 / 4.0) * Lc.z();
      /// Define Taylor scale
      double lambda_g = sqrt(10.0) * pow(eta, 2.0 / 3.0) * pow(Lc.z(), 1.0 / 3.0);

      PrintInfo("Ra number is  ", getParameter(0, Ra));
      PrintInfo("Fully resolved length scale in y direction: ", eta);
      PrintInfo("Fully resolved mesh size (assume mesh is uniform): ", Lc * (1 / eta),
                ": ",
                Lc.x() / eta * Lc.y() / eta * Lc.z() / eta);
      PrintInfo("Cut off length scale in z direction: ", lambda_g);
      PrintInfo("Total Mesh count in x-y-z direction should be (with a 0.5 cut-off scale): ",
                Lc * (1 / (0.5 * lambda_g)));
      PrintInfo("Current Mesh count in x-y-z are:", pow(2, mesh_def.refine_l));
      return true;
    }

    return true;
  }

  /**
   * get parameters that changes with time
   * @tparam T
   * @param time
   * @param v
   * @return
   */
  template<class T>
  T getParameter(const double time, const std::vector<T> &v) const {
    unsigned int interval = ramping.size() - 1;
    assert(fabs(ramping[0]) < 1e-6);
    assert(v.size() == ramping.size());
    for (unsigned int i = 0; i < ramping.size(); i++) {
      if (time < ramping[i]) {
        interval = i - 1;
        break;
      }
    }
    T current_variable = v[interval];
    return current_variable;
  }

  /**
   * Get coefficient of diffusion
   * @tparam T
   * @param time
   * @param v
   * @return
   */
  double getDiffusionHT(const double time) const {
    double Coe_diff = 0.0;
    if (nondimType == NSHTInputData::FREECONV) {
      Coe_diff = 1.0 / sqrt(getParameter(time, Pr) * getParameter(time, Ra));
    } else if (nondimType == NSHTInputData::MIXCONV) {
      Coe_diff = 1.0 / (getParameter(time, Pe));
    }
    return Coe_diff;
  }

  /**
   * Get coefficient of diffusion
   * @tparam T
   * @param time
   * @param v
   * @return
   */
  double getDiffusionNS(const double time) const {
    double Coe_diff = 0.0;
    if (nondimType == NSHTInputData::FREECONV) {
      Coe_diff = sqrt(getParameter(time, Pr) / getParameter(time, Ra));
    } else if (nondimType == NSHTInputData::MIXCONV) {
      Coe_diff = 1.0 / (getParameter(time, Re));
    }
    return Coe_diff;
  }

  /**
   * Get coefficient of body force
   * @tparam T
   * @param time
   * @param v
   * @return
   */
  double getBodyForce(const double time) const {
    double Coe_bdyforce = 0.0;
    if (nondimType == NSHTInputData::FREECONV) {
      Coe_bdyforce = 1.0;
    } else if (nondimType == NSHTInputData::MIXCONV) {
      Coe_bdyforce = getParameter(time, Gr) / pow(getParameter(time, Re), 2);
    }
    return Coe_bdyforce;
  }

 private:
  /// Function for reading case dependent parameters
  template<typename T>
  void ReadValueRequired(const std::string &name, T &out) {
    if (!ReadValue(name, out))
      throw TALYFEMLIB::TALYException() << "Required parameter missing: " << name;
  }

  /// Function for reading type of Navier Stokes solver
  static typeNSsolver read_NSsolver(libconfig::Setting &root,
                                    const char *name) {
    std::string str;
    /// If nothing specified stays stabilizedNS
    if (root.lookupValue(name, str)) {
      if (str == "stabilizedNS") {
        return STABILIZED_NS;
      } else if (str == "rbvmsNS") {
        return RBVMS_NS;
      } else {
        throw TALYFEMLIB::TALYException() << "Unknown solver name for NS: " << name << str;
      }
    } else {
      throw TALYFEMLIB::TALYException() << "Must specify NavierStokesSolverType: stabilizedNS or rbvmsNS";
    }

  }

  /// Function for reading type of Heat equation solver
  static typeHTsolver read_HTsolver(libconfig::Setting &root,
                                    const char *name) {
    std::string str;
    /// If nothing specified stays stabilizedHT
    if (root.lookupValue(name, str)) {
      if (str == "stabilizedHT") {
        return STABILIZED_HT;
      } else if (str == "rbvmsHT") {
        return RBVMS_HT;
      } else {
        throw TALYFEMLIB::TALYException() << "Unknown solver name for HT: " << name << str;
      }
    } else {
      throw TALYFEMLIB::TALYException() << "Must specify HeatSolverType: stabilizedHT or rbvmsHT";
    }
  }

  /// Function for reading type of Non-dimensionalization
  static typeNondimension read_NondimensionType(libconfig::Setting &root,
                                                const char *name) {
    std::string str;
    /// If nothing specified stays stabilizedHT
    if (root.lookupValue(name, str)) {
      if (str == "free_conv") {
        return FREECONV;
      } else if (str == "mix_conv") {
        return MIXCONV;
      } else {
        throw TALYFEMLIB::TALYException() << "Unknown Non-dimensionalization: " << name << str;
      }
    } else {
      PrintWarning("Non dimensional type not specified, use free convection");
      return FREECONV;
    }
  }
};
