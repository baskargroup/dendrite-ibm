#pragma once

#include <Refinement/Refinement.h>
#include <DendriteUtils.h>
#include "IBM/Geom.h"
class NSHTRefine : public Refinement {
  double *coords_ = NULL;
  std::vector<ZEROPTV> coords;
  bool doCoarsen;
 protected:
  using Refinement::calcElementalError;
  std::vector<Geom<NSHTNodeData> *> geoms;
  NSHTInputData *idata;
 public:
  NSHTRefine(ot::DA *da,
             const DENDRITE_REAL *problemSize,
             NSHTInputData *idata_in,
             const std::vector<Geom<NSHTNodeData> *> &geoms_in,
             bool coarsen = false);


  ot::DA_FLAGS::Refine calcRefineFlags() override;

  ~NSHTRefine() {
    delete coords_;
  }

};

NSHTRefine::NSHTRefine(ot::DA *da,
                       const DENDRITE_REAL *problemSize,
                       NSHTInputData *idata_in,
                       const std::vector<Geom<NSHTNodeData> *> &geoms_in, bool coarsen)
    : Refinement(da, problemSize), geoms(geoms_in), idata(idata_in), doCoarsen(coarsen) {
  coords_ = new double[da->getNumNodesPerElement() * m_uiDim];
  eleOrder_ = baseDA_->getElementOrder();
  coords.resize(da->getNumNodesPerElement());
  if (da->isActive()) {
    this->calcRefinementVector();
  }
}

ot::DA_FLAGS::Refine NSHTRefine::calcRefineFlags() {
  const int nodeNo = baseDA_->getNumNodesPerElement();
  const int currLevel = baseDA_->getLevel(baseDA_->curr());
  baseDA_->getElementalCoords(baseDA_->curr(), coords_);
  coordsToZeroptv(coords_, coords, problemSize_, eleOrder_, true);


  /// refine walls (maximum to the refine_h level)
  const double eps = 1e-13;
  unsigned int levelForWall = idata->mesh_def.refine_l;
  const auto &bc = idata->boundary_def;
  if (idata->mesh_def.refine_walls) {
    if (bc[BoundaryDef::X_MINUS].ifRefine) {
      for (const auto &p : coords) {
        if (p.x() - idata->mesh_def.channel_min.x() < eps and currLevel < idata->mesh_def.refine_h) {
          levelForWall = idata->mesh_def.refine_h;
          break;
        }
      }
    }
    if (bc[BoundaryDef::Y_MINUS].ifRefine) {
      for (const auto &p : coords) {
        if (p.y() - idata->mesh_def.channel_min.y() < eps and currLevel < idata->mesh_def.refine_h) {
          levelForWall = idata->mesh_def.refine_h;
          break;
        }
      }
    }
    if (bc[BoundaryDef::Z_MINUS].ifRefine) {
      for (const auto &p : coords) {
        if (p.z() - idata->mesh_def.channel_min.z() < eps and currLevel < idata->mesh_def.refine_h) {
          levelForWall = idata->mesh_def.refine_h;
          break;
        }
      }
    }
    if (bc[BoundaryDef::X_PLUS].ifRefine) {
      for (const auto &p : coords) {
        if (idata->mesh_def.channel_max.x() - p.x() < eps and currLevel < idata->mesh_def.refine_h) {
          levelForWall = idata->mesh_def.refine_h;
          break;
        }
      }
    }
    if (bc[BoundaryDef::Y_PLUS].ifRefine) {
      for (const auto &p : coords) {
        if (idata->mesh_def.channel_max.y() - p.y() < eps and currLevel < idata->mesh_def.refine_h) {
          levelForWall = idata->mesh_def.refine_h;
          break;
        }
      }
    }
    if (bc[BoundaryDef::Z_PLUS].ifRefine) {
      for (const auto &p : coords) {
        if (idata->mesh_def.channel_max.z() - p.z() < eps and currLevel < idata->mesh_def.refine_h) {
          levelForWall = idata->mesh_def.refine_h;
          break;
        }
      }
    }
  }

  // region refine
  const auto &rf = idata->region_refine;
  unsigned int maxlevelForRegion = idata->mesh_def.refine_l;
  std::vector<unsigned int> levelForRegions;
  levelForRegions.resize(rf.size());
  for (int i = 0; i < rf.size(); i++) {
    auto r = rf[i];
    for (const auto &p : coords) {
      if (r.in_region(p)) {
        levelForRegions[i] = r.refine_region_lvl;
        break;
      } else {
        levelForRegions[i] = idata->mesh_def.refine_l;
      }
    }
  }
  if (!levelForRegions.empty()) {
    maxlevelForRegion = *max_element(levelForRegions.begin(), levelForRegions.end());
  }


  /// regine geometry (IBM)
  unsigned int maxlevelForGeom = idata->mesh_def.refine_l;
  std::vector<int> countOfInPointsEachGeometry;
  std::vector<unsigned int> levelForGeoms;
  countOfInPointsEachGeometry.resize(geoms.size());
  levelForGeoms.resize(geoms.size());
  for (unsigned long noGeom = 0; noGeom < geoms.size(); noGeom++) {
    const auto &geom = geoms[noGeom];
    for (const auto &p : coords) {
      if (geom->point_in_geometry(p)) {
        countOfInPointsEachGeometry[noGeom]++;
      }
    }
  }
  for (unsigned int noGeom = 0; noGeom < geoms.size(); noGeom++) {
    if (not(countOfInPointsEachGeometry[noGeom] == nodeNo || countOfInPointsEachGeometry[noGeom] == 0)) {
      levelForGeoms[noGeom] = (geoms[noGeom]->def_.refine_lvl);
    }
    if(countOfInPointsEachGeometry[noGeom] == nodeNo){
        return ot::DA_FLAGS::Refine::DA_NO_CHANGE;
    }
  }

  if (!levelForGeoms.empty()) {
    maxlevelForGeom = *max_element(levelForGeoms.begin(), levelForGeoms.end());
  }

  if (!doCoarsen) {
    if (currLevel < maxlevelForGeom or
        currLevel < maxlevelForRegion or
        currLevel < levelForWall) {
      return ot::DA_FLAGS::Refine::DA_REFINE;
    }
  } else if (doCoarsen){
    if (currLevel > maxlevelForGeom and
        currLevel > maxlevelForRegion and
        currLevel > levelForWall) {
      return ot::DA_FLAGS::Refine::DA_COARSEN;
    }
  }

  return ot::DA_FLAGS::Refine::DA_NO_CHANGE;
}


