set(NS_INCLUDE
        include/NSNodeData.h
        include/NSEquation.h
        include/NSInputData.h
        include/NSBCSetup.h
        include/NSRefine.h
        include/MathOperation.h
        )

set(NS_SRC
        src/main.cpp
        src/MathOperation.cpp
        )
add_executable(ns ${NS_INCLUDE} ${NS_SRC})
target_include_directories(ns PUBLIC include)
if(TENSOR)
    find_package(MKL)
    if(MKL_FOUND)
        target_link_libraries(ns dendrite2 dendro5 ${MPI_LIBRARIES} ${MKL_LIBRARIES} m)
    else()
        find_package(LAPACK REQUIRED)
        target_link_libraries(ns dendrite2 dendro5 ${MPI_LIBRARIES} ${LAPACK_LIBRARIES} m)
    endif()
else()
    target_link_libraries(ns dendrite2 dendro5 ${MPI_LIBRARIES}  ${MKL_LIBRARIES} m)
endif()



