#!/usr/bin/env python3

import os
import sys
import re

this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, 'scripts')
sys.path.append(scripts_dir)

# choose release mode when possible
found_exec = False
for build_folder in ['cmake-build-release-petsc', 'cmake-build-release', 'cmake-build-debug-petsc',
                     'cmake-build-debug']:
    executable = os.path.join(this_dir, build_folder, 'ns_ht')
    if os.path.exists(executable):
        found_exec = True
        break

assert found_exec, "Program executable missing ({})".format(executable)
print("Executable: {}".format(executable))

from regression import RegressionTester, RegexDiffMetric, VecDiffMetric, FileLinkGenerator, FileCopyGenerator

# test for pure HT
print("test for pure HT")
reg = None
reg = RegressionTester()
base_cfg_ht = {
    'dt': 1,
    'totalT': 2,
    'solveNS': "false",
    'solveHT': "true",
    'refine_wall': "false",
    'name': "fastHT"
}
cases_ht = [
    # pure HT
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 1},
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 4},
    {'order': 2, 'refine_l': 3, 'refine_h': 3, 'ntasks': 1},
    {'order': 2, 'refine_l': 3, 'refine_h': 3, 'ntasks': 4},
    {'order': 1, 'refine_l': 3, 'refine_h': 5, 'refine_wall': "true", 'ntasks': 1},
    {'order': 1, 'refine_l': 3, 'refine_h': 5, 'refine_wall': "true", 'ntasks': 4},
    {'order': 2, 'refine_l': 2, 'refine_h': 4, 'refine_wall': "true", 'ntasks': 1},
    {'order': 2, 'refine_l': 2, 'refine_h': 4, 'refine_wall': "true", 'ntasks': 4},
]
reg.add_cases(base_cfg_ht, cases_ht)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "Checkpoint*", "results*"])

reg.add_metric(VecDiffMetric('ht_vec'))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_1.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_2.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_3.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_4.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_5.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_6.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_1.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_2.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_3.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_4.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_5.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_6.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))

reg.add_file_generator("config.txt", """
dt = {dt}
totalT = {totalT}
surface_cost = 1000
gp_handle = 0
BasisFunction = {order}

NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

### For comparison
HTOnly = false
SolveNS = {solveNS}
SolveHT = {solveHT}


background_mesh = {{
  refine_l = {refine_l}
  refine_h = {refine_h}
  enable_subda = false
  min = [0, 0, 0]
  max = [1, 1, 1]
  refine_walls = {refine_wall}
}}

### Coe setup
Cb_e = 50
Ci_e = 36
Cb_f = 50
Ci_f = 36
Ra = 2500
Pr = 1
tauM_scale = 1
G_dir = 2


### block iteration
blockTolerance = 1e-3
iterMaxBlock = 1


### BCs
boundary = (
  {{
    side = "x-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "x+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y-"
    vel_type = "no_slip"
    temperature_type = "weak"
    temperature = 2
    ifRefine = true
  }},
  {{
    side = "y+"
    vel_type = "no_slip"
    temperature_type = "weak"
    temperature = -2
    ifRefine = true
  }},
  {{
    side = "z-"
    vel_type = "no_slip"
    temperature_type = "dirichlet"
    temperature = -1
  }},
  {{
    side = "z+"
    vel_type = "no_slip"
    temperature_type = "dirichlet"
    temperature = -1
  }}
)

### ICs
initial_condition = {{
  vel_ic_type = "user_defined"
  vel_ic = [0.0, 0.0, 0.0]
  pressure_ic_type = "user_defined"
  pressure_ic = 0.0
}}

OutputStartTime = 0
SurfaceMonitor=[1, 2, 3, 4, 5 ,6]
#################### solver setting ####################
solver_options_ns = {{}};
solver_options_ht = {{
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_monitor = ""
  ksp_converged_reason = ""
}}

""")
reg.set_run_cmd(executable + ' -dump_vec')
reg.run_main()

# test for pure HT (Robin)
print("test for pure HT (Robin)")
reg = None
reg = RegressionTester()
base_cfg_ht["dt"] = 0.1
base_cfg_ht["totalT"] = 5.0
reg.add_cases(base_cfg_ht, cases_ht)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "Checkpoint*", "ns_vec"])

reg.add_metric(VecDiffMetric('ht_vec'))

reg.add_file_generator("config.txt", """
dt = {dt}
totalT = {totalT}
surface_cost = 1000
gp_handle = 0
BasisFunction = {order}

NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

SolveNS = {solveNS}
SolveHT = {solveHT}

background_mesh = {{
  refine_l = {refine_l}
  refine_h = {refine_h}
  enable_subda = false
  min = [0, 0, 0]
  max = [3, 3, 3]
  refine_walls = {refine_wall}
}}

### Coe setup
Cb_e = 20
Ci_e = 36
Cb_f = 20
Ci_f = 36
Ra = 625
Pr = 1
tauM_scale = 1
G_dir = 2


### block iteration
blockTolerance = 1e-3
iterMaxBlock = 1


### BCs
boundary = (
  {{
    side = "x-"
    temperature_type = "dirichlet"
    temperature = 0
    ifRefine = true
  }},
  {{
    side = "x+"
    temperature_type = "robin"
    G_constant = 0
    a_constant = -0.01
    ifRefine = true
  }},
  {{
    side = "y-"
  }},
  {{
    side = "y+"
  }},
  {{
    side = "z-"
  }},
  {{
    side = "z+"
  }}
)


OutputStartTime = 0
OutputInterval = 10

#################### solver setting ####################
solver_options_ns = {{}};
solver_options_ht = {{
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_monitor = ""
  ksp_converged_reason = ""
}}

""")
reg.set_run_cmd(executable + ' -dump_vec')

reg.run_main()

# test for HT+IBM
print("test for HT+IBM")
reg = None
reg = RegressionTester()
base_cfg_ht_ibm = {
    'dt': 1,
    'totalT': 2,
    'solveNS': "false",
    'solveHT': "true",
    'refine_wall': "false",
    'ibm_bc': '"dirichlet"',
    'temperature': "temperature = 1.0",
    'flux': "",
    'name': "fastHTIBM"
}
cases_ht_ibm = [
    # pure HT_IBM
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 1},
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 4},
]
reg.add_cases(base_cfg_ht_ibm, cases_ht_ibm)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "*.vtp"])

reg.add_metric(VecDiffMetric('ht_vec'))

reg.add_generator(FileLinkGenerator('sphere.stl', os.path.join(this_dir, 'sphere_coarse.stl')))
reg.add_generator(
    FileLinkGenerator("config.txt", os.path.join(scripts_dir, "configs", "config-ht-single-ibm.txt")))

reg.set_run_cmd(executable + ' -dump_vec')
reg.run_main()

# test for HT+IBM(multi-geom)
print("test for HT+IBM(multi-geom)")
reg = None
reg = RegressionTester()
cases_ht_ibm_multi = [
    # pure HT_IBM multi geom
    {'ntasks': 1},
    {'ntasks': 4},
]
reg.add_cases({'name': "fast-HT-IBM-multi"}, cases_ht_ibm_multi)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "*.vtp"])

reg.add_metric(VecDiffMetric('ht_vec'))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_sphere.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))

reg.add_generator(FileLinkGenerator('sphere.stl', os.path.join(this_dir, 'sphere_coarse.stl')))
reg.add_generator(
    FileLinkGenerator("config.txt", os.path.join(scripts_dir, "configs", "config-ht-multi-ibm.txt")))

reg.set_run_cmd(executable + ' -dump_vec')
reg.run_main()

# test for HT+IBM (variable gauss points)
print("test for HT+IBM (variable gauss points)")
reg = None
reg = RegressionTester()
cases_ht_ibm_single_gp = [
    # HT_IBM + variable gauss points
    {'ntasks': 1, 'gp_max_splitting': 0},
    {'ntasks': 4, 'gp_max_splitting': 0},
    {'ntasks': 1, 'gp_max_splitting': 1},
    {'ntasks': 4, 'gp_max_splitting': 1},
    {'ntasks': 1, 'gp_max_splitting': 2},
    {'ntasks': 4, 'gp_max_splitting': 2},
]

reg.add_cases({'name': "fast-HT-IBM-gp"}, cases_ht_ibm_single_gp)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "*.vtp", "ns_vec", "Checkpoint*"])

reg.add_metric(VecDiffMetric('ht_vec'))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_cube.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Flux_cube.dat',
                               re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))

reg.add_generator(FileLinkGenerator('cube.stl', os.path.join(this_dir, 'cube_coarse.stl')))
reg.add_file_generator("config.txt", """

dt = 1
totalT = 2

BasisFunction = 1
gp_handle = 32
gp_max_splitting = {gp_max_splitting}
OutputInterval = 2
SolveHT = true
SolveNS = false

background_mesh = {{
  refine_l = 4
  refine_h = 4

  min = [0, 0, 0]
  max = [8, 8, 8]
  refine_walls = false
}}

### Coe setup
Cb_f = 20
Ci_f = 36

Cb_e = 20
Ci_e = 36

Ra = 40000
Pr = 1
tauM_scale = 1
G_dir = 2

NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

blockTolerance = 1
iterMaxBlock = 5
### surface object
geometries = (
  {{
    mesh_path = "cube.stl"
    is_static = true
    position = [2.625, 2.625, 2.625]
    dim = [2.0, 2.0, 2.0]
    type = "cube"
    #radius = 0.5
    min_corner = [2.625, 2.625, 2.625]
    #bc_type = "dirichlet"
    #temperature = 1.0
    bc_type_V = ["dirichlet", "dirichlet", "dirichlet", "dirichlet", "dirichlet"]
    dirichlet_V = [0.0, 0.0, 0.0, 0.0, 1.0]
    
    refine_lvl = 4
  }}
)

### BCs
boundary = (
  {{
    side = "x-"
    vel_type = "dirichlet"
    wall_vel = [0.0, 0.0, 0.0]
    ifRefine = false
  }},
  {{
    side = "x+"
    vel_type = "no_gradient"
    ifRefine = false
    pressure_type = "dirichlet"
    pressure = 0.0
  }},
  {{
    side = "y-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z-"
    vel_type = "no_slip"
    ifRefine = true
    temperature_type = "dirichlet"
    temperature = 1.0
  }},
  {{
    side = "z+"
    vel_type = "no_slip"
    ifRefine = true
  }}
)

### ICs
initial_condition = {{
  vel_ic_type = "user_defined"
  vel_ic = [0.0, 0.0, 0.0]
}}


#################### solver setting ####################
solver_options_ns = {{}};
solver_options_ht = {{
ksp_atol = 1e-6
  ksp_max_it = 2000
  #ksp_type = "bcgs"
  #pc_type = "none"
ksp_converged_reason = ""
ksp_monitor = ""
}};

""")

reg.set_run_cmd(executable + ' -dump_vec')
reg.run_main()

# test for pure NS
print("test for pure NS")
reg = None
reg = RegressionTester()
base_cfg_ns = {
    'dt': 10000,
    'totalT': 10000,
    'solveNS': "true",
    'solveHT': "false",
    'refine_wall': "false",
    'name': "fastNS"
}
cases_ns = [
    # LDC: check serial and parallel, w/wo refinement, linear/quadratic
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 1},
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 4},
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'refine_wall': 'true', 'ntasks': 1},
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'refine_wall': 'true', 'ntasks': 8},

    {'order': 2, 'refine_l': 2, 'refine_h': 2, 'ntasks': 1},
    {'order': 2, 'refine_l': 2, 'refine_h': 2, 'ntasks': 2},
    {'order': 2, 'refine_l': 1, 'refine_h': 2, 'refine_wall': 'true', 'ntasks': 1},
    # {'name': 'slowNS', 'order': 2, 'refine_l': 1, 'refine_h': 3, 'refine_wall': 'true', 'ntasks': 4},
]
reg.add_cases(base_cfg_ns, cases_ns)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info"])

reg.add_metric(VecDiffMetric('ns_vec'))

reg.add_file_generator("config.txt", """
dt = {dt}
totalT = {totalT}
surface_cost = 1000
gp_handle = 0
BasisFunction = {order}

NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

### For comparison
SolveNS = {solveNS}
SolveHT = {solveHT}


background_mesh = {{
  refine_l = {refine_l}
  refine_h = {refine_h}
  enable_subda = false
  min = [0, 0, 0]
  max = [1, 1, 1]
  refine_walls = {refine_wall}
}}

### Coe setup
Cb_e = 20
Ci_e = 36
Cb_f = 20
Ci_f = 36
Ra = 10000
Pr = 1
tauM_scale = 1
G_dir = 2


### block iteration
blockTolerance = 1e-3
iterMaxBlock = 5


### BCs
boundary = (
  {{
    side = "x-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "x+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z+"
    vel_type = "dirichlet"
    wall_vel = [1.0, 0.0, 0.0]
    ifRefine = true
  }}
)

### ICs
initial_condition = {{
  vel_ic_type = "user_defined"
  vel_ic = [0.0, 0.0, 0.0]
  pressure_ic_type = "user_defined"
  pressure_ic = 0.0
}}

OutputStartTime = 0

#################### solver setting ####################
solver_options_ns = {{
  snes_atol = 1e-8
  snes_rtol = 1e-8
  snes_stol = 1e-10
  snes_max_it = 40
  snes_max_funcs = 80000
  ksp_max_it = 2000
  ksp_rtol = 1e-12
  ksp_atol = 1e-10
  ksp_type = "bcgs"
  pc_type = "asm"
  #sub_pc_type = "lu"
#multigrid
  #ksp_type = "bcgs"
  #pc_type = "gamg"
  #pc_gamg_asm_use_agg = True
  #mg_levels_ksp_type = "bcgs"
  #mg_levels_pc_type = "asm"
  #mg_levels_sub_pc_type = "lu"
  #mg_levels_ksp_max_it = 50
#direct solver
  #ksp_type = "bcgs"
  #pc_type = "lu"
  #pc_factor_mat_solver_package = "mumps"
  #pc_factor_levels = 3
# monitor
  snes_monitor = ""
  #snes_converged_reason = ""
  #ksp_monitor = ""
  #ksp_converged_reason = ""
}};
solver_options_ht = {{}}

""")

reg.set_run_cmd(executable + ' -dump_vec')

reg.run_main()

# test for NS+IBM
print("test for NS+IBM")
reg = None
reg = RegressionTester()
base_cfg_ns_ibm = {
    'name': "fastNSIBM",
    'gp_handle': 0,
    'gp_rel_order': 0,
    'gp_max_splitting': 0,
}
cases_ns_ibm = [
    # single Geo, NS IBM
    {'order': 1, 'refine_l': 2, 'refine_h': 2, 'refine_geo': 2, 'ntasks': 1},
    {'order': 1, 'refine_l': 2, 'refine_h': 2, 'refine_geo': 2, 'ntasks': 2},
    {'order': 1, 'refine_l': 2, 'refine_h': 2, 'refine_geo': 4, 'ntasks': 1},
    {'order': 1, 'refine_l': 2, 'refine_h': 2, 'refine_geo': 4, 'ntasks': 8},
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'refine_geo': 4, 'ntasks': 1},
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'refine_geo': 4, 'ntasks': 8},
    {'name': 'slowNSIBM', 'order': 2, 'refine_l': 1, 'refine_h': 1, 'refine_geo': 1, 'ntasks': 1},
    {'name': 'slowNSIBM', 'order': 2, 'refine_l': 1, 'refine_h': 1, 'refine_geo': 1, 'ntasks': 2},
    {'name': 'slowNSIBM', 'order': 2, 'refine_l': 1, 'refine_h': 1, 'refine_geo': 3, 'ntasks': 1},
    {'name': 'slowNSIBM', 'order': 2, 'refine_l': 1, 'refine_h': 1, 'refine_geo': 3, 'ntasks': 2},
    {'name': 'slowNSIBM', 'order': 2, 'refine_l': 1, 'refine_h': 3, 'refine_geo': 3, 'ntasks': 1},
    {'name': 'slowNSIBM', 'order': 2, 'refine_l': 1, 'refine_h': 3, 'refine_geo': 3, 'ntasks': 4},
]
reg.add_cases(base_cfg_ns_ibm, cases_ns_ibm)

reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "*.vtp"])

reg.add_metric(VecDiffMetric('ns_vec'))
reg.add_metric(
    RegexDiffMetric('Force_pressure', 'Force_sphere.dat', re.compile(r'Fx_pre = (.*), Fy_pre = (.*), Fz_pre = (.*)')))
reg.add_metric(
    RegexDiffMetric('Force_viscous', 'Force_sphere.dat', re.compile(r'Fx_vis = (.*), Fy_vis = (.*), Fz_vis = (.*)')))
reg.add_metric(
    RegexDiffMetric('Force_penalty', 'Force_sphere.dat', re.compile(r'Fx_pen = (.*), Fy_pen = (.*), Fz_pen = (.*)')))

reg.add_generator(FileLinkGenerator('sphere.stl', os.path.join(this_dir, 'sphere_coarse.stl')))
reg.add_file_generator("config.txt", """

dt = 1
totalT = 2
surface_cost = 1000
gp_handle = {gp_handle}
gp_rel_order = {gp_rel_order}
gp_max_splitting = {gp_max_splitting}
BasisFunction = {order}

#NavierStokesSolverType = "stabilizedNS"
NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

### For comparison
SolveNS = true
SolveHT = false

background_mesh = {{
  refine_l = {refine_l}
  refine_h = {refine_h}
  enable_subda = false
  min = [0, 0, 0]
  max = [2, 2, 2]
  refine_walls = true
}}

### Coe setup
Cb_e = 50
Ci_e = 36
Cb_f = 50
Ci_f = 36
Ra = 2500
Pr = 1
tauM_scale = 1
G_dir = 2


### block iteration
blockTolerance = 1e-3
iterMaxBlock = 5

geometries = (
  {{
    mesh_path = "sphere.stl"
    is_static = true
    position = [1.0, 1.0, 1.0]
    type = "sphere"
    radius = 0.5
    com = [1.0, 1.0, 1.0]
    #bc_type = "dirichlet"
    #temperature = 0.0
    bc_type_V = ["dirichlet", "dirichlet", "dirichlet", "dirichlet", "dirichlet"]
    dirichlet_V = [0.0, 0.0, 0.0, 0.0, 0.0]
    refine_lvl = {refine_geo}
  }}
)

### BCs
boundary = (
  {{
    side = "x-"
    vel_type = "dirichlet"
    wall_vel = [1.0, 0.0, 0.0]
    ifRefine = true
  }},
  {{
    side = "x+"
    #vel_type = "no_slip"
    pressure_type = "dirichlet"
    pressure = 0.0
    ifRefine = true
  }},
  {{
    side = "y-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z+"
    vel_type = "no_slip"
    ifRefine = true
  }}
)

### ICs
initial_condition = {{
  vel_ic_type = "user_defined"
  vel_ic = [1.0, 0.0, 0.0]
  pressure_ic_type = "user_defined"
  pressure_ic = 0.0
}}

OutputStartTime = 0

#################### solver setting ####################
solver_options_ns = {{
  snes_atol = 1e-8
  snes_rtol = 1e-8
  snes_stol = 1e-10
  snes_max_it = 40
  snes_max_funcs = 80000
  ksp_max_it = 2000
  ksp_rtol = 1e-12
  ksp_atol = 1e-10
  ksp_type = "bcgs"
  pc_type = "asm"
}};
solver_options_ht = {{}}

""")

reg.set_run_cmd(executable + ' -dump_vec')
reg.run_main()

# test for NS+HT
print("test for NS+HT")
rr = """
region_refine = (
{
type = "cube"
min_c = [0.7, 0.7, 0.7]
max_c = [0.9, 0.9, 0.9]
refine_region_lvl = 5
},
{
type = "sphere"
refine_region_lvl = 5
radius = 0.15
center = [0.15, 0.15, 0.15]
},
{
type = "cylinder"
refine_region_lvl = 5
radius = 0.15
c1 = [0.3, 0.3, 0.3]
c2 = [0.6, 0.6, 0.6]
}
)"""
base_cfg_nsht = {
    'name': "fastNH",
    'gp_handle': 0,
    'gp_rel_order': 0,
    'gp_max_splitting': 0,
    'dt': 0.1,
    'totalT': 0.3,
    'NONDIM_TYPE': "free_conv",
    'RP': "[0]",
    'V': "",
    'Ra': 10000,
    'Pr': 1.0,
    'Re': 100,
    'Gr': 10000,
    'Pe': 100,
    'REGION_REFINE': ""
}
cases_nsht = [
    # NSHT
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 8},
    {'NONDIM_TYPE': "mix_conv", 'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 8},
    {'NONDIM_TYPE': "mix_conv", 'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 8, 'REGION_REFINE': rr},
    # ramping of parameters
    {'dt': 0.1, 'totalT': 1.0, 'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 8,
     "RP": "[0.0, 0.5]", "V": "_V", "Ra": "[100, 10000]", "Pr": "[0.7, 1.0]"},
    {'NONDIM_TYPE': "mix_conv",
     'dt': 0.1, 'totalT': 1.0, 'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 8,
     "RP": "[0.0, 0.5]", "V": "_V", "Re": "[11.95228609, 100.0]", "Gr": "[142.8571429, 10000.0]",
     "Pe": "[8.366600265, 100.0]"},
]
reg = None
reg = RegressionTester()
reg.add_cases(base_cfg_nsht, cases_nsht)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "*.vtp", "Checkpoint*"])

reg.add_metric(VecDiffMetric('ns_vec'))
reg.add_metric(VecDiffMetric('ht_vec'))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_1.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(RegexDiffMetric('Nu', 'Nu_2.dat', re.compile(r'Nu_normal = (.*), Nu_penalty = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_1.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))
reg.add_metric(
    RegexDiffMetric('Flux', 'Flux_2.dat', re.compile(r'Flux_x_total = (.*), Flux_y_total = (.*), Flux_z_total = (.*)')))

reg.add_file_generator("config.txt", """

dt = {dt}
totalT = {totalT}
surface_cost = 1000
gp_handle = {gp_handle}
gp_rel_order = {gp_rel_order}
gp_max_splitting = {gp_max_splitting}
BasisFunction = {order}

#NavierStokesSolverType = "stabilizedNS"
NavierStokesSolverType = "rbvmsNS"
HeatSolverType = "rbvmsHT"

### For comparison
HTOnly = false
SolveNS = True
SolveHT = True


background_mesh = {{
  refine_l = {refine_l}
  refine_h = {refine_h}
  enable_subda = false
  min = [0, 0, 0]
  max = [1, 1, 1]
  refine_walls = true
}}

{REGION_REFINE}

### Coe setup
Cb_e = 50
Ci_e = 36
Cb_f = 50
Ci_f = 36

NondimensionType = "{NONDIM_TYPE}"
ramping_parameter = {RP}
Ra{V} = {Ra}
Pr{V} = {Pr}
Re{V} = {Re}
Pe{V} = {Pe}
Gr{V} = {Gr}
tauM_scale = 1
G_dir = 2


### block iteration
blockTolerance = 1e-3
iterMaxBlock = 5

### BCs
boundary = (
  {{
    side = "x-"
    vel_type = "no_slip"
    ifRefine = true
    temperature_type = "dirichlet"
    temperature = 1.0
  }},
  {{
    side = "x+"
    vel_type = "no_slip"
    ifRefine = true
    temperature_type = "dirichlet"
    temperature = -1.0
  }},
  {{
    side = "y-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z+"
    vel_type = "no_slip"
    ifRefine = true
  }}
)

### ICs
initial_condition = {{
  vel_ic_type = "user_defined"
  vel_ic = [0.0, 0.0, 0.0]
  pressure_ic_type = "user_defined"
  pressure_ic = 0.0
}}

OutputStartTime = 0
OutputInterval = 1
SurfaceMonitor = [1, 2]
#################### solver setting ####################
solver_options_ns = {{
  snes_atol = 1e-8
  snes_rtol = 1e-8
  snes_stol = 1e-10
  snes_max_it = 40
  snes_max_funcs = 80000
  ksp_max_it = 2000
  ksp_rtol = 1e-12
  ksp_atol = 1e-10
  ksp_type = "bcgs"
  pc_type = "asm"
}};
solver_options_ht = {{  
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_monitor = ""
  ksp_converged_reason = ""
}}


""")
reg.set_run_cmd(executable + ' -dump_vec')
reg.run_main()
